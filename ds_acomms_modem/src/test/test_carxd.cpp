/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/whoi_micromodem_serialization.h"
#include "ds_acomms_micromodem_msgs/CARXD.h"
#include <gtest/gtest.h>

TEST(CARXD, deserialize)
{
  auto str = std::string{ "$CARXD,1,15,0,1,"
                          "660053445120300d0000000000000000000000000000000000000000000000000000000000000000000000000000"
                          "000000000000000000000000000000000000*03" };
  auto expected = ds_acomms_micromodem_msgs::CARXD{};
  expected.src_address = 1;
  expected.dest_address = 15;
  expected.ack = false;
  expected.frame = 1;
  expected.bytes = { 0x66, 0x00, 0x53, 0x44, 0x51, 0x20, 0x30, 0x0d };
  expected.bytes.resize(64);

  auto result = ds_acomms_micromodem_msgs::CARXD{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.src_address, result.src_address);
  EXPECT_EQ(expected.dest_address, result.dest_address);
  EXPECT_EQ(expected.ack, result.ack);
  EXPECT_EQ(expected.frame, result.frame);
  EXPECT_EQ(expected.bytes, result.bytes);
}

TEST(CARXD, serialize)
{
  auto expected = std::string{ "$CARXD,1,15,0,1,660053445120300D*23\r\n" };
  auto msg = ds_acomms_micromodem_msgs::CARXD{};
  msg.src_address = 1;
  msg.dest_address = 15;
  msg.ack = false;
  msg.frame = 1;
  msg.bytes = { 0x66, 0x00, 0x53, 0x44, 0x51, 0x20, 0x30, 0x0D };

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
