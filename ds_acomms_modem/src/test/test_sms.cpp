/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/SMS.h"

#include <gtest/gtest.h>

const auto SMS_TEST_STRING =
    "SMS:1009[XC87,SNR44,DBV0,CRP81;100,IFL28,TEL;RPSKV1,FEC0;0,DOP0.00;-3.16;0.00,SXC-39911424;95027200;-14876672;"
    "80543744;-46006272;34996224;20578304;207749120"
    ";-47841280;414973952;-140967936;564658176;-143065088;771096576;-304480256;619970560;-239861760;413335552;-"
    "151584768;240517120;-123076608;27918336;-4587520;45350912,CHAN286525730;46;46;185;104;12;148;604;863;565;24"
    "3;75;34;28;63;93;104;205;316;487;540;73;58;382;671;33;357;1506;2127;443;215;1000;1305;104;323;2195;3547;2066;951;"
    "220;3;109;212;348;641;339;332;174;24;21;11;35;62;326;686;1384;1811;857;466;93;993;4787;13327;27564;3"
    "4976;19284;9903;2475;191;372;588;1355;1678;935;413;57;23;2;0;2;67;107;358;612;566;421;180;73;83;512;1612;3368;"
    "4098;1463;316;336;910;89;132;1038;1832;508;48;269;846;214;171;488;594;385;179;51;73;38;27;28;43;138;321"
    ";661;876;365;138;32;80;56;19;3;29;94;365;864;983;106;182;1114;1973;959;373;36;63;7;74;255;463;511;603;683;679;388;"
    "175;24;18;77;202;349;442;515;513;507;388;155;47;2;32;72;255;561;694;426;187;39;48;13;13;3;0;4;9;9;1"
    "6;16;52;100;123;57;32;5;1;2;4;1;1;3;4;4;12;22;18;12;5;0;0;0;2;5;6;8;7;5;4;1;0;0;0;1;2;3;2;0;0;0;1;3;4;4;1;1;4;7;5;"
    "2;0;1;1;1;0;0;1;1;1;0;0;0;0;1;1;1;1;1;1;1;2;2;1;1;0]|SDQ 0\r\n";

TEST(Sms, deserialize)
{
  auto str = SMS_TEST_STRING;

  auto expected = ds_acomms_sonardyne_avtrak_msgs::SMS{};
  expected.remote_addr = 1009;
  expected.xc = { 87 };
  expected.snr = { 44 };
  expected.dbv = { 0 };
  expected.ifl = { 28 };
  expected.crp = { 81, 100 };
  expected.tel = { "RPSKV1" };
  expected.fec = { 0, 0 };
  expected.dop = { -0.0, -3.16, 0 };
  expected.sxc = { -39911424,  95027200,  -14876672,  80543744,  -46006272,  34996224,  20578304,   207749120,
                   -47841280,  414973952, -140967936, 564658176, -143065088, 771096576, -304480256, 619970560,
                   -239861760, 413335552, -151584768, 240517120, -123076608, 27918336,  -4587520,   45350912 },
  expected.chan = {
    286525730, 46,   46,   185,  104, 12,  148, 604,  863,   565,   243,   75,    34,   28,   63,   93,  104, 205,
    316,       487,  540,  73,   58,  382, 671, 33,   357,   1506,  2127,  443,   215,  1000, 1305, 104, 323, 2195,
    3547,      2066, 951,  220,  3,   109, 212, 348,  641,   339,   332,   174,   24,   21,   11,   35,  62,  326,
    686,       1384, 1811, 857,  466, 93,  993, 4787, 13327, 27564, 34976, 19284, 9903, 2475, 191,  372, 588, 1355,
    1678,      935,  413,  57,   23,  2,   0,   2,    67,    107,   358,   612,   566,  421,  180,  73,  83,  512,
    1612,      3368, 4098, 1463, 316, 336, 910, 89,   132,   1038,  1832,  508,   48,   269,  846,  214, 171, 488,
    594,       385,  179,  51,   73,  38,  27,  28,   43,    138,   321,   661,   876,  365,  138,  32,  80,  56,
    19,        3,    29,   94,   365, 864, 983, 106,  182,   1114,  1973,  959,   373,  36,   63,   7,   74,  255,
    463,       511,  603,  683,  679, 388, 175, 24,   18,    77,    202,   349,   442,  515,  513,  507, 388, 155,
    47,        2,    32,   72,   255, 561, 694, 426,  187,   39,    48,    13,    13,   3,    0,    4,   9,   9,
    16,        16,   52,   100,  123, 57,  32,  5,    1,     2,     4,     1,     1,    3,    4,    4,   12,  22,
    18,        12,   5,    0,    0,   0,   2,   5,    6,     8,     7,     5,     4,    1,    0,    0,   0,   1,
    2,         3,    2,    0,    0,   0,   1,   3,    4,     4,     1,     1,     4,    7,    5,    2,   0,   1,
    1,         1,    0,    0,    1,   1,   1,   0,    0,     0,     0,     1,     1,    1,    1,    1,   1,   1,
    2,         2,    1,    1,    0
  };
  expected.data = "SDQ 0";

  auto result = ds_acomms_sonardyne_avtrak_msgs::SMS{};

  ASSERT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_STREQ(expected.data.data(), result.data.data());
  EXPECT_EQ(expected.xc, result.xc);
  EXPECT_EQ(expected.snr, result.snr);
  EXPECT_EQ(expected.dbv, result.dbv);
  EXPECT_EQ(expected.dop, result.dop);
  EXPECT_EQ(expected.chan, result.chan);
  EXPECT_EQ(expected.tel, result.tel);
  EXPECT_EQ(expected.fec, result.fec);
  EXPECT_EQ(expected.crp, result.crp);
  EXPECT_EQ(expected.sxc, result.sxc);
  EXPECT_EQ(expected.ifl, result.ifl);
}

#if 0
TEST(SmsDiagnostics, serialize)
{
  auto expected = SMS_TEST_STRING;
  auto msg = ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics{};

  msg.XC = 90;
  msg.XC_valid = true;
  msg.SNR = 44;
  msg.SNR_valid = true;
  msg.DBV = 0;
  msg.DBV_valid = true;
  msg.IFL = 27;
  msg.IFL_valid = true;
  msg.CRP = { 98, 4 };
  msg.CRP_valid = true;
  msg.TEL = { "TCS1", "RPSKV2_200" };
  msg.TEL_valid = true;
  msg.FEC = { 0, 0 };
  msg.FEC_valid = true;
  msg.DOP = { -0.58, -1, 0 };
  msg.DOP_valid = true;
  msg.SXC = { 108265472, -85458944,  144637952, -44498944,  51511296, -53280768,  14417920,  -277544960,
              -12255232, -497876992, -52822016, -703201280, 90570752, -826474496, 138543104, -598409216,
              133562368, -385482752, 134479872, -178782208, 41943040, -8585216,   -6488064,  -66650112 };
  msg.SXC_valid = true;
  msg.CHAN = { 321664544, 37,    37,    90,   150,  48,   68,   457,  964,  722,  383, 82,   23,   93,   31,   48,
               49,        120,   284,   425,  650,  201,  17,   277,  948,  377,  126, 835,  2508, 1372, 292,  377,
               1683,      740,   153,   1226, 3823, 3090, 1387, 274,  7,    100,  256, 330,  723,  591,  302,  238,
               56,        22,    32,    19,   2,    97,   473,  1033, 2039, 1313, 688, 232,  117,  2896, 9552, 21650,
               39265,     28272, 14124, 4390, 316,  1318, 1073, 1392, 2068, 1336, 684, 193,  7,    46,   47,   59,
               122,       194,   243,   403,  491,  460,  316,  169,  102,  59,   636, 1835, 3973, 2302, 528,  311,
               1588,      888,   166,   777,  2590, 1547, 380,  203,  1099, 758,  201, 45,   421,  506,  345,  236,
               125,       78,    23,    2,    20,   38,   196,  540,  1130, 700,  245, 17,   100,  117,  55,   27,
               42,        46,    267,   887,  1708, 747,  72,   394,  1865, 1534, 621, 111,  56,   88,   69,   160,
               388,       550,   599,   788,  855,  446,  153,  12,   99,   121,  164, 151,  158,  254,  353,  517,
               708,       525,   313,   133,  38,   164,  327,  657,  1109, 779,  377, 110,  8,    46,   78,   97,
               111,       78,    32,    22,   9,    9,    9,    3,    7,    10,   11,  29,   35,   33,   14,   1,
               0,         5,     9,     8,    4,    1,    0,    2,    2,    1,    0,   4,    6,    4,    1,    0,
               0,         0,     0,     0,    0,    0,    0,    0,    0,    0,    0,   0,    0,    1,    1,    0,
               0,         0,     0,     1,    1,    0,    0,    2,    4,    4,    2,   1,    0,    0,    0,    1,
               2,         1,     0,     0,    0,    1,    1,    1,    1,    0,    0,   0,    0,    0,    0,    0,
               0 };
  msg.CHAN_valid = true;

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}
#endif

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
