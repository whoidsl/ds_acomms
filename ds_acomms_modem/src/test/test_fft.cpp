/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/FFT.h"

#include <gtest/gtest.h>

const static auto FFT_MESSAGE =
    "FFT:1201,E0,N256,F0,A16,GAIN34,-47.3;-52.7;-52.7;-59.3;-71.3;-83.1;-75.8;-79.5;-80.5;-77.8;-80.3;-75.9;-79.3;-74."
    "8;-78.0;-73.3;-75.9;-75.9;-75.2;-75.2;-74.8;-81.0;-75.9;-77.8;-78.5;-85.3;-77.1;-83.1;-82.5;-79.5;-78.3;-80.8;-84."
    "2;-79.1;-83.5;-80.5;-83.1;-82.5;-81.3;-81.3;-81.6;-80.3;-77.5;-82.7;-78.9;-81.9;-79.5;-87.0;-79.5;-86.8;-81.2;-85."
    "9;-83.3;-85.3;-83.3;-83.5;-82.7;-81.3;-82.3;-82.9;-83.8;-82.7;-84.5;-81.9;-85.9;-83.8;-83.1;-84.2;-83.3;-86.3;-83."
    "8;-89.5;-83.8;-85.5;-82.3;-85.7;-83.1;-85.5;-82.9;-85.5;-82.7;-85.1;-83.1;-84.5;-85.7;-81.9;-82.7;-84.0;-83.8;-83."
    "8;-84.2;-85.5;-83.3;-85.1;-85.3;-81.3;-84.0;-81.3;-83.1;-82.1;-84.0;-85.1;-83.3;-86.1;-83.3;-84.5;-83.5;-84.3;-84."
    "6;-83.3;-83.5;-83.1;-83.3;-81.6;-84.6;-83.5;-84.2;-85.5;-82.1;-84.3;-85.1;-82.5;-85.1;-85.3;-83.8;-86.5;-84.6;-85."
    "3;-84.0;-86.3;-85.3;-83.8;-84.3;-85.7;-85.1;-85.1;-85.5;-84.3;-84.3;-86.5;-84.3;-83.1;-85.3;-84.0;-87.0;-83.5;-87."
    "3;-83.8;-87.3;-84.6;-80.5;-81.3;-84.2;-84.6;-82.5;-85.3;-85.1;-87.0;-86.3;-84.8;-87.6;-85.5;-84.8;-87.3;-85.1;-87."
    "6;-86.5;-86.5;-85.3;-86.3;-84.9;-86.8;-87.9;-85.5;-85.7;-86.5;-87.0;-86.5;-86.5;-86.5;-86.8;-86.3;-86.3;-85.7;-86."
    "3;-86.8;-86.5;-87.3;-86.5;-87.9;-86.1;-87.0;-86.1;-86.8;-86.1;-87.0;-86.3;-87.3;-84.2;-83.1;-84.3;-85.5;-84.0;-85."
    "3;-85.5;-85.9;-85.1;-79.1;-77.8;-83.5;-87.9;-85.3;-87.6;-84.9;-87.6;-86.5;-86.5;-86.3;-87.0;-86.1;-86.3;-88.3;-86."
    "1;-87.6;-87.0;-86.1;-86.3;-87.0;-86.8;-87.6;-85.9;-87.3;-86.5;-84.6;-83.5;-86.1;-87.9;-85.3;-87.0;-86.8;-87.6;-86."
    "5;-86.5;-86.3;-86.8;-86.1;-80.1;-71.5;-74.3;-85.3;-87.9;-87.3;-86.1;-86.1;-88.7;-85.9\r";
const static auto FFT_COMMAND = "FFT:N256,F0,A16\r";

TEST(FFT, deserialize_fft)
{
  auto str = FFT_MESSAGE;
  auto expected = ds_acomms_sonardyne_avtrak_msgs::FFT{};
  expected.address = 1201;
  expected.n_points = 256;
  expected.gain = 34;
  expected.averages = 16;
  expected.frequency_enum = 0;
  expected.fft = {
    -47.3, -52.7, -52.7, -59.3, -71.3, -83.1, -75.8, -79.5, -80.5, -77.8, -80.3, -75.9, -79.3, -74.8, -78.0, -73.3,
    -75.9, -75.9, -75.2, -75.2, -74.8, -81.0, -75.9, -77.8, -78.5, -85.3, -77.1, -83.1, -82.5, -79.5, -78.3, -80.8,
    -84.2, -79.1, -83.5, -80.5, -83.1, -82.5, -81.3, -81.3, -81.6, -80.3, -77.5, -82.7, -78.9, -81.9, -79.5, -87.0,
    -79.5, -86.8, -81.2, -85.9, -83.3, -85.3, -83.3, -83.5, -82.7, -81.3, -82.3, -82.9, -83.8, -82.7, -84.5, -81.9,
    -85.9, -83.8, -83.1, -84.2, -83.3, -86.3, -83.8, -89.5, -83.8, -85.5, -82.3, -85.7, -83.1, -85.5, -82.9, -85.5,
    -82.7, -85.1, -83.1, -84.5, -85.7, -81.9, -82.7, -84.0, -83.8, -83.8, -84.2, -85.5, -83.3, -85.1, -85.3, -81.3,
    -84.0, -81.3, -83.1, -82.1, -84.0, -85.1, -83.3, -86.1, -83.3, -84.5, -83.5, -84.3, -84.6, -83.3, -83.5, -83.1,
    -83.3, -81.6, -84.6, -83.5, -84.2, -85.5, -82.1, -84.3, -85.1, -82.5, -85.1, -85.3, -83.8, -86.5, -84.6, -85.3,
    -84.0, -86.3, -85.3, -83.8, -84.3, -85.7, -85.1, -85.1, -85.5, -84.3, -84.3, -86.5, -84.3, -83.1, -85.3, -84.0,
    -87.0, -83.5, -87.3, -83.8, -87.3, -84.6, -80.5, -81.3, -84.2, -84.6, -82.5, -85.3, -85.1, -87.0, -86.3, -84.8,
    -87.6, -85.5, -84.8, -87.3, -85.1, -87.6, -86.5, -86.5, -85.3, -86.3, -84.9, -86.8, -87.9, -85.5, -85.7, -86.5,
    -87.0, -86.5, -86.5, -86.5, -86.8, -86.3, -86.3, -85.7, -86.3, -86.8, -86.5, -87.3, -86.5, -87.9, -86.1, -87.0,
    -86.1, -86.8, -86.1, -87.0, -86.3, -87.3, -84.2, -83.1, -84.3, -85.5, -84.0, -85.3, -85.5, -85.9, -85.1, -79.1,
    -77.8, -83.5, -87.9, -85.3, -87.6, -84.9, -87.6, -86.5, -86.5, -86.3, -87.0, -86.1, -86.3, -88.3, -86.1, -87.6,
    -87.0, -86.1, -86.3, -87.0, -86.8, -87.6, -85.9, -87.3, -86.5, -84.6, -83.5, -86.1, -87.9, -85.3, -87.0, -86.8,
    -87.6, -86.5, -86.5, -86.3, -86.8, -86.1, -80.1, -71.5, -74.3, -85.3, -87.9, -87.3, -86.1, -86.1, -88.7, -85.9
  };

  auto result = ds_acomms_sonardyne_avtrak_msgs::FFT{};

  ASSERT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.address, result.address);
  EXPECT_EQ(expected.n_points, result.n_points);
  EXPECT_EQ(expected.gain, result.gain);
  EXPECT_EQ(expected.averages, result.averages);
  EXPECT_EQ(expected.frequency_enum, result.frequency_enum);
  ASSERT_EQ(expected.fft.size(), result.fft.size());
  EXPECT_EQ(expected.fft, result.fft);
  EXPECT_EQ(expected.n_points, result.fft.size());
}

TEST(FFT, seriailze_fft_command)
{
  auto msg = ds_acomms_sonardyne_avtrak_msgs::FFT{};
  msg.address = 1201;
  msg.n_points = 256;
  msg.gain = 34;
  msg.averages = 16;
  msg.frequency_enum = 0;
  msg.fft = { -47.3, -52.7, -52.7, -59.3, -71.3, -83.1, -75.8, -79.5, -80.5, -77.8, -80.3, -75.9, -79.3, -74.8, -78.0,
              -73.3, -75.9, -75.9, -75.2, -75.2, -74.8, -81.0, -75.9, -77.8, -78.5, -85.3, -77.1, -83.1, -82.5, -79.5,
              -78.3, -80.8, -84.2, -79.1, -83.5, -80.5, -83.1, -82.5, -81.3, -81.3, -81.6, -80.3, -77.5, -82.7, -78.9,
              -81.9, -79.5, -87.0, -79.5, -86.8, -81.2, -85.9, -83.3, -85.3, -83.3, -83.5, -82.7, -81.3, -82.3, -82.9,
              -83.8, -82.7, -84.5, -81.9, -85.9, -83.8, -83.1, -84.2, -83.3, -86.3, -83.8, -89.5, -83.8, -85.5, -82.3,
              -85.7, -83.1, -85.5, -82.9, -85.5, -82.7, -85.1, -83.1, -84.5, -85.7, -81.9, -82.7, -84.0, -83.8, -83.8,
              -84.2, -85.5, -83.3, -85.1, -85.3, -81.3, -84.0, -81.3, -83.1, -82.1, -84.0, -85.1, -83.3, -86.1, -83.3,
              -84.5, -83.5, -84.3, -84.6, -83.3, -83.5, -83.1, -83.3, -81.6, -84.6, -83.5, -84.2, -85.5, -82.1, -84.3,
              -85.1, -82.5, -85.1, -85.3, -83.8, -86.5, -84.6, -85.3, -84.0, -86.3, -85.3, -83.8, -84.3, -85.7, -85.1,
              -85.1, -85.5, -84.3, -84.3, -86.5, -84.3, -83.1, -85.3, -84.0, -87.0, -83.5, -87.3, -83.8, -87.3, -84.6,
              -80.5, -81.3, -84.2, -84.6, -82.5, -85.3, -85.1, -87.0, -86.3, -84.8, -87.6, -85.5, -84.8, -87.3, -85.1,
              -87.6, -86.5, -86.5, -85.3, -86.3, -84.9, -86.8, -87.9, -85.5, -85.7, -86.5, -87.0, -86.5, -86.5, -86.5,
              -86.8, -86.3, -86.3, -85.7, -86.3, -86.8, -86.5, -87.3, -86.5, -87.9, -86.1, -87.0, -86.1, -86.8, -86.1,
              -87.0, -86.3, -87.3, -84.2, -83.1, -84.3, -85.5, -84.0, -85.3, -85.5, -85.9, -85.1, -79.1, -77.8, -83.5,
              -87.9, -85.3, -87.6, -84.9, -87.6, -86.5, -86.5, -86.3, -87.0, -86.1, -86.3, -88.3, -86.1, -87.6, -87.0,
              -86.1, -86.3, -87.0, -86.8, -87.6, -85.9, -87.3, -86.5, -84.6, -83.5, -86.1, -87.9, -85.3, -87.0, -86.8,
              -87.6, -86.5, -86.5, -86.3, -86.8, -86.1, -80.1, -71.5, -74.3, -85.3, -87.9, -87.3, -86.1, -86.1, -88.7,
              -85.9 };

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  ASSERT_STREQ(FFT_MESSAGE, result.data());
}

TEST(FFT, deserialize_fft_command)
{
  auto str = FFT_COMMAND;
  auto expected = ds_acomms_sonardyne_avtrak_msgs::FFT{};
  expected.address = 0;
  expected.n_points = 256;
  expected.gain = 0;
  expected.averages = 16;
  expected.frequency_enum = 0;
  expected.fft = {};

  auto result = ds_acomms_sonardyne_avtrak_msgs::FFT{};

  ASSERT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.address, result.address);
  EXPECT_EQ(expected.n_points, result.n_points);
  EXPECT_EQ(expected.gain, result.gain);
  EXPECT_EQ(expected.averages, result.averages);
  EXPECT_EQ(expected.frequency_enum, result.frequency_enum);
  ASSERT_EQ(expected.fft.size(), result.fft.size());
  EXPECT_EQ(expected.fft, result.fft);
  EXPECT_EQ(0, result.fft.size());
}

TEST(FFT, serialize_fft_command)
{
  auto msg = ds_acomms_sonardyne_avtrak_msgs::FFT{};
  msg.address = 0;
  msg.n_points = 256;
  msg.gain = 0;
  msg.averages = 16;
  msg.frequency_enum = 0;
  msg.fft = {};

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  ASSERT_STREQ(FFT_COMMAND, result.data());
}

#if 0
TEST(DIAG, serialize)
{
  auto expected = std::string{ "DIAG:XC,DBV\r" };
  auto msg = ds_acomms_sonardyne_avtrak_msgs::DIAG{};
  msg.DBV = true;
  msg.XC = true;

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}
#endif
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
