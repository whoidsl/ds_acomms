/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/whoi_micromodem_serialization.h"
#include <ds_acomms_micromodem_msgs/CACST.h>
#include <gtest/gtest.h>

TEST(CACST, deserialize_version_0)
{
  auto str = std::string{ "$CACST,1,033442.0000,1,129,-2,0146,0112,145,02,02,04,02,1,001,015,3,4,4,4,136,06,-7,-7,03,"
                          "000,0.5,33,10000,2000*7E\r\n" };
  auto expected = ds_acomms_micromodem_msgs::CACST{};
  expected.mm2 = false;
  expected.version_number = 0;
  expected.date = "";
  expected.mode = 1;
  expected.time_of_arrival = "033442.0000";
  expected.time_of_arrival_mode = 1;
  expected.mfd_peak = 129;
  expected.mfd_power_db = -2;
  expected.mfd_ratio_db = 146;
  expected.spl_db = 112;
  expected.analog_gain = 145;
  expected.shift_previous_ain = 2;
  expected.shift_current_ain = 2;
  expected.shift_mfd = 4;
  expected.shift_p2b = 2;
  expected.rate = 1;
  expected.source = 1;
  expected.destination = 15;
  expected.psk_error_code = 3;
  expected.packet_type = 4;
  expected.num_frames = 4;
  expected.num_frames_bad = 4;
  expected.rss_db = 136;
  expected.snr_input_db = 6;
  expected.snr_equalizer_output_db = -7;
  expected.snr_symbols = -7;
  expected.equilizer_mse = 3;
  expected.data_quality_factor = 0;
  expected.doppler = 0.5;
  expected.noise_stdev = 33;
  expected.carrier_freq_hz = 10000;
  expected.bandwidth_hz = 2000;

  auto result = ds_acomms_micromodem_msgs::CACST{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.mm2, result.mm2);
  EXPECT_EQ(expected.version_number, result.version_number);
  EXPECT_EQ(expected.date, result.date);
  EXPECT_EQ(expected.mode, result.mode);
  EXPECT_EQ(expected.time_of_arrival, result.time_of_arrival);
  EXPECT_EQ(expected.time_of_arrival_mode, result.time_of_arrival_mode);
  EXPECT_EQ(expected.mfd_peak, result.mfd_peak);
  EXPECT_EQ(expected.mfd_ratio_db, result.mfd_ratio_db);
  EXPECT_EQ(expected.spl_db, result.spl_db);
  EXPECT_EQ(expected.analog_gain, result.analog_gain);
  EXPECT_EQ(expected.shift_previous_ain, result.shift_previous_ain);
  EXPECT_EQ(expected.shift_current_ain, result.shift_current_ain);
  EXPECT_EQ(expected.shift_mfd, result.shift_mfd);
  EXPECT_EQ(expected.shift_p2b, result.shift_p2b);
  EXPECT_EQ(expected.rate, result.rate);
  EXPECT_EQ(expected.source, result.source);
  EXPECT_EQ(expected.destination, result.destination);
  EXPECT_EQ(expected.psk_error_code, result.psk_error_code);
  EXPECT_EQ(expected.packet_type, result.packet_type);
  EXPECT_EQ(expected.num_frames, result.num_frames);
  EXPECT_EQ(expected.num_frames_bad, result.num_frames_bad);
  EXPECT_EQ(expected.rss_db, result.rss_db);
  EXPECT_EQ(expected.snr_input_db, result.snr_input_db);
  EXPECT_EQ(expected.snr_equalizer_output_db, result.snr_equalizer_output_db);
  EXPECT_EQ(expected.snr_symbols, result.snr_symbols);
  EXPECT_EQ(expected.equilizer_mse, result.equilizer_mse);
  EXPECT_EQ(expected.data_quality_factor, result.data_quality_factor);
  EXPECT_EQ(expected.doppler, result.doppler);
  EXPECT_EQ(expected.noise_stdev, result.noise_stdev);
  EXPECT_EQ(expected.carrier_freq_hz, result.carrier_freq_hz);
  EXPECT_EQ(expected.bandwidth_hz, result.bandwidth_hz);
}

TEST(CACST, serialize)
{
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
