/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/DIAG.h"

#include <gtest/gtest.h>

TEST(DIAG, deserialize)
{
  auto str = std::string{ "DIAG:1013,DBV,XC\r" };
  auto expected = ds_acomms_sonardyne_avtrak_msgs::DIAG{};
  expected.DBV = true;
  expected.XC = true;

  auto result = ds_acomms_sonardyne_avtrak_msgs::DIAG{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.XC, result.XC);
  EXPECT_EQ(expected.SNR, result.SNR);
  EXPECT_EQ(expected.DBV, result.DBV);
  EXPECT_EQ(expected.DOP, result.DOP);
  EXPECT_EQ(expected.CHAN, result.CHAN);
  EXPECT_EQ(expected.TEL, result.TEL);
  EXPECT_EQ(expected.FEC, result.FEC);
  EXPECT_EQ(expected.CRP, result.CRP);
  EXPECT_EQ(expected.E, result.E);
  EXPECT_EQ(expected.SXC, result.SXC);
  EXPECT_EQ(expected.IFL, result.IFL);
  EXPECT_EQ(expected.MCHAN, result.MCHAN);
}

TEST(DIAG, serialize)
{
  auto expected = std::string{ "DIAG:XC,DBV\r" };
  auto msg = ds_acomms_sonardyne_avtrak_msgs::DIAG{};
  msg.DBV = true;
  msg.XC = true;

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
