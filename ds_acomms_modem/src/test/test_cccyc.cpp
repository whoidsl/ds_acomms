/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/whoi_micromodem_serialization.h"
#include "ds_acomms_micromodem_msgs/CCCYC.h"
#include <gtest/gtest.h>

#if 0
TEST(CCCYC, deserialize)
{

  auto str = std::string{"$CACFG,TXF,1*3B\r\n"};
  auto expected = ds_acomms_micromodem::CACFG{};
  expected.param = "TXF";
  expected.value = 1;

  auto result = ds_acomms_micromodem::CACFG{};

  EXPECT_TRUE(ds_acomms::deserialize(str, result));
  EXPECT_STREQ(expected.param.data(), result.param.data());
  EXPECT_EQ(expected.value, result.value);

}
#endif

TEST(CCCYC, serialize)
{
  auto expected = std::string{ "$CCCYC,0,0,15,1,0,1*6D\r\n" };
  auto msg = ds_acomms_micromodem_msgs::CCCYC{};
  msg.command = 0;
  msg.src_address = 0;
  msg.dest_address = 15;
  msg.ack = false;
  msg.rate = 1;
  msg.num_frames = 1;

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
