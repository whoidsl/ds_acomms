/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/SmsDiagnostics.h"

#include <gtest/gtest.h>

const auto SMS_TEST_STRING = std::string{
  "XC90,SNR44,DBV0,CRP98;4,IFL27,TEL;TCS1;RPSKV2_200,FEC0;0,DOP-0.58;-1;0,"
  "SXC108265472;-85458944;144637952;-44498944;51511296;-53280768;14417920;-277544960;-12255232;-497876992;"
  "-52822016;-703201280;90570752;-826474496;138543104;-598409216;133562368;-385482752;134479872;-178782208;"
  "41943040;-8585216;-6488064;-66650112,"
  "CHAN321664544;37;37;90;150;48;68;457;964;722;383;82;23;93;31;48;49;120;284;425;650;201;17;277;948;377;126;835;"
  "2508;1372;292;377;1683;740;153;1226;3823;3090;1387;274;7;100;256;330;723;591;302;238;56;22;32;19;2;97;473;1033;"
  "2039;1313;688;232;117;2896;9552;21650;39265;28272;14124;4390;316;1318;1073;1392;2068;1336;684;193;7;46;47;59;122;"
  "194;243;403;491;460;316;169;102;59;636;1835;3973;2302;528;311;1588;888;166;777;2590;1547;380;203;1099;758;201;45;"
  "421;506;345;236;125;78;23;2;20;38;196;540;1130;700;245;17;100;117;55;27;42;46;267;887;1708;747;72;394;1865;1534;"
  "621;111;56;88;69;160;388;550;599;788;855;446;153;12;99;121;164;151;158;254;353;517;708;525;313;133;38;164;327;657;"
  "1109;779;377;110;8;46;78;97;111;78;32;22;9;9;9;3;7;10;11;29;35;33;14;1;0;5;9;8;4;1;0;2;2;1;0;4;6;4;1;0;0;0;0;0;0;"
  "0;0;0;0;0;0;0;0;1;1;0;0;0;0;1;1;0;0;2;4;4;2;1;0;0;0;1;2;1;0;0;0;1;1;1;1;0;0;0;0;0;0;0;0"
};

TEST(SmsDiagnostics, deserialize)
{
  auto str = SMS_TEST_STRING;

  auto expected = ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics{};
  expected.XC = 90;
  expected.XC_valid = true;
  expected.SNR = 44;
  expected.SNR_valid = true;
  expected.DBV = 0;
  expected.DBV_valid = true;
  expected.IFL = 27;
  expected.IFL_valid = true;
  expected.CRP = { 98, 4 };
  expected.CRP_valid = true;
  expected.TEL = { "TCS1", "RPSKV2_200" };
  expected.TEL_valid = true;
  expected.FEC = { 0, 0 };
  expected.FEC_valid = true;
  expected.DOP = { -0.58, -1, 0 };
  expected.DOP_valid = true;
  expected.SXC = { 108265472, -85458944,  144637952, -44498944,  51511296, -53280768,  14417920,  -277544960,
                   -12255232, -497876992, -52822016, -703201280, 90570752, -826474496, 138543104, -598409216,
                   133562368, -385482752, 134479872, -178782208, 41943040, -8585216,   -6488064,  -66650112 };
  expected.SXC_valid = true;
  expected.CHAN = {
    321664544, 37,   37,   90,   150, 48,  68,   457,  964,  722,   383,   82,    23,    93,   31,   48,   49,   120,
    284,       425,  650,  201,  17,  277, 948,  377,  126,  835,   2508,  1372,  292,   377,  1683, 740,  153,  1226,
    3823,      3090, 1387, 274,  7,   100, 256,  330,  723,  591,   302,   238,   56,    22,   32,   19,   2,    97,
    473,       1033, 2039, 1313, 688, 232, 117,  2896, 9552, 21650, 39265, 28272, 14124, 4390, 316,  1318, 1073, 1392,
    2068,      1336, 684,  193,  7,   46,  47,   59,   122,  194,   243,   403,   491,   460,  316,  169,  102,  59,
    636,       1835, 3973, 2302, 528, 311, 1588, 888,  166,  777,   2590,  1547,  380,   203,  1099, 758,  201,  45,
    421,       506,  345,  236,  125, 78,  23,   2,    20,   38,    196,   540,   1130,  700,  245,  17,   100,  117,
    55,        27,   42,   46,   267, 887, 1708, 747,  72,   394,   1865,  1534,  621,   111,  56,   88,   69,   160,
    388,       550,  599,  788,  855, 446, 153,  12,   99,   121,   164,   151,   158,   254,  353,  517,  708,  525,
    313,       133,  38,   164,  327, 657, 1109, 779,  377,  110,   8,     46,    78,    97,   111,  78,   32,   22,
    9,         9,    9,    3,    7,   10,  11,   29,   35,   33,    14,    1,     0,     5,    9,    8,    4,    1,
    0,         2,    2,    1,    0,   4,   6,    4,    1,    0,     0,     0,     0,     0,    0,    0,    0,    0,
    0,         0,    0,    0,    0,   1,   1,    0,    0,    0,     0,     1,     1,     0,    0,    2,    4,    4,
    2,         1,    0,    0,    0,   1,   2,    1,    0,    0,     0,     1,     1,     1,    1,    0,    0,    0,
    0,         0,    0,    0,    0
  };
  expected.CHAN_valid = true;

  auto result = ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));
  EXPECT_EQ(expected.XC, result.XC);
  EXPECT_EQ(expected.XC_valid, result.XC_valid);
  EXPECT_EQ(expected.SNR, result.SNR);
  EXPECT_EQ(expected.SNR_valid, result.SNR_valid);
  EXPECT_EQ(expected.DBV, result.DBV);
  EXPECT_EQ(expected.DBV_valid, result.DBV_valid);
  EXPECT_EQ(expected.DOP, result.DOP);
  EXPECT_EQ(expected.DOP_valid, result.DOP_valid);
  EXPECT_EQ(expected.CHAN, result.CHAN);
  EXPECT_EQ(expected.CHAN_valid, result.CHAN_valid);
  EXPECT_EQ(expected.TEL, result.TEL);
  EXPECT_EQ(expected.TEL_valid, result.TEL_valid);
  EXPECT_EQ(expected.FEC, result.FEC);
  EXPECT_EQ(expected.FEC_valid, result.FEC_valid);
  EXPECT_EQ(expected.CRP, result.CRP);
  EXPECT_EQ(expected.CRP_valid, result.CRP_valid);
  EXPECT_EQ(expected.SXC, result.SXC);
  EXPECT_EQ(expected.SXC_valid, result.SXC_valid);
  EXPECT_EQ(expected.IFL, result.IFL);
  EXPECT_EQ(expected.IFL_valid, result.IFL_valid);
}

TEST(SmsDiagnostics, serialize)
{
  auto expected = SMS_TEST_STRING;
  auto msg = ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics{};

  msg.XC = 90;
  msg.XC_valid = true;
  msg.SNR = 44;
  msg.SNR_valid = true;
  msg.DBV = 0;
  msg.DBV_valid = true;
  msg.IFL = 27;
  msg.IFL_valid = true;
  msg.CRP = { 98, 4 };
  msg.CRP_valid = true;
  msg.TEL = { "TCS1", "RPSKV2_200" };
  msg.TEL_valid = true;
  msg.FEC = { 0, 0 };
  msg.FEC_valid = true;
  msg.DOP = { -0.58, -1, 0 };
  msg.DOP_valid = true;
  msg.SXC = { 108265472, -85458944,  144637952, -44498944,  51511296, -53280768,  14417920,  -277544960,
              -12255232, -497876992, -52822016, -703201280, 90570752, -826474496, 138543104, -598409216,
              133562368, -385482752, 134479872, -178782208, 41943040, -8585216,   -6488064,  -66650112 };
  msg.SXC_valid = true;
  msg.CHAN = { 321664544, 37,    37,    90,   150,  48,   68,   457,  964,  722,  383, 82,   23,   93,   31,   48,
               49,        120,   284,   425,  650,  201,  17,   277,  948,  377,  126, 835,  2508, 1372, 292,  377,
               1683,      740,   153,   1226, 3823, 3090, 1387, 274,  7,    100,  256, 330,  723,  591,  302,  238,
               56,        22,    32,    19,   2,    97,   473,  1033, 2039, 1313, 688, 232,  117,  2896, 9552, 21650,
               39265,     28272, 14124, 4390, 316,  1318, 1073, 1392, 2068, 1336, 684, 193,  7,    46,   47,   59,
               122,       194,   243,   403,  491,  460,  316,  169,  102,  59,   636, 1835, 3973, 2302, 528,  311,
               1588,      888,   166,   777,  2590, 1547, 380,  203,  1099, 758,  201, 45,   421,  506,  345,  236,
               125,       78,    23,    2,    20,   38,   196,  540,  1130, 700,  245, 17,   100,  117,  55,   27,
               42,        46,    267,   887,  1708, 747,  72,   394,  1865, 1534, 621, 111,  56,   88,   69,   160,
               388,       550,   599,   788,  855,  446,  153,  12,   99,   121,  164, 151,  158,  254,  353,  517,
               708,       525,   313,   133,  38,   164,  327,  657,  1109, 779,  377, 110,  8,    46,   78,   97,
               111,       78,    32,    22,   9,    9,    9,    3,    7,    10,   11,  29,   35,   33,   14,   1,
               0,         5,     9,     8,    4,    1,    0,    2,    2,    1,    0,   4,    6,    4,    1,    0,
               0,         0,     0,     0,    0,    0,    0,    0,    0,    0,    0,   0,    0,    1,    1,    0,
               0,         0,     0,     1,    1,    0,    0,    2,    4,    4,    2,   1,    0,    0,    0,    1,
               2,         1,     0,     0,    0,    1,    1,    1,    1,    0,    0,   0,    0,    0,    0,    0,
               0 };
  msg.CHAN_valid = true;

  auto result = std::string{};

  ds_acomms_serialization::serialize(msg, result);
  EXPECT_STREQ(expected.data(), result.data());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
