/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/whoi_micromodem_serialization.h"
#include <ds_acomms_micromodem_msgs/CACFG.h>
#include <ds_acomms_micromodem_msgs/CACST.h>
#include <ds_acomms_micromodem_msgs/CACYC.h>
#include <ds_acomms_micromodem_msgs/CADRQ.h>
#include <ds_acomms_micromodem_msgs/CARXD.h>
#include <ds_acomms_micromodem_msgs/CAXST.h>
#include <ds_acomms_micromodem_msgs/CCCYC.h>
#include <ds_acomms_micromodem_msgs/CCTXD.h>

namespace ds_acomms_modem
{
namespace micromodem
{
uint8_t xor_checksum(const std::string& buf)
{
  //ROS_ERROR_STREAM("XOR string: " << buf);
  uint8_t checksum = uint8_t{ 0 };
  for (const auto& it : buf)
  //for (std::string::size_type it = 0; it < buf.size() - 1; ++it)
  {
    checksum ^= static_cast<uint8_t>(it);
    //checksum ^= static_cast<uint8_t>(buf[it]);
  }

  return checksum;
}

bool hexstr_to_bytes(char* hexstr, size_t hexstr_len, std::vector<uint8_t>& bytes)
{
  if (hexstr_len % 2)
  {
    ROS_ERROR("Expected an even number of hex characters, received %lu", hexstr_len);
    return false;
  }

  bytes.resize(hexstr_len / 2);

  std::for_each(std::begin(bytes), std::end(bytes), [&](uint8_t& value) {
    sscanf(hexstr, "%02hhX", &value);
    hexstr += 2;
  });
  return true;
}

template <>
uint8_t modem_message_type(const std::string& msg)
{
  const auto start_pos = msg.find('$');
  if (start_pos == msg.npos)
  {
    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
  }

  if (msg.size() - start_pos < 6)
  {
    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
  }

  const auto prefix_pos = start_pos + 1;
  const auto suffix_pos = prefix_pos + 2;

  // QUESTION: Why does this exist in parallel with DEFINE_MODEM_MESSAGE_TYPE in the header?
#define DEFINE_MESSAGE_CASE(PREFIX, SUFFIX)                                                                            \
  if (msg.compare(suffix_pos, 3, #SUFFIX) == 0)                                                                        \
  {                                                                                                                    \
    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::PREFIX##SUFFIX;                                           \
  }

  if (msg.compare(prefix_pos, 2, "CA") == 0)
  {
    DEFINE_MESSAGE_CASE(CA, CST)
    DEFINE_MESSAGE_CASE(CA, CFG)
    DEFINE_MESSAGE_CASE(CA, CYC)
    DEFINE_MESSAGE_CASE(CA, XST)
    DEFINE_MESSAGE_CASE(CA, RXD)
    DEFINE_MESSAGE_CASE(CA, DRQ)

    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
  }

  if (msg.compare(prefix_pos, 2, "CC") == 0)
  {
    DEFINE_MESSAGE_CASE(CC, CYC)
    DEFINE_MESSAGE_CASE(CC, TXD)
    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
  }

#undef DEFINE_MESSAGE_CASE

  return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
}
}  // namespace micromodem
}  // namespace ds_acomms_modem



namespace ds_acomms_serialization
{
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACYC& msg)
{
  const auto start_offset = buf.find("$CACYC,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  const auto num_parsed = sscanf(buf.data() + start_offset, "$CACYC,%hhu,%hu,%hu,%hhu,%hhu,%hhu*%hhx", &msg.command,
                                 &msg.src_address, &msg.dest_address, &msg.rate, &msg.ack, &msg.num_frames, &checksum);

  if (num_parsed < 6)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CACYC& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto os2 = std::ostringstream{};
  os << "CACYC," << static_cast<int>(msg.command) << "," << static_cast<int>(msg.src_address) << ","
     << static_cast<int>(msg.dest_address) << "," << static_cast<int>(msg.rate) << "," << static_cast<int>(msg.ack)
     << "," << static_cast<int>(msg.num_frames);
  os2.str(os.str());

  os << "*" << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << ds_acomms_modem::micromodem::xor_checksum(os2.str()) << "\r\n";

  buf = "$" + os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CCCYC& msg)
{
  const auto start_offset = buf.find("$CCCYC,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  const auto num_parsed = sscanf(buf.data() + start_offset, "$CCCYC,%hhu,%hu,%hu,%hhu,%hhu,%hhu*%hhx", &msg.command,
                                 &msg.src_address, &msg.dest_address, &msg.rate, &msg.ack, &msg.num_frames, &checksum);

  if (num_parsed < 6)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CCCYC& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto os2 = std::ostringstream{};
  os << "CCCYC," << static_cast<int>(msg.command) << "," << static_cast<int>(msg.src_address) << ","
     << static_cast<int>(msg.dest_address) << "," << static_cast<int>(msg.rate) << "," << static_cast<int>(msg.ack)
     << "," << static_cast<int>(msg.num_frames);
  os2.str(os.str());
  
  os << "*" << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << static_cast<int>(ds_acomms_modem::micromodem::xor_checksum(os2.str())) << "\r\n";

  buf = "$" + os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CARXD& msg)
{
  const auto start_offset = buf.find("$CARXD,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  auto hex_buf = std::array<char, 4096>{};
  hex_buf.fill('\0');

  const auto num_parsed = sscanf(buf.data() + start_offset, "$CARXD,%hu,%hu,%hhu,%hhu,%[0123456789ABCDEFabcdef]s*%hhx",
                                 &msg.src_address, &msg.dest_address, &msg.ack, &msg.frame, hex_buf.data(), &checksum);

  if (num_parsed < 5)
  {
    return false;
  }

  return ds_acomms_modem::micromodem::hexstr_to_bytes(hex_buf.data(), strlen(hex_buf.data()), msg.bytes);
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CARXD& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto os2 = std::ostringstream{};
  os << "CARXD," << static_cast<int>(msg.src_address) << "," << static_cast<int>(msg.dest_address) << ","
     << static_cast<int>(msg.ack) << "," << static_cast<int>(msg.frame) << ",";

  os << std::hex << std::setfill('0') << std::uppercase;
  for (const auto& b : msg.bytes)
  {
    os << std::setw(2) << static_cast<int>(b);
  }
  os2.str(os.str());
  
  os << "*" << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << static_cast<int>(ds_acomms_modem::micromodem::xor_checksum(os2.str())) << "\r\n";

  buf = "$" + os.str();
}
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACFG& msg)
{
  const auto start_offset = buf.find("$CACFG,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  auto data_buf = std::array<char, 3>{};
  data_buf.fill('\0');

  const auto num_parsed =
      sscanf(buf.data() + start_offset, "$CACFG,%3s,%u*%hhx", data_buf.data(), &msg.value, &checksum);

  if (num_parsed < 2)
  {
    return false;
  }

  msg.param = std::string{ data_buf.data() };
  return true;
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CACFG& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto os2 = std::ostringstream{};
  os << "CACFG," << msg.param << "," << static_cast<int>(msg.value);
  os2.str(os.str());

  os << "*" << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << static_cast<int>(ds_acomms_modem::micromodem::xor_checksum(os2.str())) << "\r\n";

  buf = "$" + os.str();
}
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CADRQ& msg)
{
  const auto start_offset = buf.find("$CADRQ,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  auto time_buf = std::array<char, 6>{};
  time_buf.fill('\0');

  const auto num_parsed = sscanf(buf.data() + start_offset, "$CADRQ,%6s,%hu,%hu,%hhu,%hu,%hhu*%hhu", time_buf.data(),
                                 &msg.src_address, &msg.dest_address, &msg.ack, &msg.max_bytes, &msg.frame, &checksum);

  if (num_parsed < 6)
  {
    return false;
  }

  msg.time = std::string{ time_buf.data() };
  return true;
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CADRQ& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto os2 = std::ostringstream{};
  os << "CADRQ," << msg.src_address << "," << msg.dest_address << "," << static_cast<int>(msg.ack) << ","
     << msg.max_bytes << "," << static_cast<int>(msg.frame);
  os2.str(os.str());
  
  os << "*" << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << static_cast<int>(ds_acomms_modem::micromodem::xor_checksum(os2.str())) << "\r\n";

  buf = "$" + os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CCTXD& msg)
{
  const auto start_offset = buf.find("$CCTXD,");
  if (start_offset == buf.npos)
  {
    return false;
  }

  uint8_t checksum;
  auto hex_buf = std::array<char, 1024>{};
  hex_buf.fill('\0');

  const auto num_parsed = sscanf(buf.data() + start_offset, "$CCTXD,%hu,%hu,%hhu,%[0123456789ABCDEFabcdef]s*%hhu",
                                 &msg.src_address, &msg.dest_address, &msg.ack, hex_buf.data(), &checksum);

  if (num_parsed < 4)
  {
    return false;
  }

  return ds_acomms_modem::micromodem::hexstr_to_bytes(hex_buf.data(), strlen(hex_buf.data()), msg.bytes);
}

template <>
void serialize(const ds_acomms_micromodem_msgs::CCTXD& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "CCTXD," << msg.src_address << "," << msg.dest_address << "," << static_cast<int>(msg.ack) << ",";

  os << std::hex << std::setfill('0') << std::uppercase;
  for (const auto& b : msg.bytes)
  {
    os << std::setw(2) << static_cast<int>(b);
  }

#if 0
  os << "*"
     << std::setfill('0') << std::setw(2) << std::uppercase << std::hex
     << static_cast<int>(ds_acomms_micromodem::xor_checksum(os.str()))
     << "\r\n";
#else
  os << "\r\n";
#endif

  buf = "$" + os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACST& msg)
{
  const auto start_offset = buf.find("$CACST,");
  if (start_offset == buf.npos) {
    return false;
  }

  uint8_t checksum;
  auto date_buf = std::array<char, 1024>{};
  date_buf.fill('\0');

  uint8_t mode = 0;
  auto num_parsed = sscanf(buf.data() + start_offset, "$CACST,%hhu", &mode);
  if (num_parsed != 1) {
    return false;
  }

  auto expected_fields = 0;

  if (mode < 6) {
    msg.mm2 = false;
    expected_fields = 30;
    num_parsed =
        sscanf(buf.data() + start_offset, "$CACST,%hhu,%11c,%hhu,%hu,%f,%hd,%f,%hd,%hd,%hu,%hu,%hu,%hhu,%hu,%hu,%hhu,%"
                                          "hhd,%hhu,%hhu,%f,%f,%f,%f,%f,%hhu,%f,%f,%hu,%hu*%hhu",
               &msg.mode, date_buf.data(), &msg.time_of_arrival_mode, &msg.mfd_peak, &msg.mfd_power_db,
               &msg.mfd_ratio_db, &msg.spl_db, &msg.analog_gain, &msg.shift_previous_ain, &msg.shift_current_ain,
               &msg.shift_mfd, &msg.shift_p2b, &msg.rate, &msg.source, &msg.destination, &msg.psk_error_code,
               &msg.packet_type, &msg.num_frames, &msg.num_frames_bad, &msg.rss_db, &msg.snr_input_db,
               &msg.snr_equalizer_output_db, &msg.snr_symbols, &msg.equilizer_mse, &msg.data_quality_factor,
               &msg.doppler, &msg.noise_stdev, &msg.carrier_freq_hz, &msg.bandwidth_hz, &checksum);

    msg.time_of_arrival = std::string{ date_buf.data() };
  }
  else
  {
    msg.mm2 = true;
    expected_fields = 31;

    // Example data from micromodem 2 on Atlantis (firmware 2.0.18189)
    // $CACST,6,0,20191012105536.429019,3,32767,45,0101,0150,100,00,00,00,00,1,000,001,0,3,1,0,150,4.8,5.0,00,-2.0,-01,-0.5,84,10000,2000*4E\r\n
    num_parsed = sscanf(buf.data() + start_offset, "$CACST,%hhu,%hhu,%21c,%hhu,%hu,%f,%hd,%f,%hd,%hd,%hu,%hu,%hu,%hhu,%"
                                                   "hu,%hu,%hhu,%hhd,%hhu,%hhu,%f,%f,%f,%f,%f,%hhu,%f,%f,%hu,%hu*%hhu",
                        &msg.version_number, &msg.mode, date_buf.data(), &msg.time_of_arrival_mode, &msg.mfd_peak,
                        &msg.mfd_power_db, &msg.mfd_ratio_db, &msg.spl_db, &msg.analog_gain, &msg.shift_previous_ain,
                        &msg.shift_current_ain, &msg.shift_mfd, &msg.shift_p2b, &msg.rate, &msg.source,
                        &msg.destination, &msg.psk_error_code, &msg.packet_type, &msg.num_frames, &msg.num_frames_bad,
                        &msg.rss_db, &msg.snr_input_db, &msg.snr_equalizer_output_db, &msg.snr_symbols,
                        &msg.equilizer_mse, &msg.data_quality_factor, &msg.doppler, &msg.noise_stdev,
                        &msg.carrier_freq_hz, &msg.bandwidth_hz, &checksum);

    msg.time_of_arrival = std::string{ date_buf.data() };
  }

  return num_parsed == expected_fields;
}


template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CAXST& msg)
{
  const auto start_offset = buf.find("$CAXST,");
  if (start_offset == buf.npos) {
    return false;
  }

  uint8_t checksum;
  auto date_buf = std::array<char, 1024>{};
  date_buf.fill('\0');
  auto time_buf = std::array<char, 1024>{};
  time_buf.fill('\0');

  uint8_t mode = 0;
  auto num_parsed = sscanf(buf.data() + start_offset, "$CAXST,%hhu", &mode);
  if (num_parsed != 1) {
    return false;
  }

  uint8_t ack = 0;
  auto expected_fields = 0;

  if (mode < 6) {
    msg.mm2 = false;
    expected_fields = 16;
    // TODO: Find example data or test on robot; i don't know for sure that time is %10c
    num_parsed = sscanf(buf.data() + start_offset,
			"$CAXST,%8c,%10c,%hhu,%hhu,%hhu,%hu,%hu,%hhu,"
			"%hu,%hu,%hhu,%hhu,%hhu,%hhd,%hu*%hhu",
			date_buf.data(), time_buf.data(),
			&msg.time_of_transmit_mode, &msg.mode, &msg.probe_length,
			&msg.bandwidth_hz, &msg.carrier_freq_hz, &msg.rate,
			&msg.source, &msg.destination, &ack,
			&msg.num_frames_expected, &msg.num_frames,
			&msg.packet_type, &msg.num_data_bytes, &checksum);

    msg.ack = (ack == 1);
    msg.date = std::string{date_buf.data()};
    msg.time_of_transmit = std::string{time_buf.data()};
  }
  else {
    msg.mm2 = true;
    expected_fields = 17;

    // Example data from micromodem 2 on Atlantis (firmware 2.0.18189)
    // $CAXST,6,20191012,105530.347555,3,0,200,2000,10000,1,1,15,0,1,1,3,64*4D\r\n
    // And, from micromodem 2 testbox:
    // $CAXST,6,19031119,233545.430807,3,0,200,2000,10000,1,0,1,0,1,1,4,0*4E
    num_parsed = sscanf(buf.data() + start_offset,
			"$CAXST,%hhu,%8c,%13c,%hhu,%hhu,%hhu,%hu,%hu,%hhu,"
			"%hu,%hu,%hhu,%hhu,%hhu,%hhd,%hu*%hhu",
			&msg.version_number, date_buf.data(), time_buf.data(),
			&msg.time_of_transmit_mode, &msg.mode, &msg.probe_length,
			&msg.bandwidth_hz, &msg.carrier_freq_hz, &msg.rate,
			&msg.source, &msg.destination, &ack,
			&msg.num_frames_expected, &msg.num_frames,
			&msg.packet_type, &msg.num_data_bytes, &checksum);

    msg.ack = (ack == 1);
    msg.date = std::string{date_buf.data()};
    msg.time_of_transmit = std::string{time_buf.data()};
  }

  return num_parsed == expected_fields;
}


}  // namespace ds_acomms_serializatoin
