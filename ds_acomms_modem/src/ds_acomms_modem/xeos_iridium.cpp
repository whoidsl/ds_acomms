/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/xeos_iridium.h"
#include "ds_acomms_modem/xeos_iridium_serialization.h"

namespace ds_acomms_modem
{
XeosIridium::XeosIridium() : ModemBase()
{
}

XeosIridium::XeosIridium(int argc, char** argv, const std::string& name) : ModemBase(argc, argv, name)
{
}

XeosIridium::~XeosIridium() = default;

void XeosIridium::handleModemMessage(const ds_core_msgs::RawData& bytes)
{
  // Convert to string -- avtrak can only send/receive ASCII messages
  auto msg = std::string{ std::begin(bytes.data), std::end(bytes.data) };

  // Remove trailing line terminators
  msg.erase(msg.find_last_not_of("\r\n") + 1);

  // Look for response to $status.  This indicates whether the beacon thinks it is
  // submerged or on the surface.  e.g.
  // Status:Up=3590 EvtCfg=1 TMde=1 RMde=1 vb=23.906/23.070 Tlt=N;

  // Downlinked (MT) data from email looks like:
  // $unlock 1234
  // $outport A
  // unlocked
  // payload line 1
  // payload line 2
  // ...

  // Add discard by age here

  // Publish data
  const auto payload = std::vector<uint8_t>(std::begin(msg), std::end(msg));
  publishReceivedAcousticModemData((remote_address_), std::move(payload));
}

void XeosIridium::setupParameters()
{
  ModemBase::setupParameters();

  beacon_status_ = BeaconStatus::UNKNOWN;

  remote_address_ = ros::param::param<int>("~remote_address", 100);

  max_message_size_ = ros::param::param<int>("~max_message_size", 260);
  ;  // For 9601 and 9602 Iridium modems max=340bytes. Xeos limits to 260.

  truncate_outgoing_data_ = ros::param::param<bool>("~truncate_outgoing_data", false);

  allow_invalid_data_ = ros::param::param<bool>("~allow_invalid_data", true);

  discard_by_age_ = ros::param::param<bool>("~discard_by_age", true);

  strip_timestamp_ = ros::param::param<bool>("~strip_timestamp", true);

  max_age_seconds_ = ros::param::param<int>("~max_age_seconds", 120);

  invalid_ascii_char_ = '?';
}

void XeosIridium::setupTimers()
{
  ModemBase::setupTimers();

  const auto period = ros::Duration(ros::param::param<int>("~poll_beacon_period", 120));
  poll_beacon_timer_ = nodeHandle().createTimer(ros::Duration(period), &XeosIridium::pollBeaconCallback, this);
}

void XeosIridium::pollBeaconCallback(const ros::TimerEvent&)
{
  modemConnection()->send("$iMsgChk\r");
  modemConnection()->send("$diag 1\r$status\r");
}

int XeosIridium::send(uint16_t remote_address, std::vector<uint8_t> bytes)
{
  // Check size
  if (bytes.size() > max_message_size_)
  {
    if (!truncate_outgoing_data_)
    {
      ROS_ERROR_STREAM("Unable to send message of size " << bytes.size() << " bytes.   Max allowed is "
                                                         << max_message_size_);
      return ErrorCode::MESSAGE_TOO_LARGE;
    }

    ROS_WARN_STREAM("Truncating message to " << max_message_size_ << " bytes (from " << bytes.size() << ")");
    bytes.resize(max_message_size_);
  }

  // Xeos serial relay require ASCII data.  Whether we reject the data or replace illegal
  // characters is set by the user.
  bool illegal_char_present = false;
  std::for_each(std::begin(bytes), std::end(bytes), [&](uint8_t& value) {
    if ((value < 32) || (value > 126))
    {
      ROS_ERROR_STREAM(std::setfill('0') << "Message contains invalid character (hex): 0x" << std::setw(2) << std::hex
                                         << std::uppercase << static_cast<int>(value));

      illegal_char_present = true;
      value = static_cast<uint8_t>(invalid_ascii_char_);
    }
  });

  // Return an error if invalid characters were present and we're not set to allow them
  if (illegal_char_present && !allow_invalid_data_)
  {
    return ErrorCode::NON_ASCII_DATA;
  }

  // Create serial relay header
  auto os = std::ostringstream{};
  os << "$sendSBD\r" << std::setw(4) << std::setfill('0') << remote_address;

  // Iridium payload
  os << std::string{ std::begin(bytes), std::end(bytes) };

  os << "\r";

  os << "$finished";

  const auto message_str = os.str() + "\r";

  auto modem = modemConnection();
  if (!modem)
  {
    ROS_ERROR_STREAM("Cannot send data, no modem I/O connection.");
    return ErrorCode::NO_MODEM_CONNECTION;
  }

  ROS_INFO_STREAM("Sending: '" << message_str << "'");
  modem->send(message_str);

  return ErrorCode::OK;
}
}
