/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/DIAG.h"
#include "ds_acomms_sonardyne_avtrak_msgs/FFT.h"
#include "ds_acomms_sonardyne_avtrak_msgs/SMS.h"
#include "ds_acomms_sonardyne_avtrak_msgs/SmsDiagnostics.h"

#include <boost/algorithm/string.hpp>
#include <regex>

namespace ds_acomms_modem
{
namespace sonardyne_avtrak
{
template <>
uint8_t modem_message_type(const std::string& msg)
{
  if (msg.compare(0, 3, "SMS") == 0)
  {
    return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::SMS;
  }
  if (msg.compare(0, 4, "DIAG") == 0)
  {
    return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::DIAG;
  }

  if (msg.compare(0, 3, "FFT") == 0)
  {
    return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::FFT;
  }

  return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::INVALID_MESSAGE;
}
}
}

namespace ds_acomms_serialization
{
template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::DIAG& msg)
{
  const auto start_offset = buf.find("DIAG:");
  if (start_offset == buf.npos)
  {
    return false;
  }

  msg = ds_acomms_sonardyne_avtrak_msgs::DIAG{};

  auto diag_fields = std::vector<std::string>{};
  if (start_offset == 0)
  {
    boost::algorithm::split(diag_fields, buf, boost::is_any_of(","), boost::token_compress_on);
  }
  else
  {
    const auto tmp = buf.substr(start_offset);
    boost::algorithm::split(diag_fields, tmp, boost::is_any_of(","), boost::token_compress_on);
  }

  if (diag_fields.size() == 0)
  {
    return false;
  }

  if (diag_fields.size() == 1)
  {
    return true;
  }

#define CHECK_FIELD(NAME)                                                                                              \
  if (it.compare(#NAME) == 0)                                                                                          \
  {                                                                                                                    \
    msg.NAME = true;                                                                                                   \
    continue;                                                                                                          \
  }

  for (auto& it : diag_fields)
  {
    boost::algorithm::trim(it);
    CHECK_FIELD(XC)
    CHECK_FIELD(SNR)
    CHECK_FIELD(DBV)
    CHECK_FIELD(DOP)
    CHECK_FIELD(CHAN)
    CHECK_FIELD(TEL)
    CHECK_FIELD(FEC)
    CHECK_FIELD(CRP)
    CHECK_FIELD(E)
    CHECK_FIELD(SXC)
    CHECK_FIELD(IFL)
    CHECK_FIELD(MCHAN)
  }
#undef CHECK_FIELD

  return true;
}

template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::DIAG& msg, std::string& buf)
{
  auto fields = std::vector<std::string>{};

#define ADD_FIELD(NAME)                                                                                                \
  if (msg.NAME)                                                                                                        \
  {                                                                                                                    \
    fields.push_back(#NAME);                                                                                           \
  }

  ADD_FIELD(XC)
  ADD_FIELD(SNR)
  ADD_FIELD(DBV)
  ADD_FIELD(DOP)
  ADD_FIELD(CHAN)
  ADD_FIELD(TEL)
  ADD_FIELD(FEC)
  ADD_FIELD(CRP)
  ADD_FIELD(E)
  ADD_FIELD(SXC)
  ADD_FIELD(IFL)
  ADD_FIELD(MCHAN)

#undef ADD_FIELD

  auto os = std::ostringstream{};
  os << "DIAG";
  if (fields.empty())
  {
    buf = os.str() + "\r";
    return;
  }

  auto it = fields.cbegin();
  os << ":" << *it;
  while (++it != fields.cend())
  {
    os << "," << *it;
  }

  os << "\r";
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg)
{
  auto fields = std::vector<std::string>{};
  boost::algorithm::split(fields, buf, boost::is_any_of(","), boost::token_compress_on);

  auto values = std::vector<std::string>{};

  auto field_name_regex = std::regex("([A-Z]+)");

  auto ok = true;

#define ADD_SINGLE(NAME, VALUE, CONV)                                                                                  \
  try                                                                                                                  \
  {                                                                                                                    \
    msg.NAME = CONV(VALUE);                                                                                            \
    msg.NAME##_valid = true;                                                                                           \
  }                                                                                                                    \
  catch (const std::invalid_argument& e)                                                                               \
  {                                                                                                                    \
    ok = false;                                                                                                        \
  }

#define ADD_ARRAY(NAME, VALUES, CONV)                                                                                  \
  msg.NAME.resize(VALUES.size());                                                                                      \
  msg.NAME##_valid = true;                                                                                             \
  for (auto i = 0; i < VALUES.size(); ++i)                                                                             \
  {                                                                                                                    \
    try                                                                                                                \
    {                                                                                                                  \
      msg.NAME[i] = CONV(VALUES[i]);                                                                                   \
    }                                                                                                                  \
    catch (std::invalid_argument & e)                                                                                  \
    {                                                                                                                  \
      msg.NAME##_valid = false;                                                                                        \
      ok = false;                                                                                                      \
      break;                                                                                                           \
    }                                                                                                                  \
  }

  for (auto& f : fields)
  {
    values.clear();

    auto s_match = std::smatch{};
    if (!std::regex_search(f, s_match, field_name_regex))
    {
      continue;
    }

    if (s_match.size() < 2)
    {
      continue;
    }

    const auto name = s_match[1].str();

    f = f.substr(name.length());

    boost::algorithm::split(values, f, boost::is_any_of(";"), boost::token_compress_on);

    if (values.empty())
    {
      continue;
    }

    if (name == "SNR")
    {
      ADD_SINGLE(SNR, values.at(0), std::stoi);
    }

    else if (name == "XC")
    {
      ADD_SINGLE(XC, values.at(0), std::stoi)
    }

    else if (name == "DBV")
    {
      ADD_SINGLE(DBV, values.at(0), std::stoi);
    }

    else if (name == "IFL")
    {
      ADD_SINGLE(IFL, values.at(0), std::stoi);
    }

    else if (name == "CHAN")
    {
      ADD_ARRAY(CHAN, values, std::stoi);
    }

    else if (name == "SXC")
    {
      ADD_ARRAY(SXC, values, std::stoi);
    }

    else if (name == "DOP")
    {
      ADD_ARRAY(DOP, values, std::stod);
    }

    else if (name == "FEC")
    {
      ADD_ARRAY(FEC, values, std::stoi);
    }

    else if (name == "CRP")
    {
      ADD_ARRAY(CRP, values, std::stoi);
    }

    else if (name == "TEL")
    {
      std::copy(std::next(std::begin(values), 1), std::end(values), std::back_inserter(msg.TEL));
      msg.TEL_valid = true;
    }
  }

#undef ADD_SINGLE
#undef ADD_ARRAY

  return ok;
}

template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg, std::string& buf)
{
  auto is_first_field = true;
  auto os = std::ostringstream();

#define ADD_SINGLE(NAME)                                                                                               \
  if (msg.NAME##_valid)                                                                                                \
  {                                                                                                                    \
    if (!is_first_field)                                                                                               \
    {                                                                                                                  \
      os << ",";                                                                                                       \
    }                                                                                                                  \
    os << #NAME << msg.NAME;                                                                                           \
    is_first_field = false;                                                                                            \
  }

#define ADD_ARRAY(NAME)                                                                                                \
  if (msg.NAME##_valid && !msg.NAME.empty())                                                                           \
  {                                                                                                                    \
    if (!is_first_field)                                                                                               \
    {                                                                                                                  \
      os << ",";                                                                                                       \
    }                                                                                                                  \
    os << #NAME;                                                                                                       \
    if (!strcmp(#NAME, "TEL"))                                                                                         \
    {                                                                                                                  \
      os << ";";                                                                                                       \
    }                                                                                                                  \
    os << msg.NAME[0];                                                                                                 \
    for (auto i = 1; i < msg.NAME.size(); ++i)                                                                         \
    {                                                                                                                  \
      os << ";" << msg.NAME[i];                                                                                        \
    }                                                                                                                  \
  }

  ADD_SINGLE(XC);
  ADD_SINGLE(SNR);
  ADD_SINGLE(DBV);
  ADD_ARRAY(CRP);
  ADD_SINGLE(IFL);
  ADD_ARRAY(TEL);
  ADD_ARRAY(FEC);
  ADD_ARRAY(DOP);
  ADD_ARRAY(SXC);
  ADD_ARRAY(CHAN);

#undef ADD_ARRAY
#undef ADD_SINGLE

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::FFT& msg)
{
  const auto fft_regex = std::regex(R"(FFT:(\d+)?(?:,E\d,)?N(\d+),F(\d+),A(\d+)(,GAIN(\d+))?(,(\S+))?[\r\n]*)");

  auto smatch = std::smatch{};
  if (!std::regex_match(buf, smatch, fft_regex))
  {
    return false;
  }

  if (smatch.size() < 5)
  {
    return false;
  }

  if (smatch[1].matched)
  {
    msg.address = static_cast<std::uint16_t>(std::stoi(smatch[1].str()));
  }
  else
  {
    msg.address = 0;
  }

  msg.n_points = static_cast<std::uint16_t>(std::stoi(smatch[2].str()));
  msg.frequency_enum = static_cast<unsigned char>(std::stoi(smatch[3].str()));
  msg.averages = static_cast<unsigned char>(std::stoi(smatch[4].str()));
  if (smatch[6].matched)
  {
    msg.gain = static_cast<unsigned char>(std::stoi(smatch[6].str()));
  }

  if (smatch[8].matched)
  {
    msg.fft.clear();
    const auto num_regex = std::regex(R"(-?\d+(\.\d+))");
    const auto field = smatch[8].str();
    std::transform(std::sregex_token_iterator(std::begin(field), std::end(field), num_regex, 0),
                   std::sregex_token_iterator(), std::back_inserter(msg.fft),
                   [](const std::string& s) { return std::stof(s); });
  }

  return true;
}

template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::FFT& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "FFT:";
  if (msg.address > 0)
  {
    os << msg.address << ",";
  }

  if (!msg.fft.empty())
  {
    os << "E0,";
  }
  os << "N" << msg.n_points << ",F" << static_cast<int>(msg.frequency_enum) << ",A" << static_cast<int>(msg.averages);
  if (msg.gain > 0)
  {
    os << ",GAIN" << static_cast<int>(msg.gain);
  }

  if (!msg.fft.empty())
  {
    os << ",";
    os << std::setprecision(1) << std::fixed;
    os << msg.fft.at(0);
    for (auto i = 1; i < msg.fft.size(); ++i)
    {
      os << ";" << msg.fft.at(i);
    }
  }

  os << "\r";
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::SMS& msg)
{
  const auto sms_regex = std::regex(R"(SMS:(\d+);?([^\|\[]+)?(\[([^\]]+)\])?(\|([^\r]+))?[\r\n]*)");
  //                                                  ^            ^              ^
  // Non-DIAG related parameters (W#,B#, etc) --------'            |              |
  // DIAGnostics related parameters -------------------------------'              |
  // SMS Data message ------------------------------------------------------------'

  auto smatch = std::smatch{};
  if (!std::regex_match(buf, smatch, sms_regex))
  {
    return false;
  }

  // Grab the remote address
  msg.remote_addr = static_cast<std::uint16_t>(std::stoul(smatch[1].str()));

  const auto param_regex = std::regex(R"(([A-Z]+)([^,]+))");
  const auto value_regex = std::regex(R"([^;]+)");
  auto param_smatch = std::smatch{};

  // match non-diagnostics header parameters first.  We'll loop through all of the CSV parameters here.  All parameters
  // are scalar.
  const auto& sms_params = smatch[2];
  if (sms_params.matched)
  {
    // Loop over the parameters.
    for (auto param_it = std::sregex_token_iterator(sms_params.first, sms_params.second, param_regex, 0);
         param_it != std::sregex_token_iterator(); param_it++)
    {
      // Split the match into name/value pairs.
      if (!std::regex_match(param_it->first, param_it->second, param_smatch, param_regex))
      {
        continue;
      }

      auto param_name = param_smatch[1].str();
      const auto param_value = param_smatch[2].str();

      // We only care about the range 'R' value for now.
      if (param_name == "R")
      {
        msg.range = std::stoul(param_value);
      }
      if (param_name == "W")
      {
        // ignored
      }
      else if (param_name == "A")
      {
        // ignored
      }
      else if (param_name == "B")
      {
        // ignored
      }
      else if (param_name == "WKT")
      {
        // ignored
      }
      else if (param_name == "C")
      {
        // ignored
      }
      else if (param_name == "P")
      {
        // ignored
      }
      else
      {
        ROS_WARN("Unexpected SMS parameter: %s", param_name.data());
      }
    }
  }

  // match diagnostic parameters
  const auto& diagnostics = smatch[4];
  msg.diagnostics_valid = diagnostics.matched;
  if (diagnostics.matched)
  {
    // Again, loop over parameters
    for (auto param_it = std::sregex_token_iterator(diagnostics.first, diagnostics.second, param_regex, 0);
         param_it != std::sregex_token_iterator(); param_it++)
    {
      // Then split the parameters into name/value pairs.
      if (!std::regex_match(param_it->first, param_it->second, param_smatch, param_regex))
      {
        continue;
      }

      auto param_name = param_smatch[1].str();
      // Convert name to lowercase to help with the message field macro below.
      std::transform(std::begin(param_name), std::end(param_name), std::begin(param_name),
                     [](const char c) { return std::tolower(c); });

      const auto& param_values = param_smatch[2];

      // This macro takes a field name and conversion function.  It will populate the propper message field with
      // converted values.
#define ADD_VALUES(NAME, CONV)                                                                                         \
      if (param_name == #NAME)                                                                                         \
      {                                                                                                                \
        std::transform(std::sregex_token_iterator(param_values.first, param_values.second, value_regex, 0),            \
                       std::sregex_token_iterator(), std::back_inserter(msg.NAME),                                     \
                       [](const std::string& v) { return CONV(v); });                                                  \
        continue;                                                                                                      \
      }

      ADD_VALUES(xc, std::stoi);
      ADD_VALUES(snr, std::stoi);
      ADD_VALUES(dbv, std::stoi);
      ADD_VALUES(dop, std::stof);
      ADD_VALUES(chan, std::stoi);
      ADD_VALUES(sxc, std::stoi);
      ADD_VALUES(fec, std::stoi);
      ADD_VALUES(crp, std::stoi);
      ADD_VALUES(ifl, std::stoi);
      ADD_VALUES(tel, [](const std::string& s) { return s; });
#undef ADD_VALUES
    }
  }

  // Save SMS message data.
  if (smatch[6].matched)
  {
    msg.data = smatch[6].str();
  }

  return true;
}

template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::SMS& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "SMS:" << msg.remote_addr;

  if (!msg.data.empty())
  {
    os << "|" << msg.data;
  }

  buf = os.str();
  if (buf.at(buf.length()-1) != '\r')
  {
    buf += '\r';
  }
}
}
