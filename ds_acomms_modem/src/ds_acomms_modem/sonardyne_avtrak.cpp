/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/sonardyne_avtrak.h"
#include "ds_acomms_modem/sonardyne_avtrak_serialization.h"
#include <ds_acomms_msgs/AcousticModemData.h>
#include <ds_acomms_sonardyne_avtrak_msgs/SMS.h>
#include <ds_acomms_sonardyne_avtrak_msgs/FFT.h>

#include <regex>

namespace ds_acomms_modem
{
struct SonardyneAvtrakPrivate
{
  char invalid_ascii_char_ = '?';
  bool invalid_data_allowed_ = true;
  bool truncate_outgoing_data_enabled_ = true;
  bool buffer_outgoing_ = false;
  uint32_t max_message_size_ = 128;
  int sms_code_ = 0;
  bool enable_ranging_ = 0;
  size_t minimum_sms_size_ = 0;

  std::string extra_sms_params_ = std::string{};

  const std::vector<std::string> status_commands_ = { "VS\r", "FV\r",   "MS\r",   "CS\r",
                                                      "SC\r", "SENS\r", "DIAG\r", "FFT:N256,A16,F0\r" };
  size_t status_command_index_ = 0;
  ros::Duration status_command_poll_interval_ = ros::Duration(0);
  ros::Timer status_command_timer_;

  ds_acomms_sonardyne_avtrak_msgs::DIAG diagnostic_flags_;
  ros::Publisher sms_pub_;
  ros::Publisher fft_pub_;
};

SonardyneAvtrak::SonardyneAvtrak()
  : ModemBase(), d_ptr_(std::unique_ptr<SonardyneAvtrakPrivate>(new SonardyneAvtrakPrivate))
{
}

SonardyneAvtrak::SonardyneAvtrak(int argc, char** argv, const std::string& name)
  : ModemBase(argc, argv, name), d_ptr_(std::unique_ptr<SonardyneAvtrakPrivate>(new SonardyneAvtrakPrivate))
{
}

SonardyneAvtrak::~SonardyneAvtrak() = default;

bool SonardyneAvtrak::allowInvalidData() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->invalid_data_allowed_;
}

void SonardyneAvtrak::enableAllowInvalidData(bool enabled)
{
  DS_D(SonardyneAvtrak);
  d->invalid_data_allowed_ = enabled;
}

void SonardyneAvtrak::enableDiagonstics(const ds_acomms_sonardyne_avtrak_msgs::DIAG& diag)
{
  DS_D(SonardyneAvtrak);
  d->diagnostic_flags_ = diag;

  auto buf = std::string{};
  ds_acomms_serialization::serialize(diag, buf);

  auto modem = modemConnection();
  modem->send(buf);
}

ds_acomms_sonardyne_avtrak_msgs::DIAG SonardyneAvtrak::diagnostics() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->diagnostic_flags_;
}

char SonardyneAvtrak::invalidAsciiChar() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->invalid_ascii_char_;
}

bool SonardyneAvtrak::setInvalidAsciiChar(char value)
{
  DS_D(SonardyneAvtrak);
  if ((value < 32) || (value > 126))
  {
    return false;
  }

  d->invalid_ascii_char_ = value;
  return true;
}

bool SonardyneAvtrak::truncateOutgoingData() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->truncate_outgoing_data_enabled_;
}

void SonardyneAvtrak::enableTruncateOutgoingData(bool enabled)
{
  DS_D(SonardyneAvtrak);
  d->truncate_outgoing_data_enabled_ = enabled;
}

bool SonardyneAvtrak::bufferOutgoingData() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->buffer_outgoing_;
}

void SonardyneAvtrak::enableBufferOutgoingData(bool enabled)
{
  DS_D(SonardyneAvtrak);
  d->buffer_outgoing_ = enabled;
}

size_t SonardyneAvtrak::minimumSmsSize() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->minimum_sms_size_;
}

void SonardyneAvtrak::setMinimumSmsSize(size_t size)
{
  DS_D(SonardyneAvtrak);
  d->minimum_sms_size_ = size;
}

std::string SonardyneAvtrak::extraSmsParameters() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->extra_sms_params_;
}

void SonardyneAvtrak::setExtraSmsParameters(std::string params)
{
  DS_D(SonardyneAvtrak);
  d->extra_sms_params_ = std::move(params);
}
int SonardyneAvtrak::smsCode() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->sms_code_;
}

bool SonardyneAvtrak::setSmsCode(int code)
{
  DS_D(SonardyneAvtrak);
  switch (code)
  {
    case SMS_STD_EXCHANGE:
    case SMS_ACK_ONLY:
    case SMS_NO_RESPONSE:
      d->sms_code_ = code;
      return true;
    default:
      ROS_ERROR_STREAM("Invalid 'sms_code' value: " << code);
      return false;
  }
}

bool SonardyneAvtrak::smsRanging() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->enable_ranging_;
}

void SonardyneAvtrak::enableSmsRanging(bool enabled)
{
  DS_D(SonardyneAvtrak);
  d->enable_ranging_ = enabled;
}

ros::Duration SonardyneAvtrak::statusPollInterval() const noexcept
{
  const DS_D(SonardyneAvtrak);
  return d->status_command_poll_interval_;
}

void SonardyneAvtrak::setStatusPollInterval(ros::Duration duration)
{
  if (duration.toSec() < 0)
  {
    duration.fromSec(0);
  }

  if (duration == statusPollInterval())
  {
    return;
  }

  DS_D(SonardyneAvtrak);
  d->status_command_timer_.stop();
  d->status_command_timer_.setPeriod(duration);
  if (duration.toSec() > 0)
  {
    d->status_command_timer_.start();
  }
}
int SonardyneAvtrak::send(uint16_t remote_address, std::vector<uint8_t> bytes)
{
  DS_D(SonardyneAvtrak);

  // Check size
  if (bytes.size() > d->max_message_size_)
  {
    if (!truncateOutgoingData())
    {
      ROS_ERROR_STREAM("Unable to send message of size " << bytes.size() << " bytes.   Max allowed is "
                                                         << d->max_message_size_);
      return ErrorCode::MESSAGE_TOO_LARGE;
    }

    ROS_WARN_STREAM("Truncating message to " << d->max_message_size_ << " bytes (from " << bytes.size() << ")");
    bytes.resize(d->max_message_size_);
  }

  // Avtrak's require ASCII data.  Whether we reject the data or replace illegal
  // characters is set by the user.
  bool illegal_char_present = false;
  std::for_each(std::begin(bytes), std::end(bytes), [&](uint8_t& value) {
    if ((value < 32) || (value > 126))
    {
      ROS_ERROR_STREAM(std::setfill('0') << "Message contains invalid character (hex): 0x" << std::setw(2) << std::hex
                                         << std::uppercase << static_cast<int>(value));

      illegal_char_present = true;
      value = static_cast<uint8_t>(d->invalid_ascii_char_);
    }
  });

  // Return an error if invalid characters were present and we're not set to allow them
  if (illegal_char_present && !allowInvalidData())
  {
    return ErrorCode::NON_ASCII_DATA;
  }

  // Create SMS header
  auto os = std::ostringstream{};
  os << "SMS:" << std::setw(4) << std::setfill('0') << remote_address;

  os << ",B" << std::noboolalpha << d->buffer_outgoing_;
  os << ",R" << std::noboolalpha << d->enable_ranging_;
  os << ",A" << static_cast<int>(d->sms_code_);

  if (!d->extra_sms_params_.empty())
  {
    os << "," << d->extra_sms_params_;
  }

  // SMS  message start indicator
  os << "|";

  // SMS Payload
  os << std::string{ std::begin(bytes), std::end(bytes) };

  // Pad with blank spaces if required
  const auto required_padding = minimumSmsSize() - os.tellp();
  if (required_padding > 0)
  {
    ROS_DEBUG_STREAM("Padding SMS message by " << required_padding << " to reach length " << minimumSmsSize());
    os << std::setw(required_padding) << std::setfill(' ') << " ";
  }

  const auto message_str = os.str() + "\r";

  auto modem = modemConnection();
  if (!modem)
  {
    ROS_ERROR_STREAM("Cannot send data, no modem I/O connection.");
    return ErrorCode::NO_MODEM_CONNECTION;
  }

  ROS_INFO_STREAM("Sending: '" << message_str << "'");
  modem->send(message_str);

  return ErrorCode::OK;
}

void SonardyneAvtrak::handleModemMessage(const ds_core_msgs::RawData& bytes)
{
  // Convert to string -- avtrak can only send/receive ASCII messages
  auto msg = std::string{ std::begin(bytes.data), std::end(bytes.data) };

  // Remove preceeding direction indicators
  msg.erase(0, msg.find_first_not_of("<>"));

  // Remove trailing line terminators
  msg.erase(msg.find_last_not_of("\r\n") + 1);

  const auto message_type = sonardyne_avtrak::modem_message_type(msg);

  switch (message_type)
  {
    case ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::SMS:
    {
      if (msg == "SMS:OK")
      {
        ROS_DEBUG_STREAM("Skipping SMS:OK Acknowledgement");
        return;
      }

      auto sms = ds_acomms_sonardyne_avtrak_msgs::SMS{};
      if (!ds_acomms_serialization::deserialize(msg, sms))
      {
        ROS_WARN_STREAM("Failed to parse SMS message: " << msg);
        return;
      }
      sms.local_addr = localAddress();

      DS_D(SonardyneAvtrak);
      d->sms_pub_.publish(sms);

      if (!sms.data.empty())
      {
        const auto payload = std::vector<uint8_t>(std::begin(sms.data), std::end(sms.data));
        publishReceivedAcousticModemData(sms.remote_addr, std::move(payload));
      }
      return;
    }
    case ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::FFT:
    {
      auto fft = ds_acomms_sonardyne_avtrak_msgs::FFT{};
      if (!ds_acomms_serialization::deserialize(msg, fft))
      {
        ROS_WARN_STREAM("Failed to parse FFT message: " << msg);
        return;
      }
      DS_D(SonardyneAvtrak);
      d->fft_pub_.publish(fft);
      return;
    }

    case ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::DIAG:
    default:
    {
      ROS_DEBUG_STREAM("Ignoring message: " << msg);
      return;
    }
  }
}

void SonardyneAvtrak::setupParameters()
{
  ModemBase::setupParameters();

  const auto extra_parameters = ros::param::param<std::string>("~sms_parameters", std::string{});
  setExtraSmsParameters(std::move(extra_parameters));

  auto int_param = ros::param::param<int>("~sms_code", 0);
  setSmsCode(int_param);

  int_param = ros::param::param<int>("~minimum_sms_size", 0);
  if (int_param > 0)
  {
    setMinimumSmsSize(static_cast<size_t>(int_param));
  }

  auto bool_param = ros::param::param<bool>("~enable_ranging", false);
  enableSmsRanging(bool_param);

  bool_param = ros::param::param<bool>("~buffer_messages", false);
  enableBufferOutgoingData(bool_param);
}

void SonardyneAvtrak::setupTimers()
{
  ModemBase::setupTimers();

  DS_D(SonardyneAvtrak);
  d->status_command_timer_ =
      nodeHandle().createTimer(ros::Duration(0), &SonardyneAvtrak::statusTimerCallback, this, false, false);

  const auto period = ros::Duration(ros::param::param<int>("~status_poll_period", 0));
  setStatusPollInterval(period);
}

void SonardyneAvtrak::statusTimerCallback(const ros::TimerEvent&)
{
  DS_D(SonardyneAvtrak);
  modemConnection()->send(d->status_commands_.at(d->status_command_index_));
  d->status_command_index_ = (d->status_command_index_ + 1) % d->status_commands_.size();
}

void SonardyneAvtrak::setupConnections()
{
  ModemBase::setupConnections();

  if (ros::param::has("~diagnostics"))
  {
    auto diag_flags = std::vector<std::string>{};
    ros::param::get("~diagnostics", diag_flags);

    auto diag_msg = ds_acomms_sonardyne_avtrak_msgs::DIAG{};

    for (const auto& it : diag_flags)
    {
#define SET_DIAG(NAME)                                                                                                 \
  if (it == #NAME)                                                                                                     \
  {                                                                                                                    \
    diag_msg.NAME = true;                                                                                              \
    continue;                                                                                                          \
  }
      SET_DIAG(XC)
      SET_DIAG(SNR)
      SET_DIAG(DBV)
      SET_DIAG(DOP)
      SET_DIAG(CHAN)
      SET_DIAG(TEL)
      SET_DIAG(FEC)
      SET_DIAG(CRP)
      SET_DIAG(E)
      SET_DIAG(SXC)
      SET_DIAG(IFL)
      SET_DIAG(MCHAN)
#undef SET_DIAG
    }
    enableDiagonstics(diag_msg);
  }
}

void SonardyneAvtrak::setup()
{
  ModemBase::setup();
  // Clear out any tx buffer contents.  The remote address isn't important
  const auto init_msg = std::string{ "INITIALIZING AVTRAK TX BUFFER" };
  send(509, std::vector<uint8_t>{ std::begin(init_msg), std::end(init_msg) });
}

void SonardyneAvtrak::setupPublishers()
{
  ModemBase::setupPublishers();

  DS_D(SonardyneAvtrak);
  auto topic_name = ros::names::resolve(ros::this_node::getName(), std::string{ "sms_rx" });
  auto nh = nodeHandle();
  d->sms_pub_ = nh.advertise<ds_acomms_sonardyne_avtrak_msgs::SMS>(topic_name, 1, false);

  topic_name = ros::names::resolve(ros::this_node::getName(), std::string{ "fft" });
  d->fft_pub_ = nh.advertise<ds_acomms_sonardyne_avtrak_msgs::FFT>(topic_name, 1, false);
};
}
