/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_msgs/AcousticModemData.h"
#include "ds_acomms_modem/whoi_micromodem.h"
#include "ds_acomms_modem/whoi_micromodem_serialization.h"

#include <ds_acomms_micromodem_msgs/CACFG.h>
#include <ds_acomms_micromodem_msgs/CACYC.h>
#include <ds_acomms_micromodem_msgs/CADRQ.h>
#include <ds_acomms_micromodem_msgs/CARXD.h>
#include <ds_acomms_micromodem_msgs/CCCYC.h>
#include <ds_acomms_micromodem_msgs/CCTXD.h>
#include <ds_acomms_micromodem_msgs/CACST.h>
#include <ds_acomms_micromodem_msgs/CAXST.h>

namespace ds_acomms_modem
{
struct WhoiMicromodemPrivate
{
  WhoiMicromodem::Rate default_rate_ = WhoiMicromodem::Rate::BCH_128_8;
  // How many frames we have queued up from an incoming message but
  // not yet sent in response to a CADRQ.
  uint8_t num_unsent_frames_ = 0;
  uint8_t expected_total_rx_frames_ = 0;
  uint8_t next_expected_rx_frame_ = 0;
  // Remote address that sent to-be-published data.
  uint16_t remote_address_;
  // Rate at which to-be-published data was received.
  uint8_t receive_rate_;
  // Whether the packet corresponding to most-recently-received CACYC
  // (for incoming data) has been published.
  bool unsent_packet_ = false;
  // packet that will be sent to the modem
  WhoiMicromodem::Packet tx_packet_;
  // packet that is received from the modem
  WhoiMicromodem::Packet rx_packet_;
  // Publishers for status/request messages received from micromodem
  ros::Publisher cacst_pub_;  // Cycle receive statistics
  ros::Publisher cacyc_pub_;  // Echo cycle initialization command
  ros::Publisher cadrq_pub_;  // Data request from modem
  ros::Publisher caxst_pub_;  // Cycle transmit statistics
  // Publisher used for ds_acomms_msgs::MicromodemData
  ros::Publisher micromodem_pub_;
  // Subscriber used for ds_acomms_msgs::MicromodemData
  ros::Subscriber micromodem_sub_;
  // Timer started on receipt of cacyc message in order to ensure that the
  // resulting packet is published even if the last frame is dropped.
  ros::Timer cacyc_timer_;
  // How long after receipt of CACYC message to publish corresponding packet.
  float cacyc_timeout_;
};

WhoiMicromodem::WhoiMicromodem()
  : ModemBase(), d_ptr_(std::unique_ptr<WhoiMicromodemPrivate>(new WhoiMicromodemPrivate))
{
}

WhoiMicromodem::WhoiMicromodem(int argc, char** argv, const std::string& name)
  : ModemBase(argc, argv, name), d_ptr_(std::unique_ptr<WhoiMicromodemPrivate>(new WhoiMicromodemPrivate))
{
}

WhoiMicromodem::~WhoiMicromodem() = default;

void WhoiMicromodem::handleMicromodemData(const ds_acomms_msgs::MicromodemData& msg) {
  // For compatability with ModemBase, only transmit if local addr matches
  // (I guess this supports having multiple modems subscribing to the
  // same transmit topic? But why?)
  if (msg.local_addr != localAddress()) {
    ROS_WARN_STREAM("Ignoring message with local_addr = " << msg.local_addr
		    <<", since driver's addr is " << localAddress());
    return;
  }

  Packet packet;
  ROS_INFO_STREAM("Constructing packet with " << msg.frames.size() << " frames ...");
  for (auto msg_frame : msg.frames) {
    Frame frame;
    std::move(std::begin(msg_frame.payload), std::end(msg_frame.payload),
	      std::back_inserter(frame));
    ROS_INFO_STREAM("...added frame, length = " << frame.size());
    packet.push_back(frame);
  }

  sendPacket(msg.remote_addr, packet, static_cast<Rate>(msg.rate));
}

void WhoiMicromodem::handleModemMessage(const ds_core_msgs::RawData& bytes)
{
  if (bytes.data.empty()) {
    return;
  }

  auto msg_string = std::string{ std::begin(bytes.data), std::end(bytes.data) };

  // Erase trailing \r\n
  msg_string.erase(msg_string.find_last_not_of("\r\n") + 1);

  const auto message_type = micromodem::modem_message_type(msg_string);

  DS_D(WhoiMicromodem);

  switch (message_type) {
  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CACYC: {
    ROS_INFO_STREAM("Recieved CACYC: " << msg_string);
    auto msg = ds_acomms_micromodem_msgs::CACYC{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CACYC message: " << msg_string);
      return;
    }

    d->cacyc_pub_.publish(msg);

    // This is a response to our own CCCYC command.  Nothing further to do.
    if (msg.src_address == localAddress()) {
      ROS_INFO_STREAM("...In response to our own CCCYC");
      if (msg.num_frames != d->num_unsent_frames_) {
	ROS_ERROR_STREAM("Received CACYC in response to our CCCYC, but with "
			 << "wrong number of frames. "
			 << "Expected: " << msg.num_frames
			 << ", Queued: " << d->num_unsent_frames_);
      }
      return;
    }

    // Ignore messages not intended for us
    if (msg.dest_address != localAddress() && msg.dest_address != BroadcastAddress) {
      ROS_INFO_STREAM("...Address was neither local nor broadcast. Ignoring.");
      return;
    }

    // We're about to receive some data.  Set the expected number of frames
    // and clear the receive buffer.
    ROS_INFO_STREAM("...Indicating incoming data");

    // If there's a partial packet already in the buffer, it should
    // be published before starting this new one. This could happen
    // if the final frame is dropped and a second message arrives before
    // the cacyc_timer fires.
    if (d->unsent_packet_) {
      ROS_WARN_STREAM("Got CACYC before previous packet was published.");
      publishPacket();
    }

    d->rx_packet_.clear();
    d->expected_total_rx_frames_ = msg.num_frames;
    d->remote_address_ = msg.src_address;
    d->receive_rate_ = msg.rate;
    d->unsent_packet_ = true;
    d->next_expected_rx_frame_ = 1; // One-indexed
    d->cacyc_timer_.setPeriod(ros::Duration(d->cacyc_timeout_));
    d->cacyc_timer_.start();
    break;
  }

  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CARXD: {
    ROS_INFO_STREAM("Recieved CARXD: " << msg_string);
    auto msg = ds_acomms_micromodem_msgs::CARXD{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CARXD message: " << msg_string);
      return;
    }

    // Ignore messages not intended for us
    if (msg.dest_address != localAddress() && msg.dest_address != BroadcastAddress) {
      ROS_INFO_STREAM("Ignoring message. localAddress = " << localAddress()
		      << ", dest = " << msg.dest_address
		      << ", broadcast = " << BroadcastAddress);
      return;
    }

    // Overwrite the broadcast address with our own.
    if (msg.dest_address == BroadcastAddress) {
      msg.dest_address = localAddress();
    }

    // If this has a different source than the most recently received
    // CACYC, something has gone wrong ...
    if (msg.src_address != d->remote_address_) {
      ROS_ERROR_STREAM("Received CARXD with src_address " << msg.src_address
		       << ", but expected " << d->remote_address_
		       << ". Dropping message.");
      return;
    }

    ROS_INFO("Received frame %d of %d. Next expected was %d", msg.frame,
	     d->expected_total_rx_frames_, d->next_expected_rx_frame_);

    if (msg.frame < d->next_expected_rx_frame_) {
      ROS_ERROR_STREAM("Received out-of-order CARXD. Expected frame "
		       << d->next_expected_rx_frame_ << ", but got "
		       << msg.frame << ". Dropping message." );
      return;
    }

    while (d->next_expected_rx_frame_ < msg.frame) {
      ROS_ERROR("Dropped frame %d", d->next_expected_rx_frame_);
      d->next_expected_rx_frame_ += 1;
      WhoiMicromodem::Frame frame;
      d->rx_packet_.push_back(frame);
    }

    // Add recieved bytes to rx buffer
    Frame frame;
    std::move(std::begin(msg.bytes), std::end(msg.bytes),
	      std::back_inserter(frame));
    d->rx_packet_.push_back(frame);
    msg.bytes.clear();

    // Still more frames to go...
    // QUESTION(LEL): What happens if the last frame is dropped? It looks
    //   like in that case, we never call handleMicromodemPacket and the
    //   received data in d->rx_buffer_ is deleted by the next CACYC?
    //   (This bug wouldn't have appeared in current operations, where we
    //   limit ourselves to single frame packets at rate 1...)
    if (d->next_expected_rx_frame_ < d->expected_total_rx_frames_) {
      d->next_expected_rx_frame_ += 1;
    } else {
      ROS_INFO_STREAM("Received last frame of packet -- publishing!");
      publishPacket();
    }
    break;
  }

  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CADRQ: {
    ROS_INFO_STREAM("Recieved CADRQ: " << msg_string);
    auto msg = ds_acomms_micromodem_msgs::CADRQ{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CADRQ message: " << msg_string);
      return;
    }

    d->cadrq_pub_.publish(msg);

    // Check match of CADRQ against buffered packet
    if (msg.frame <= 0 ) {
      ROS_ERROR_STREAM("CADRQ requested frame " << msg.frame
		       << " but micromodem frames are expected to be one-indexed");
      return;
    }

    // TODO: If we didn't initialize this transmission, ignore the data request.
    //     (This will happen when running in parallel with the shim and
    //     existing acomms/sentrysitter stack)
    // This could probably be implemented by a flag set when a cycle-init
    // is started and then unset after the first cadrq.
    // However, that's starting to feel like a change that goes better with a
    // refactor of this driver to respect the modem's internal state machine.
    // For now, the next check will trigger, giving a potentially-confusing
    // ROS_ERROR message.

    if (d->num_unsent_frames_ <= 0) {
      ROS_ERROR_STREAM("Received CADRQ, but have no unsent frames. Ignoring."
		       " (This is OK when running alongside sentrysitter)");
      return;
    }

    if (msg.frame > d->tx_packet_.size()) {
      ROS_ERROR_STREAM("Requested frame " << msg.frame << " but packet only has "
		       << d->tx_packet_.size() << " frames."
		       << " (If running sentrysitter in parallel, this is expected.)");
      return;
    }
    if (msg.max_bytes < d->tx_packet_[msg.frame - 1].size()) {
      ROS_ERROR_STREAM("Modem requested " << msg.max_bytes
		       << " bytes but next frame in packet has more: "
		       << d->tx_packet_[msg.frame-1].size());
      return;
    }

    // TODO: We really need to make sure that we only send each frame once.
    //       The CATXD mesage does NOT include the frame, just the length
    //       (makes sense, b/c CCTXD also ignores frame)

    auto txd_msg = ds_acomms_micromodem_msgs::CCTXD{};
    txd_msg.src_address = msg.src_address;
    txd_msg.dest_address = msg.dest_address;
    txd_msg.ack = msg.ack;
    txd_msg.frame = msg.frame;
    txd_msg.bytes = d->tx_packet_[msg.frame - 1];

    auto txd_str = std::string{};
    ds_acomms_serialization::serialize(txd_msg, txd_str);
    ROS_INFO_STREAM("Sending: '" << txd_str << "'");
    d->num_unsent_frames_ = d->num_unsent_frames_ - 1;
    modemConnection()->send(txd_str);
    break;
  }

  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CACFG: {
    // QUESTION(LEL): It seems like this is assuming another process
    //   sent `$CCCFG,SRC,#` and trying to keep in sync with current
    //   modem status. Is that true? (It would make sense with what
    //   I've heard about "current driver only handles re-configuring
    //   modem when it power cycles"...) So do we run TWO drivers?
    //   Both communicating w/ the micromodem via UDP/Moxa?
    auto msg = ds_acomms_micromodem_msgs::CACFG{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CACFG message: " << msg_string);
      return;
    }
    if (msg.param.compare("SRC") == 0) {
      setLocalAddress(static_cast<uint16_t>(msg.value));
    }
    break;
  }

  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CACST: {
    auto msg = ds_acomms_micromodem_msgs::CACST{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CACST message: " << msg_string);
      return;
    }
    d->cacst_pub_.publish(msg);
    break;
  }

    /**
// This doesn't yet parse for the micromodem1, so disable for now
  case ds_acomms_micromodem_msgs::MicromodemMessageEnum::CAXST: {
    auto msg = ds_acomms_micromodem_msgs::CAXST{};
    if (!ds_acomms_serialization::deserialize(msg_string, msg)) {
      ROS_ERROR_STREAM("Failed to deserialize CAXST message: " << msg_string);
      return;
    }
    d->caxst_pub_.publish(msg);
    break;
  }
    **/

  default:
    ROS_WARN_STREAM("Micromodem driver Ignoring: '" << msg_string << "'");
    break;
  }
}

void WhoiMicromodem::publishPacket(const ros::TimerEvent& event) {
  ROS_WARN_STREAM("Publishing packet due to TimerEvent!");
  publishPacket();
}

void WhoiMicromodem::publishPacket() {
  DS_D(WhoiMicromodem);
  d->cacyc_timer_.stop();
  auto msg = ds_acomms_msgs::MicromodemData{};
  msg.stamp = ros::Time::now();
  msg.local_addr = localAddress();
  msg.remote_addr = d->remote_address_;
  msg.rate = d->receive_rate_;
  for (const auto frame : d->rx_packet_) {
    auto frame_msg = ds_acomms_msgs::ModemData{};
    frame_msg.payload = frame;
    msg.frames.push_back(frame_msg);
  }
  d->micromodem_pub_.publish(msg);

  // IF all frames are valid, concatenate them into buffer and send through
  // to the base class's publish method for AcousticModemData.
  bool is_complete_packet = true;
  if (d->rx_packet_.size() != d->expected_total_rx_frames_) {
    ROS_INFO_STREAM("Packet has wrong number of frames; not publishing ModemData.");
    is_complete_packet = false;
  }

  // At the first skipped frame, we have an invalid packet and won't send it
  // via the base class interface.
  // NB: This doesn't differentiate between skipped and empty frames.
  //     That's OK because the base class's ROS interface will never
  //     transmit an empty frame.
  std::vector<uint8_t> rx_buffer;
  for (const auto frame : d->rx_packet_) {
    if (frame.size() == 0) {
      ROS_INFO_STREAM("Packet has missing/empty frame; not publishing ModemData.");
      is_complete_packet = false;
    }
    std::copy(std::begin(frame), std::end(frame), std::back_inserter(rx_buffer));
  }

  if (is_complete_packet) {
    handleMicromodemPacket(d->remote_address_, rx_buffer);
  }

  d->unsent_packet_ = false;
}

void WhoiMicromodem::handleMicromodemPacket(uint16_t src_address, std::vector<uint8_t> bytes)
{
  publishReceivedAcousticModemData(src_address, std::move(bytes));
}

void WhoiMicromodem::setDefaultRate(WhoiMicromodem::Rate rate) noexcept
{
  if (rate >= NUM_RATES)
  {
    ROS_ERROR_STREAM("Invalid rate value: '" << static_cast<int>(rate) << "'");
    return;
  }

  DS_D(WhoiMicromodem);
  d->default_rate_ = rate;
}

WhoiMicromodem::Rate WhoiMicromodem::defaultRate() const noexcept
{
  const DS_D(WhoiMicromodem);
  return d->default_rate_;
}

int WhoiMicromodem::sendPacket(uint16_t remote_address, WhoiMicromodem::Packet packet, WhoiMicromodem::Rate rate) {
  ROS_INFO_STREAM("WhoiMicromodem::sendPacket() called");
  // TODO: perform validation of packet / rate for valid rate, valid nframes, and valid frame lengths

  auto cycle_init = ds_acomms_micromodem_msgs::CCCYC{};
  cycle_init.command = 0;
  cycle_init.src_address = localAddress();
  cycle_init.dest_address = remote_address;
  cycle_init.rate = static_cast<uint8_t>(rate);
  cycle_init.ack = false;
  cycle_init.num_frames = packet.size();

  DS_D(WhoiMicromodem);
  d->tx_packet_ = std::move(packet);
  d->num_unsent_frames_ = cycle_init.num_frames;
  auto modem = modemConnection();
  auto init_str = std::string{};
  ds_acomms_serialization::serialize(cycle_init, init_str);

  ROS_INFO_STREAM("Sending init string: '" << init_str << "'.");

  modem->send(init_str);
  return 0;  // success
}

int WhoiMicromodem::send(uint16_t remote_address, std::vector<uint8_t> bytes, WhoiMicromodem::Rate rate)
{
  ROS_INFO_STREAM("WhoiMicromodem::send(...) called!");
  auto max_bytes = WhoiMicromodem::RateBytesPerFrame.at(rate);
  auto max_frames = WhoiMicromodem::RateMaxFrames.at(rate);
  // NOTE(LEL): This was always sending an extra empty frame. The only reasoning I can
  //   think of was to make sure that it never attempts to send a 0-frame packet
  //auto num_frames = static_cast<uint8_t>(std::ceil(1.0 * bytes.size() / max_bytes)) + 1;
  auto num_frames = static_cast<uint8_t>(std::ceil(1.0 * bytes.size() / max_bytes));
  if (num_frames == 0) {
    num_frames = 1;
  }
  if (num_frames > max_frames) {
    ROS_ERROR_STREAM("Cannot send " << bytes.size() << " bytes via rate "
		     << static_cast<int>(rate) << "; exceeds max frame count of "
		     << max_frames);
    return 1;  // error
  }

  Packet packet;
  for (int ii = 0; ii < num_frames; ii++) {
    auto last_elem = max_bytes > bytes.size() ? std::end(bytes) : std::next(std::begin(bytes), max_bytes);
    Frame frame;
    std::move(std::begin(bytes), last_elem, std::back_inserter(frame));
    bytes.erase(std::begin(bytes), last_elem);
    packet.push_back(frame);

  }

  return sendPacket(remote_address, packet, rate);
}

int WhoiMicromodem::send(uint16_t remote_address, std::vector<uint8_t> bytes)
{
  return send(remote_address, std::move(bytes), defaultRate());
}

void WhoiMicromodem::setupSubscriptions()
{
  ModemBase::setupSubscriptions();
  DS_D(WhoiMicromodem);
  auto nh = nodeHandle();
  //  ros::NodeHandle nh("~");
  d->micromodem_sub_ = nh.subscribe("tx_packet", 1, &WhoiMicromodem::handleMicromodemData, this);
}

void WhoiMicromodem::setupPublishers() {
  ModemBase::setupPublishers();
  DS_D(WhoiMicromodem);
  auto nh = nodeHandle();
  d->micromodem_pub_ = nh.advertise<ds_acomms_msgs::MicromodemData>("rx_packet", 1, false);

  d->cacst_pub_ = nh.advertise<ds_acomms_micromodem_msgs::CACST>("cacst", 1, false);
  d->cacyc_pub_ = nh.advertise<ds_acomms_micromodem_msgs::CACYC>("cacyc", 1, false);
  d->cadrq_pub_ = nh.advertise<ds_acomms_micromodem_msgs::CADRQ>("cadrq", 1, false);
  d->caxst_pub_ = nh.advertise<ds_acomms_micromodem_msgs::CAXST>("caxst", 1, false);
}

void WhoiMicromodem::setupServices() {
  ModemBase::setupServices();
  // TODO(LEL): add configuration service
}

void WhoiMicromodem::setupTimers() {
  ModemBase::setupTimers();
  DS_D(WhoiMicromodem);
  auto nh = nodeHandle();
  // Inspection of on-vehice logs indicates that the legacy driver received
  // the first CARXD message 0.13 seconds after the corresponding CACYC.
  // So, the delay is NOT the full transmission length, but IS longer than
  // the time required to transmit the data over serial.
  d->cacyc_timer_ = nh.createTimer(ros::Duration(d->cacyc_timeout_),
				   &WhoiMicromodem::publishPacket, this, true);
  d->cacyc_timer_.stop();
}

void WhoiMicromodem::setupParameters() {
  ModemBase::setupParameters();
  DS_D(WhoiMicromodem);

  ros::NodeHandle nh("~");
  // TODO(LEL): I hate default parameters, but this lets me test on sentry
  //            without having to synchronize config changes as well.
  nh.param<float>("cacyc_timeout", d->cacyc_timeout_, 2.0);
  /**
  if(!nh.getParam("cacyc_timeout", d->cacyc_timeout_)) {
    ROS_FATAL("Unable to find parameter cacyc_timeout");
    ROS_BREAK();
  }
  **/

}

}
