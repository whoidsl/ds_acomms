/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_modem/modem_base.h"
#include "ds_acomms_modem/SendAcousticData.h"
#include "ds_acomms_msgs/AcousticModemData.h"

#include "modem_base_private.h"
namespace ds_acomms_modem
{
ModemBase::ModemBase() : d_ptr_(std::unique_ptr<ModemBasePrivate>(new ModemBasePrivate))
{
}

ModemBase::ModemBase(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<ModemBasePrivate>(new ModemBasePrivate))
{
}

ModemBase::~ModemBase() = default;

void ModemBase::setLocalAddress(uint16_t address)
{
  ROS_INFO_STREAM("Setting local modem address to '" << address << "'");
  DS_D(ModemBase);
  d->local_addr_ = address;
}

uint16_t ModemBase::localAddress() const noexcept
{
  const DS_D(ModemBase);
  return d->local_addr_;
}

void ModemBase::setupConnections()
{
  DsProcess::setupConnections();
  DS_D(ModemBase);
  d->modem_ = addConnection("modem", boost::bind(&ModemBase::handleModemMessage, this, _1));
}

void ModemBase::setupParameters()
{
  DsProcess::setupParameters();
  DS_D(ModemBase);

  auto str = std::string{};
  auto int_param = 0;

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "local_address" });
  if (!ros::param::get(str, int_param))
  {
    ROS_FATAL_STREAM("Missing configuration parameter: '" << str << "'");
    ROS_BREAK();
  }

  if (int_param < 0 || (int_param > std::numeric_limits<decltype(d->local_addr_)>::max()))
  {
    ROS_FATAL_STREAM("Illegal value (" << int_param << ") for parameter: '" << str << "'");
    ROS_BREAK();
  }

  d->local_addr_ = static_cast<decltype(d->local_addr_)>(int_param);
}

ds_asio::DsConnection* ModemBase::modemConnection()
{
  DS_D(ModemBase);
  return d->modem_.get();
}

void ModemBase::sendAcousticModemData(const ds_acomms_msgs::AcousticModemData& msg)
{
  if (msg.local_addr != localAddress())
  {
    return;
  }

  send(msg.remote_addr, std::move(msg.payload));
}

void ModemBase::publishReceivedAcousticModemData(uint16_t remote_addr, std::vector<uint8_t> bytes)
{
  auto msg = ds_acomms_msgs::AcousticModemData{};
  msg.stamp = ros::Time::now();
  msg.local_addr = localAddress();
  msg.remote_addr = remote_addr;
  msg.payload = std::move(bytes);
  DS_D(ModemBase);
  d->received_pub_.publish(msg);
}

void ModemBase::setupSubscriptions()
{
  DsProcess::setupSubscriptions();
  DS_D(ModemBase);
  auto nh = nodeHandle();
  d->transmit_sub_ = nh.subscribe("tx_acoustic_data", 1, &ModemBase::sendAcousticModemData, this);
}

void ModemBase::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(ModemBase);
  auto nh = nodeHandle();
  d->received_pub_ = nh.advertise<ds_acomms_msgs::AcousticModemData>("rx_acoustic_data", 1, false);
}

void ModemBase::setupServices()
{
  DsProcess::setupServices();
  auto nh = nodeHandle("~");
  DS_D(ModemBase);

  auto callback = [=](SendAcousticData::Request& req, SendAcousticData::Response& res) {
    this->send(req.remote_address, req.payload);
    return true;
  };

  d->send_data_service_ =
      nh.advertiseService<SendAcousticData::Request, SendAcousticData::Response>("send_acoustic_data", callback);
}
}
