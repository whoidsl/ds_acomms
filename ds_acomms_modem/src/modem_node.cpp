/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
///
/// # ROS node for running QueueManagers defined in sentry_acomms
///

#include "ds_acomms_modem/sonardyne_avtrak.h"
#include "ds_acomms_modem/whoi_micromodem.h"

#include <ros/ros.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

static const auto MODEMS =
    std::list<std::pair<std::string, std::string>>{
        { "avtrak", "Sonardyne Avtrak USBL beacon" },
        { "umodem", "WHOI MicroModem2" }
    };

void print_usage(const std::string& name, const po::options_description& options)
{
  std::cout << "Usage:  " << name << " modem --node [ROS_OPTIONS]" << std::endl
            << std::endl
            << "Starts a node named 'modem' for the desired modem driver" << std::endl
            << std::endl
            << options << std::endl;

  std::cout << "ROS_OPTIONS:  These can be used to override ROS-specific parameters" << std::endl
            << "  __name:=$NAME         Override the default node name" << std::endl;

  std::cout << "EXAMPLE:" << std::endl
            << "  start a node named 'avtrak2' under the namespace 'modems' using the 'avtrak' manager:"
            << std::endl
            << "    env ROS_NAMESPACE=modems rosrun ds_acomms modem avtrak __name:=avtrak2" << std::endl
            << std::endl;
}

int main(int argc, char* argv[])
{
  //
  //  Setting up our command line parsing
  //

  po::options_description core("Core options");
  core.add_options()("modem,m", po::value<std::string>(), "modem type");

  po::options_description info("Info options");
  info.add_options()("help,h", "print this help message")("list-modems", po::bool_switch()->default_value(false),
                                                          "Show list of available queue managers");

  po::options_description connections("Connection options");
  connections.add_options()("serial", "Add a serial connection.  Format is $PORT:$BYTE$PARITY$STOP (e.g. "
                                      "/dev/ttyS0:8N1")("udp", "Add a udp connection.  Format is "
                                                               "$LISTEN_IFACE:LISTEN_PORT,$DEST_IP:$DEST_PORT (e.g. "
                                                               "0.0.0.0:5000,192.168.100.100:5000");

  po::options_description all("Options");
  all.add(core)
      //  .add(connections)
      .add(info);

  // Positional arguments
  po::positional_options_description p;
  p.add("modem", 1);

  // Call ros::init now.  It'll handle any remappings from roslaunch or the like and remove all ros-specific
  // command line arguments.
  ros::init(argc, argv, "modem");

  // Now try parsing our command line after ros has stripped out it's own parameters
  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(all).positional(p).run(), vm);

  po::notify(vm);

  //
  // Start checking command line arguments
  //
  if (vm.count("help"))
  {
    print_usage(argv[0], info);
    return 0;
  }

  //
  // Print a list of available modems
  //
  if (vm["list-modems"].as<bool>())
  {
    std::cout << std::left << "Available modems:" << std::endl << std::endl;

    std::cout << std::left << std::setw(20) << "modem name:" << std::left << std::setw(50)
              << "description:" << std::endl;

    for (auto& modem_pair : MODEMS)
    {
      std::cout << std::left << std::setw(20) << modem_pair.first << std::left << std::setw(50) << modem_pair.second
                << std::endl;
    }

    return 0;
  }

  //
  // Handle missing required arguments
  //
  if (vm["modem"].empty())
  {
    std::cerr << "ERROR:  No modem specified.\n"
              << "ERROR:  Use '--list-modems' and choose a valid queue manager." << std::endl
              << std::endl;
    print_usage(argv[0], info);
    return 1;
  }

  //
  // Everything seems good, start initializing the node.
  //

  // Create an empty unique pointer to a ds_acomms::QueueManager instance.  We'll use this to hold our
  // actual sensor class
  auto node = std::unique_ptr<ds_acomms_modem::ModemBase>{};

  // Figure out our sensor type and the name of the node.
  const auto modem = vm["modem"].as<std::string>();

  // Look up our sensor.  There's better ways of doing this, but this is the easiest.
  if (modem == "avtrak")
  {
    node.reset(new ds_acomms_modem::SonardyneAvtrak);
  }
  else if (modem == "umodem")
  {
    node.reset(new ds_acomms_modem::WhoiMicromodem);
  }
  else
  {
    std::cerr << "ERROR: Unknown queue manager specified: " << modem << std::endl;
    std::cerr << "ERROR: Use '--list-modems' and choose a valid queue manager." << std::endl;
    return 1;
  }

  // Start running.
  node->run();

  // Return success.
  return 0;
}
