#
# File: acomm.ini.a2s.nereus.nosync.master.01min.uplink
#

# This is an example ini file for code running subsea (i.e. on the
# vehicle). Several parameters MUST be set for a new installation:
#
# DEVICE in the MODEM_SIO_THREAD section
# REQUIRED SETTINGS in the CFG_INFO section
#
# MODIFICATION HISTORY
# 2012-07-22 LLW Created for a2s from cayman_2009/acomm.ini.nereus.CAYMAN09.nodepr
#               Changes: changed vehicle from master to slave per MVJ discussion
#                        changed to non-synchronous mode
# 2012-07-22 LLW master mode with 1 min uplink
# 2012-07-22 LLW master mode with 1 min uplink at 10 seconds past the top of minute
# 2012-07-22 LLW master mode with 1 min uplink at 30 seconds past the top of minute
# 2013-05-27 MVJ Created for nereus-2013-cayman from 
#                acomm.ini.a2s.nereus.nosync.master.01min.uplink
#                Master mode with 1 minute spaced uplinke at the top of the 
#                minute.
# 2013-06-11 MVJ Change to lower BW0=1250 from BW0=2000 for nereus051.  Comms
#                has been good, this is to trial in anticipation of deeper
#                dives.
# 2014-05-17 SS  Adapting the inifile for NUI
# 2014-07-12 LLW Revised for 10 kHz Nui Polarstern ops. Tat=0 DOP=1 DQF=1 MFD=1
# 2014-07-13 LLW parameters for 10 kHz modem revised in colsultation with acomms folks
# 2014/07/25 LLW Revised for NUI 003 with two 10 kHz modems in NUI


#############    MAIN     ###################
[LOGGING]
DIR_LOG=/data/acomm/

############# I/O SECTION ###################
#
# In order to maintian symmetry between the code running subsea and
# topside, all necessary SIO and NIO threads are launched in both
# places. This should not affect the execution of the program provided
# suitable "dummy" ports are provided (non-existent and
# traffic-less). Unused thread ini sections are noted. If these ini
# sections are deleted the program will abort.

[MODEM_SIO_THREAD]
#DEVICE=ttyXR22
DEVICE=ttyUSB0
destination_1=ACOMM_THREAD
BAUD=19200
PARITY=NONE
STOPBITS=1
DATABITS=8
TERM_CHAR=\n
ECHO=0
ATTACHED_TO=modem

[MODEM_UDP_THREAD]
# SS - destination is the modem Moxa
PORT=5701
TO_IP_ADDRESS=192.168.50.57
TO_PORT=5701
destination_1=ACOMM_THREAD

[AUV_NIO_THREAD]
PORT=50201
TO_IP_ADDRESS=127.0.0.1
TO_PORT=50200
destination_1=ACOMM_THREAD

[NAV_NIO_THREAD]
PORT=50205
TO_IP_ADDRESS=127.0.0.1
TO_PORT=50101
destination_1=ACOMM_THREAD

############# ACOMM THREAD SECTION ###################

[ACOMM_THREAD]
MODEM_NAME=MODEM_1
MODEM_BAUD=19200
MODEM_DEVICE=/dev/ttyXR22
MODEM_STREAMING=FALSE
MODEM_VERBOSE=TRUE


############# CFG SECTION ###################

[CFG_INFO]
# ALL=0 resets modem to factory default before each initialization
# On the testbox this will make the modem continually reset, so
# comment it out when using the test box.
#ALL=0

# REQUIRED SETTINGS: You must have MASTER, MODEM_IO_THREAD and SRC
#                    and BND for the program to run properly.
# 2012-07-22 LLW Changed from master mode (MASTER=1) to slave mode (MASTER=0)
MASTER=1

# use this for moxa operation
# MODEM_IO_THREAD=MODEM_UDP_THREAD
# use this for serial operation
MODEM_IO_THREAD=MODEM_SIO_THREAD

# Delta time in seconds between acoustic messages that will cause
# daemon to drop out of Synchronous Nav Mode
MAX_TIME_NO_MSG_SENT=90

# Fixed Interval Time Out: set to one for fixed interval tdma
FITO=1

# DISABLE TIME SYNCING
CFG_CLOCK_SET_CMD=1

# OPTIONAL SETTINGS: The following settings (and all other modem cfg
#		     settings) are not required for program to run.

# 2014-07-13 LLW parameters for 10 kHz modem revised in colsultation with acomms folks
SRC=0 

# 2014-07-15 Per Lee and Keenan
# - for short range high bandwidth: BND=1 and FCO and BW0 are hard-coded
# - for long range low bandwidth: BND=0, FCO=10000, BW0=2000
BND=0
FC0=10000
BW0=2000

AGC=1
AGN=250
AGX=1
ASD=1
BBD=0
BSP=1

#BR1=3
#BR2=3
BRN=0

CPR=1
CRL=50
CST=1
CTO=10
DBG=0
DGM=0
DOP=1
DQF=1
DTH=108
DTO=2
DTP=90
ECD=50
EDR=0
EFF=20
EFB=10

FMD=1
FML=200
GPS=0
HFC=0
IRE=0
MOD=1
MFD=1
MSE=1
MCM=1

# 2014-07-15 per Keenan:
# - when using mult-channel array PSK set MPR=0 and PCM=15
# - when using single channel ducer PSK set MPR=1 and PCM=0
MPR=1
PCM=0


MVM=1
NDT=120
# NPT=50
NRL=25
NRV=150
PAD=2
PRL=50
PTH=50

POW=-100

PTO=14
REV=1
RSP=-157
RXA=0
RXD=1
RXP=1
SCG=0
SGP=1
SHF=0
SNR=1
SNV=0

TAT=0

# 2014/07/25 LLW TOA=1
TOA=1
TXD=600
TXP=1
TXF=1
XST=1



############## STATE MACHINE SECTION ############
#
# **IF** the modem is designated as master, the acomm program will
# look for entries to the TDMA state machine in order: SM_01,
# SM_02... If multiple entries have the same heading, the first one is
# used. If there is a break in the numbering sequence the state
# machine stops adding entries. Do NOT delete entries that are not in
# use b/c they serve as examples. Simply delete the heading number.

# always finish with a VLPAUSE as final state for fixed period TDMA 

[SM_01]
TYPE=CYCLEINIT
NAME=CI1_PSK
MODE=0
SRC=0
DEST=15
PKTTYPE=1
ACK=0
NPKTS=1
RETRY=0
TIMEOUT=13

[SM_02]
TYPE=VLPAUSE
TIME=15

[SM_03]
TYPE=CYCLEINIT
NAME=CI1_PSK
MODE=0
SRC=0
DEST=15
PKTTYPE=1
ACK=0
NPKTS=1
RETRY=0
TIMEOUT=13

[SM_04]
TYPE=VLPAUSE
TIME=30


############################

[SM_]
# Nui 10.0 LBL Cycle - timeout 14 sec
TYPE=LBL
NAME=LBL
TIMEOUT=13.0
RXTIMEOUT=14000
TXFREQ=9000
TXTAU=10
RXTAU=10
# xponder A - Located at the Polarstern's Moon Pool
RXFREQ1=08500
# xponder B - Located in the NUI depressor
RXFREQ2=10500
# unused
RXFREQ3=11500
# unused
RXFREQ4=12000
RETRY=0

[SM_]
TYPE=VLPAUSE
TIME=60

[SM_]
# Nui 10.0 (1) pings Deck 10.0 (4) for range
TYPE=PING
xNAME=PING_1TO4
SRC=1	
DEST=4
RETRY=0
TIMEOUT=13.0

[SM_]
TYPE=VLPAUSE
TIME=75

[SM_]
# Nui 10.0 (1) pings Cage 10.0 (3) for range
TYPE=PING
NAME=PING_1TO3
SRC=1	
DEST=3
RETRY=0
TIMEOUT=13.0

[SM_]
TYPE=VLPAUSE
TIME=90


[SM_]
TYPE=CYCLEINIT
NAME=CI2_PSK4
MODE=0
SRC=0
DEST=15
PKTTYPE=4
ACK=0
NPKTS=1
#NPKTS=2
RETRY=0
TIMEOUT=15

[SM_]
TYPE=CYCLEINIT
NAME=CI3_PSK5
MODE=0
SRC=0
DEST=15
PKTTYPE=5
ACK=0
NPKTS=1
#NPKTS=8
RETRY=0
TIMEOUT=15

[SM_]
TYPE=LBL
NAME=LBL
TIMEOUT=5
RXTIMEOUT=4500
TXFREQ=9000
TXTAU=5
RXTAU=10
RXFREQ1=10500
RXFREQ2=11000
RXFREQ3=11500
RXFREQ4=12000
RETRY=0

[SM_0]
TYPE=CYCLEINIT
NAME=CI_REQDATA_MOD0_MOD1
MODE=1
SRC=0
DEST=1
PKTTYPE=0
NPKTS=1
ACK=1
RETRY=0

[SM_0]
TYPE=CYCLEINIT
NAME=CI_SENDDATA_MOD1_MOD0
MODE=1
SRC=1
DEST=0
PKTTYPE=0
NPKTS=1
ACK=1
RETRY=0
TIMEOUT=11

[SM_0]
TYPE=PING
NAME=PING_MODEM1_MODEM0
SRC=1
DEST=0
RETRY=0
TIMEOUT=5

[SM_0]
TYPE=PING
NAME=PING_MODEM0_MODEM1
SRC=1
DEST=0
RETRY=0
TIMEOUT=11

[SM_0]
TYPE=MPACKET
NAME=MINIPACKET_ABORT
SRC=1
DEST=2
DATA=1FFF
TIMEOUT=5
RETRY=0

[SM_0]
TYPE=PAUSE
TIME=2

[SM_0]
TYPE=LBL
NAME=LBL
TIMEOUT=5
RXTIMEOUT=4000
TXFREQ=9000
TXTAU=5
RXTAU=10
RXFREQ1=10500
RXFREQ2=11500
RETRY=0
