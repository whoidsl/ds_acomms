#! /usr/bin/env python2.7

"""
Communicate with the micromodem node over UDP, mocking interactions
that would occur with the real modem:
- Receive ROS message, driver emits CCCYC -> (mock) CACYC, CADRQ -> (driver) CCTXD
- (mock) CARXD, CACST -> (driver) publish ROS message

ALL micromodem messages are published as both a byte string AND
as a packet with the payload broken into frames. This is for
compatability with the general ModemBase class, and code interacting
with the micromodem should only subscribe to one of the two topics.

Similarly, the driver subscribes to both AcousticModemData and MicromodemData,
and sent the data contained in either via the modem.

We are only testing the rates that are in use / likely to be in use:
- rate 1: 1-3 frames, 64 bytes each (BCH 128:8 QPSK)
- rate 4: 1-2 frames, 256 bytes each (BCH 64:10 QPSK)
- rate 5: 1-8 frames, 256 bytes each (Hamming 14:9 QPSK)
"""

import numpy as np
import rospy
import select
import socket
import StringIO
import time
import unittest

import ds_acomms_msgs.msg


def read_data(socket):
    timeout = 0.1  # seconds
    # Input to select is (read_list, write_list, x_list, timeout),
    # where the lists are FDs to check for readiness
    read_list = [socket]
    write_list, x_list = [], []
    read_ready, _, _ = select.select(read_list, write_list, x_list, timeout)
    data = []
    if len(read_ready) > 0:
        data = socket.recv(4096)
    return data


class TestPublishModemData(unittest.TestCase):
    '''
    Test that the driver publishes a ROS message on receipt of modem data.
    '''
    def setUp(self):
        self.tx_port = 50200
        self.rx_port = 50201
        self.tx_addr = "127.0.0.1" # also in test_frames.test
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('', self.rx_port))

        self.legacy_sub = rospy.Subscriber("/rx_acoustic_data",
                                           ds_acomms_msgs.msg.AcousticModemData,
                                           self.legacy_cb)
        self.packet_sub = rospy.Subscriber("/rx_packet",
                                           ds_acomms_msgs.msg.MicromodemData,
                                           self.packet_cb)
        self.legacy_msg = None
        self.packet_msg = None
        self.packet_count = 0

        time.sleep(1.0)

    def tearDown(self):
        self.socket.close()

    def reset(self):
        '''
        This is hacky, since I have multiple test cases within a single test.
        '''
        self.legacy_msg = None
        self.packet_msg = None
        self.packet_count = 0

    def write_data(self, data):
        self.socket.sendto(data, (self.tx_addr, self.tx_port))

    def legacy_cb(self, msg):
        print "Received message!", msg
        self.legacy_msg = msg

    def packet_cb(self, msg):
        print "Received message!", msg
        self.packet_msg = msg
        self.packet_count += 1


    def rate1_publish_helper(self, length, dropped):
        # type: (int, List[bool]) -> None
        '''
        * length - number of chars in message
        * dropped - array of bools for which frames should be dropped.
                    Length must match expected number of frames for input length.
        '''
        # Before doing anything, reset the received messages!
        self.reset()

        # $CACYC,CMD,ADR1,ADR2,Packet Type,ACK,Npkt*CS
        num_frames = int(np.ceil(length/64.0))
        self.write_data("$CACYC,0,1,0,1,0,{}".format(num_frames))
        # Skipping the optional status information in: CAMFD, CATOA, CASNR, CADOP

        for frame, drop in zip(range(num_frames), dropped):
            if drop:
                continue
            start_idx = frame*64
            end_idx = min((frame+1)*64, length)
            # $CARXD,SRC,DEST,Ack,frame,HHHH
            mm_data = ''.join('%02x'%ii for ii in range(start_idx, end_idx))
            self.write_data("$CARXD,1,0,0,{},{}".format(frame+1, mm_data))

        time.sleep(1.0)

        # First, check legacy interface. We only expect it to be published if
        # all frames were received.
        if any(dropped):
            self.assertIsNone(self.legacy_msg)
        else:
            self.assertIsNotNone(self.legacy_msg, msg="Modem driver did not publish ROS message")
            # The ros message is an array of bytes, while the data received from the micromodem is a string
            mm_data = ''.join('%02x'%ii for ii in range(length))
            ros_data = ''.join('%02x'%ord(ii) for ii in self.legacy_msg.payload)
            self.assertEqual(ros_data, mm_data)

        # Now for the new interface; it should always be published, even if only a CACYC
        # is received without any of the actual payload
        self.assertIsNotNone(self.packet_msg)

        # Test that the messages data is actually as expected
        for frame_index, drop, msg_frame in zip(range(num_frames), dropped, self.packet_msg.frames):
            if drop:
                self.assertEqual(msg_frame.payload, '')
            else:
                start_idx = frame_index*64
                end_idx = min((frame_index+1)*64, length)
                # $CARXD,SRC,DEST,Ack,frame,HHHH
                mm_data = ''.join('%02x'%ii for ii in range(start_idx, end_idx))
                ros_data = ''.join('%02x'%ord(ii) for ii in msg_frame.payload)
                self.assertEqual(mm_data, ros_data)

    def test_publish(self):
        self.rate1_publish_helper(45, [False])
        self.rate1_publish_helper(150, [False, False, False])
        self.rate1_publish_helper(150, [False, False, True])
        self.rate1_publish_helper(150, [False, True, False])
        self.rate1_publish_helper(150, [True, False, False])
        self.rate1_publish_helper(150, [True, True, False])
        # NB: This ALSO tests that the modem publishes a partial packet
        #     after a timeout if final frame not received.
        self.rate1_publish_helper(150, [True, True, True])

    def test_publish_unexpected_cacyc(self):
        '''
        Tests that the first partial packet is published on receipt of
        second CACYC message.
        '''
        # Before doing anything, reset the received messages!
        self.reset()

        # $CACYC,CMD,ADR1,ADR2,Packet Type,ACK,Npkt*CS
        num_frames = 3
        self.write_data("$CACYC,0,1,0,1,0,{}".format(num_frames))
        # Skipping the optional status information in: CAMFD, CATOA, CASNR, CADOP
        mm_data = ''.join('%02x'%ii for ii in range(45))
        self.write_data("$CARXD,1,0,0,1,{}".format(mm_data))

        # Send second CACYC (corresponding to skipping two frames)
        self.write_data("$CACYC,0,1,0,1,0,{}".format(num_frames))

        time.sleep(1.0)

        # Since we've slept longer than the launch file's cacyc_timeout,
        # we should have received two messages.
        self.assertEquals(2, self.packet_count)
        self.assertIsNone(self.legacy_msg)


class TestSubscribeModemData(unittest.TestCase):
    '''
    Test that the driver properly subscribes to the ModemData messages
    and breaks the input bytes into the expected number of frames.

    '''
    def setUp(self):
        self.tx_port = 50200 # TODO: This should be a parameter specified in the launch file...
        self.rx_port = 50201
        self.tx_addr = "127.0.0.1" # also in test_frames.test
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('', self.rx_port))
        self.pub = rospy.Publisher("/tx_acoustic_data", ds_acomms_msgs.msg.AcousticModemData, queue_size=1)
        self.mm_pub = rospy.Publisher("/tx_packet", ds_acomms_msgs.msg.MicromodemData, queue_size=1)
        time.sleep(1.0)  # Give time for subscribers to connect

    def tearDown(self):
        self.socket.close()


    def write_data(self, data):
        self.socket.sendto(data, (self.tx_addr, self.tx_port))

    def send_acoustic_modem_message(self, length):
        '''
        Helper function to construct and send an AcousticModemData
        message with a payload of the specified length
        '''
        tx_msg = ds_acomms_msgs.msg.AcousticModemData()
        tx_msg.local_addr = 0  # Hard-coded here and in test_frames.test
        tx_msg.remote_addr = 1
        tx_msg.payload = [ii for ii in range(length)]
        self.pub.publish(tx_msg)
        # Magic number!
        time.sleep(0.2)
        return tx_msg

    def send_micromodem_message(self, msg_length, rate):
        '''
        Helper function to construct and send a MicromodemData
        message with a payload of the specified length
        '''
        num_bytes = {1:64, 4:256, 5:256}
        max_frames = {1:3, 4:2, 5:8}

        tx_msg = ds_acomms_msgs.msg.MicromodemData()
        tx_msg.local_addr = 0  # Hard-coded here and in test_frames.test
        tx_msg.remote_addr = 1
        tx_msg.rate = rate

        num_frames = int(np.ceil(1.0*msg_length/num_bytes[rate]))
        if num_frames == 0:
            num_frames = 1
        assert(num_frames <= max_frames)
        for frame_idx in range(num_frames):
            start_idx = frame_idx * num_bytes[rate]
            end_idx = min(msg_length, (frame_idx+1)*num_bytes[rate])
            frame = ds_acomms_msgs.msg.ModemData();
            frame.payload = [ii for ii in range(start_idx, end_idx)]
            tx_msg.frames.append(frame)

        self.mm_pub.publish(tx_msg)
        # Magic number!
        time.sleep(1.0)
        return tx_msg

    def rate1_send_helper(self, length, nframes, legacy_interface):
        '''
        Helper function for testing that a mdessage received via the base class's modem
        data interface is appropriateleay split into frames and sent to the micromodem.

        Provides the UDP interface that a micromodem would and checks that the
        drivers responses are as expected
        '''
        if legacy_interface:
            # ds_acomms_msgs.msg.AcousticModemData
            tx_msg = self.send_acoustic_modem_message(length)
        else:
            # ds_acomms_msgs.msg.MicromodemData
            tx_msg = self.send_micromodem_message(length, 1)

        cccyc_msg = read_data(self.socket)
        rospy.loginfo("Got data from driver! {}".format(cccyc_msg))
        # Other tests check checksums, so leave that out.
        # CCCYC,cmd,addr1,addr2,mode,ack,nframes*CS
        self.assertTrue(cccyc_msg.startswith("$CCCYC,0,0,1,1,0,{}*".format(nframes)))

        # Not strictly necessary, b/c driver just ignores it. However, the modem would send it...
        # CACYC,cmd,addr1,addr2,type,ack,nframes
        self.write_data("$CACYC,0,0,1,1,0,{}".format(nframes))

        # CADRQ,hhmmss,src,dest,ack,nbytes,frame#*CS
        for frame in range(nframes):
        # frame numbers appear to start with 1
            self.write_data("$CADRQ,123456,0,1,0,64,{}".format(frame+1))
            cadrq_msg = read_data(self.socket)
            print "Got response to CADRQ:", cadrq_msg
            self.assertTrue(cadrq_msg.startswith("$CCTXD,0,1,0,"))
            payload = cadrq_msg.strip().split(',')[-1]
            if legacy_interface:
                start_idx = frame*64
                end_idx = min((frame+1)*64, length)
                self.assertEquals(payload.lower(), ''.join('%02x'%ii for ii in tx_msg.payload[start_idx:end_idx]))
            else:
                self.assertEquals(payload.lower(), ''.join('%02x'%ii for ii in tx_msg.frames[frame].payload))

    def test_rate1_send(self):
        # Test sending shorter-than-single-frame message; this is the happy path
        # that has been exclusively used on Sentry.
        self.rate1_send_helper(45, 1, True)
        self.rate1_send_helper(45, 1, False)

        # Test 0-length message; in previous version of the driver, it always
        # sent an extra frame at the end. I've changed that to only send an
        # empty frame if it was the first.
        self.rate1_send_helper(0, 1, True)
        self.rate1_send_helper(0, 1, False)

        # Test that an exactly-single-frame message does not get split
        self.rate1_send_helper(64, 1, True)
        self.rate1_send_helper(64, 1, False)

        # Test that 65 bytes is split into two frames
        self.rate1_send_helper(65, 2, True)
        self.rate1_send_helper(65, 2, False)

        # Test that maximal-length message can be sent
        self.rate1_send_helper(192, 3, True)
        self.rate1_send_helper(192, 3, False)



class RostestMicromodemFrames(unittest.TestSuite):
    def __init__(self):
        super(RostestMicromodemFrames, self).__init__()

        self.addTest(unittest.makeSuite(TestSubscribeModemData))


if __name__ == "__main__":
    rospy.init_node('rostest_micromodem_frames')

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--rostest", action="store_true")
    # rostest adds additional arguments, so parse_args() is insufficient
    args,_ = parser.parse_known_args()

    if args.rostest:
        import rostest
        # Huh. I can only run one of these at a time; otherwise, the 2nd one fails
        # immediately.
        #rostest.rosrun("ds_acomms_modem", 'rostest_frames', TestPublishModemData)
        rostest.rosrun("ds_acomms_modem", 'rostest_frames', TestSubscribeModemData)
    else:
        unittest.main()
