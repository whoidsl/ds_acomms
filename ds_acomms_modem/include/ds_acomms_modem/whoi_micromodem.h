/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_MICROMODEM_WHOI_MICROMODEM_H
#define DS_MICROMODEM_WHOI_MICROMODEM_H

#include "ds_acomms_modem/modem_base.h"
#include "ds_acomms_msgs/MicromodemData.h"

namespace ds_acomms_modem
{
struct WhoiMicromodemPrivate;

class WhoiMicromodem : public ds_acomms_modem::ModemBase
{
  DS_DECLARE_PRIVATE(WhoiMicromodem)

public:
  enum Rate
  {
    CONV_219 = 0,
    BCH_128_8,
    DSSS_15,
    DSS_7,
    BCH_64_10,
    Hamming_14_9,
    DSS_15,
    NUM_RATES
  };

  // A Micromodem Packet consists of N frames, with M bytes each
  typedef std::vector<uint8_t> Frame;
  typedef std::vector<Frame> Packet;

  ///@brief Maximum number of frames per packet for each rate
  const std::vector<uint8_t> RateMaxFrames = { 1, 3, 3, 2, 2, 8, 6 };

  ///@brief Maximum number of bytes per frame for each rate
  const std::vector<uint16_t> RateBytesPerFrame = { 32, 64, 64, 256, 256, 256, 32 };

  const uint16_t BroadcastAddress = 15;

  WhoiMicromodem();
  WhoiMicromodem(int argc, char* argv[], const std::string& name);
  ~WhoiMicromodem() override;
  DS_DISABLE_COPY(WhoiMicromodem)

  // Send data to the modem; these provide the standard ModemBase
  // buffer interface, with no attention paid to frames
  int send(uint16_t remote_address, std::vector<uint8_t> bytes) override;
  int send(uint16_t remote_address, std::vector<uint8_t> bytes, Rate rate);
  // Micromodem-specific send function, allows chunking into frames
  // to be performed by other node.
  int sendPacket(uint16_t remote_address, Packet packet,
		 WhoiMicromodem::Rate rate);

  void setDefaultRate(Rate rate) noexcept;
  Rate defaultRate() const noexcept;

protected:
  // Handle data from the modem arriving on DsConnection
  void handleModemMessage(const ds_core_msgs::RawData& bytes) override;
  // This simply calls publishReceivedAcousticModemData; derived Sentry micromodem overrides it.
  virtual void handleMicromodemPacket(uint16_t src_address, std::vector<uint8_t> bytes);
  // publish the micromodem-specific packet message, and the base class version if applicable.
  void publishPacket();
  void publishPacket(const ros::TimerEvent &event);
  // Handle data received over ROS interface
  void handleMicromodemData(const ds_acomms_msgs::MicromodemData& msg);

  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupServices() override;
  void setupTimers() override;
  void setupParameters() override;

private:
  std::unique_ptr<WhoiMicromodemPrivate> d_ptr_;
};
}
#endif  // DS_MICROMODEM_WHOI_MICROMODEM_H
