/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_SONARDYNE_AVTRAK_H
#define DS_ACOMMS_SONARDYNE_AVTRAK_H

#include "ds_acomms_sonardyne_avtrak_msgs/DIAG.h"
#include "ds_acomms_modem/modem_base.h"

namespace ds_acomms_sonardyne_avtrak_msgs
{
ROS_DECLARE_MESSAGE(SmsDiagnostics);
}

namespace ds_acomms_modem
{
struct SonardyneAvtrakPrivate;

/// @brief A simple modem driver for the Sonardyne AvTrak
///
/// Extra parameters:
///
///  - ~buffer_messages:  A bool.  When true messages are buffered and
///    sent on the next received IIS hit.  When false messages are sent
///    immediately.  (The 'B' parameter)
///
///  - ~enable_ranging:  A bool.  When true the modem will report ranging
///    information derived from SMS messages.  (The 'R' parameter)
///
///  - ~sms_code:  An int.  Set the SMS messaging format.  Valid values
///    are:  0: SMS_STD_EXCHANGE, 1: SMS_ACK_ONLY, 3: SMS_NO_RESPONSE
///    (The 'A' parameter)
///
///  - ~sms_parameters:  A string containing extra SMS parameters
///    not covered above to add to outgoing messages.  This is a single
///    string for all parameters and MUST include separating ',' delimiters
///
///  - ~minimum_sms_size:  An int.  Sets the minium size for SMS payloads.
///    Pads SMS payloads with spaces up to the provided minimum size.  This
///    can help introduce an artificial delay when replying to an incoming
///    message (Transmits are triggered on the trailing <CR>)
///    A minimum size of 0 disables this feature.
///
class SonardyneAvtrak : public ds_acomms_modem::ModemBase
{
  DS_DECLARE_PRIVATE(SonardyneAvtrak)

public:
  enum ErrorCode
  {
    OK = 0,
    NO_MODEM_CONNECTION,
    NON_ASCII_DATA,
    MESSAGE_TOO_LARGE,
  };

  /// @brief Different types of SMS transmit protocols
  ///
  /// These are the 'A' parameter placed on outgoing SMS messages.
  /// The default value is "SMS_STD_EXCHANGE" ('A0')
  enum ProtocolCode
  {
    SMS_STD_EXCHANGE = 0,
    SMS_ACK_ONLY = 1,
    SMS_NO_RESPONSE = 3
  };

  explicit SonardyneAvtrak();
  SonardyneAvtrak(int argc, char* argv[], const std::string& name);
  ~SonardyneAvtrak() override;
  DS_DISABLE_COPY(SonardyneAvtrak)

  bool allowInvalidData() const noexcept;
  void enableAllowInvalidData(bool enabled);

  char invalidAsciiChar() const noexcept;
  bool setInvalidAsciiChar(char value);

  bool truncateOutgoingData() const noexcept;
  void enableTruncateOutgoingData(bool enabled);

  bool bufferOutgoingData() const noexcept;
  void enableBufferOutgoingData(bool enabled);

  int smsCode() const noexcept;
  bool setSmsCode(int enabled);

  bool smsRanging() const noexcept;
  void enableSmsRanging(bool enabled);

  std::string extraSmsParameters() const noexcept;
  void setExtraSmsParameters(std::string params);

  size_t minimumSmsSize() const noexcept;
  void setMinimumSmsSize(size_t size);

  ros::Duration statusPollInterval() const noexcept;
  void setStatusPollInterval(ros::Duration duration);

  void enableDiagonstics(const ds_acomms_sonardyne_avtrak_msgs::DIAG& diag);
  ds_acomms_sonardyne_avtrak_msgs::DIAG diagnostics() const noexcept;

  int send(uint16_t remote_address, std::vector<uint8_t> bytes) override;

protected:
  void handleModemMessage(const ds_core_msgs::RawData& bytes) override;
  void publishSmsDiagnostics(const ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg);
  void setupParameters() override;
  void setupTimers() override;
  void setupConnections() override;
  void statusTimerCallback(const ros::TimerEvent&);
  void setupPublishers() override;

public:
  void setup() override;

private:
  std::unique_ptr<SonardyneAvtrakPrivate> d_ptr_;
};
}
#endif  // DS_ACOMMS_SONARDYNE_AVTRAK_H
