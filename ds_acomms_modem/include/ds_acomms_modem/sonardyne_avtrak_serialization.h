/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_MODEM_SONARDYNE_AVTRAK_SERIALIZATION_H
#define DS_ACOMMS_MODEM_SONARDYNE_AVTRAK_SERIALIZATION_H

#include "ds_acomms_serialization/serialization.h"
#include "ds_acomms_sonardyne_avtrak_msgs/AvtrakMessageEnum.h"

#include <algorithm>

namespace ds_acomms_sonardyne_avtrak_msgs
{
ROS_DECLARE_MESSAGE(DIAG)
ROS_DECLARE_MESSAGE(FFT)
ROS_DECLARE_MESSAGE(SMS)
ROS_DECLARE_MESSAGE(SmsDiagnostics)
}

namespace ds_acomms_modem
{
namespace sonardyne_avtrak
{
template <typename T>
uint8_t modem_message_type(const T&)
{
  return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::INVALID_MESSAGE;
}

template <>
uint8_t modem_message_type(const std::string& msg);

#define DEFINE_MODEM_MESSAGE_TYPE(TYPE)                                                                                \
  template <>                                                                                                          \
  constexpr uint8_t modem_message_type(const ds_acomms_sonardyne_avtrak_msgs::TYPE&)                                  \
  {                                                                                                                    \
    return ds_acomms_sonardyne_avtrak_msgs::AvtrakMessageEnum::TYPE;                                                   \
  }

DEFINE_MODEM_MESSAGE_TYPE(DIAG)

#undef DEFINE_MODEM_MESSAGE_TYPE
}
}

namespace ds_acomms_serialization
{
/// @brief Decode a DIAG message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::DIAG& msg);

/// @brief Encode a DIAG message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::DIAG& msg, std::string& buf);

/// @brief Decode a SMS Diagnostics message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg);

/// @brief Encode a SMS Diagnostics message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg, std::string& buf);

/// @brief Decode a FFT message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg);

/// @brief Encode a FFT message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::SmsDiagnostics& msg, std::string& buf);

/// @brief Decode a SMS Diagnostics serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_sonardyne_avtrak_msgs::SMS& msg);

/// @brief Encode a SMS Diagnostics message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_sonardyne_avtrak_msgs::SMS& msg, std::string& buf);
}
#endif  // DS_ACOMMS_MODEM_SONARDYNE_AVTRAK_SERIALIZATION_H
