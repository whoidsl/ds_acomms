/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_MODEM_BASE_H
#define DS_ACOMMS_MODEM_BASE_H

#include "ds_base/ds_process.h"

namespace ds_asio
{
class DsConnection;
}

namespace ds_acomms_msgs
{
ROS_DECLARE_MESSAGE(AcousticModemData)
}

namespace ds_acomms_modem
{
struct ModemBasePrivate;

class ModemBase : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(ModemBase)

public:
  explicit ModemBase();
  ModemBase(int argc, char* argv[], const std::string& name);
  ~ModemBase() override;
  DS_DISABLE_COPY(ModemBase)

  void setLocalAddress(uint16_t address);
  uint16_t localAddress() const noexcept;

  /// @brief Send data to the remote address
  ///
  /// Returns 0 if successful, otherwise returns a device-dependant error code.
  ///
  /// \param remote_address   The accoustic address of the intended recipient
  /// \param bytes  Data to transmit
  /// \return
  virtual int send(uint16_t remote_address, std::vector<uint8_t> bytes) = 0;

  void publishReceivedAcousticModemData(uint16_t remote_address, std::vector<uint8_t> bytes);

protected:
  ds_asio::DsConnection* modemConnection();

  void setupConnections() override;
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupServices() override;

  /// @brief Handle messages received acoustically (NOT from a local source)
  ///
  /// \param bytes
  virtual void handleModemMessage(const ds_core_msgs::RawData& bytes) = 0;

private:
  /// @brief Acoustically transmit the provided message data
  ///
  /// Takes no action if the local address in the data message does not match
  /// the value returned by the modem's localAddress() method.
  ///
  /// Otherwise calls send() with the remote address and data payload of the
  /// AcousticModemData message.
  ///
  /// \param msg
  void sendAcousticModemData(const ds_acomms_msgs::AcousticModemData& msg);

  std::unique_ptr<ModemBasePrivate> d_ptr_;
};
}
#endif  // DS_ACOMMS_MODEM_BASE_H
