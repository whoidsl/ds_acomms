/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_XEOS_IRIDIUM_H
#define DS_ACOMMS_XEOS_IRIDIUM_H

#include "ds_acomms_modem/modem_base.h"

namespace ds_acomms_modem
{
class XeosIridium : public ds_acomms_modem::ModemBase
{
public:
  enum ErrorCode
  {
    OK = 0,
    NO_MODEM_CONNECTION,
    NON_ASCII_DATA,
    MESSAGE_TOO_LARGE,
  };

  enum BeaconStatus
  {
    UNKNOWN = 0,
    NORMAL,    // Mailbox check is performed
    STARTUP,   // Mailbox check is performed
    SURFACED,  // Mailbox check is performed
    SUBMERGED,
    LOW_BATT
  };

  explicit XeosIridium();
  XeosIridium(int argc, char* argv[], const std::string& name);
  ~XeosIridium() override;

  int send(uint16_t remote_address, std::vector<uint8_t> bytes) override;

  void pollBeaconCallback(const ros::TimerEvent&);

protected:
  void handleModemMessage(const ds_core_msgs::RawData& bytes) override;
  void setupParameters() override;
  void setupTimers() override;

private:
  uint32_t max_message_size_;
  uint16_t remote_address_;
  bool truncate_outgoing_data_;
  bool allow_invalid_data_;
  char invalid_ascii_char_;
  bool discard_by_age_;
  bool strip_timestamp_;
  int max_age_seconds_;
  int beacon_status_;

  ros::Timer poll_beacon_timer_;
};
}
#endif
