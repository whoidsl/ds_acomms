/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_MODEM_WHOI_MICROMODEM_SERIALIZATION_H
#define DS_ACOMMS_MODEM_WHOI_MICROMODEM_SERIALIZATION_H

#include "ds_acomms_serialization/serialization.h"
#include "ds_acomms_micromodem_msgs/MicromodemMessageEnum.h"

#include <algorithm>

namespace ds_acomms_micromodem_msgs
{
ROS_DECLARE_MESSAGE(CACFG)
ROS_DECLARE_MESSAGE(CACST)
ROS_DECLARE_MESSAGE(CACYC)
ROS_DECLARE_MESSAGE(CADRQ)
ROS_DECLARE_MESSAGE(CARXD)
ROS_DECLARE_MESSAGE(CAXST)
ROS_DECLARE_MESSAGE(CCCYC)
ROS_DECLARE_MESSAGE(CCTXD)
}

namespace ds_acomms_modem
{
namespace micromodem
{
uint8_t xor_checksum(const std::string& buf);

bool hexstr_to_bytes(char* hexstr, size_t hexstr_len, std::vector<uint8_t>& bytes);

template <typename T>
uint8_t modem_message_type(const T&)
{
  return ds_acomms_micromodem_msgs::MicromodemMessageEnum::INVALID_MESSAGE;
}

template <>
uint8_t modem_message_type(const std::string& msg);

#define DEFINE_MODEM_MESSAGE_TYPE(TYPE)                                                                                \
  template <>                                                                                                          \
  constexpr uint8_t modem_message_type(const ds_acomms_micromodem_msgs::TYPE&)                                         \
  {                                                                                                                    \
    return ds_acomms_micromodem_msgs::MicromodemMessageEnum::TYPE;                                                     \
  }

DEFINE_MODEM_MESSAGE_TYPE(CACFG)
  // No CACST?
DEFINE_MODEM_MESSAGE_TYPE(CACYC)
DEFINE_MODEM_MESSAGE_TYPE(CADRQ)
DEFINE_MODEM_MESSAGE_TYPE(CARXD)
DEFINE_MODEM_MESSAGE_TYPE(CAXST)  // Does CAXST belong here?

DEFINE_MODEM_MESSAGE_TYPE(CCCYC)
DEFINE_MODEM_MESSAGE_TYPE(CCTXD)

#undef DEFINE_MODEM_MESSAGE_TYPE
}
}

// QUESTION(llindzey): Why bother writing serializers for the CA messages?
//    Those will only ever be decoded from the modem.
namespace ds_acomms_serialization
{
/// @brief Decode a CACYC message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACYC& msg);

/// @brief Encode a CACYC message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CACYC& msg, std::string& buf);

/// @brief Decode a CCCYC message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CCCYC& msg);

/// @brief Encode a CCCYC message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CCCYC& msg, std::string& buf);
/// @brief Decode a CARXD message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CARXD& msg);

/// @brief Encode a CARXD message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CARXD& msg, std::string& buf);

/// @brief Decode a CACFG message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACFG& msg);

/// @brief Encode a CACFG message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CACFG& msg, std::string& buf);

/// @brief Decode a CADRQ message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CADRQ& msg);

/// @brief Encode a CADRQ message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CADRQ& msg, std::string& buf);

/// @brief Decode a CCTXD message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CCTXD& msg);

/// @brief Encode a CCTXD message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_micromodem_msgs::CCTXD& msg, std::string& buf);

/// @brief Deserialize a CACST message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CACST& msg);


/// @brief Deserialize a CAXST message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_micromodem_msgs::CAXST& msg);

}

#endif  // DS_ACOMMS_MODEM_WHOI_MICROMODEM_SERIALIZATION_H
