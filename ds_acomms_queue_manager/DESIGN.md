# ds_acomms Design Overview

## Design Requirements

This is a list of broad requirements that the new acomms software MUST provide.

- Simultaneous support of multiple pathways (e.g. avtrak, micro-modem)
- Scheduled broadcasting of messages without interrogation
- Modification of the broadcast schedule at runtime via acoustic commands.
- Interrogated retrieval of information
- Packetization of larger datasets

## Sentry-specific Requirements

The new acomms system absolutely must retain the ability to receive/query information
from the most often used queues:

- SDQ0:  Overall vehicle state
- SDQ7/ACQ8:  Last received message
- SDQ48: Power
- SDQ49: Reson status
- SDQ51: Blueview status
- SDQ57: Camera status

Message routing subsea needs to be handled.  In legacy ROV, the ROV code handles routing messages
based on content to other ROV threads or MC.  A new method of routing within ROS/MC/UDP domains susea
is required.

## Design Choices

### Encoding/Compression

Raw binary formats are not an option if interoperability with the AvTrak is desired, as AvTrak's require
ASCII encoded messages.  The micro-modem protocol used by Sentry already uses hex-encoding for transmission of
binary data types.  The benefits of plain-text messages are obvious to anyone that has had to debug comms issues
over AvTrak with Sentry and moving towards a encoded-only scheme is not something to be done lightly.

Preferably the communications protocol should allow the ability to send plain-text messages as well as encoded
data buffers.  However, this logic of whether the data is encoded or plain text will itself have to be part of the 
ASCII payload for use with AvTraks.  

Whatever the scheme, any encoding scheme should be it's own library to encourage code reuse and prevent errors due
to having multiple implementations that are slightly incompatible. 

## Design Limitations

- AvTrak SMS limited to 128 ASCII bytes (in document, response from Kim pending)

This 128 byte limit will serve as the maximum message size if we do not want to handle multi-message data
for the engineering cruise in April.  NOTE:  Single-message IS NOT the same as single-packet!  The micro-modem
at rate 1 requires two packets for 128 bytes, but can transmit the content in a *single* message.

- Legacy Drivers

Topside ROS drivers are not a priority for development goals ahead of the April engineering cruise.  The design will
have to accomodate existing topside drivers to work, and likely need to use the existing micro-modem driver subsea.

- Message Generics

"Type Erasure" for ROS messages is very difficult in C++.  That is, you can not easily (if at all) decode an unknown
message at runtime and then use the result just like a native object of that type.  There exist tools (ros_introspection)
that provide the means to get at the contents of an unknown message type at run time, but only as a sequence of key-value
pairs.  

In contrast, it seems possible to use Python's "duck-typing" to interact with unknown message types at run time in more
natural ways.

## Proposed Design
                                                       
                                                           |
                                                   Subesea | Topside
    Topics    +--------+-------------+        +--------+   |   +--------+        +-------------+--------+
    --------> |Queue 0 |             |<------>| AvTrak | ~~|~~ | AvTrak |<------>|             | Queue 0|
              |--------+             |        +--------+   |   +--------+        |             +--------|
    --------> |Quene 1 |             |                     |                     |             | Queue 1|
              |--------+     Queue   |        +--------+   |   +--------+        | Queue       +--------|
    --------> |Queue N |     Manager |<------>| uModem | ~~|~~ | uModem |<------>| Manager     | Queue N|
              +----------------------+        +--------+   |   +--------+        +----------------------+
                                ^                          |
                                |                          |                  
                                |
                                |
              +--------------+  |
              |  Generic     |  |
    <-------->|  Topic       |<-'
              |  Pub/Sub     |
              +--------------+
              
              
 The proposed design is symmetric, with both subsea and the eventual topside groups having the same components.
 Traditional acomms use is predominantly one-way (more data passed up from subsea to topside than interrogated
 *from* topside for subsea), but that is not a limitation.  A symmetric design allows for future expansions
 where queries for information may be initiated by vehicles from topside, or other deployed assets.
 
 
### Queues
Queues are the basic unit of the acomms structure.  A queue will:
 
- subscribe to one or more topics to construct a payload.  
- be able to provide the current payload contents as a vector of bytes when asked.
- should NOT be a node itself

### Queue Manager
The Queue Manager 

- Is a node
- A broadcast schedule that can handle modem frequency overlap (e.g. provide time slicing if needed)
- A collection of queues
- Hooks to provide a modem with the current contents of any queue.
- Hooks to publish arbitrary data on arbitrary topics (backdoor)
- Hooks to subscribe to and serialize arbitrary data on arbitrary topics.

### Modem Drivers
Modem drivers are responsible for passing the desired information to the actual modem hardware.  Modem drivers will:

- Is a node for each physical modem
- Handle any encoding if needed/desired
- Handle all packetization requirements, when needed.
- Handle all multi-message segmentation, when needed.
- Hooks to request the next suitable message for transmission from a queue on the queue manager

### Generic Topic Pub/Sub

The old "backdoor_thread" allowed routing of any data to any thread in ROV.  With sufficient knowledge it was possible
to change most of ROV's internal state using this method, which has proved useful in rare occasions.  Moving forward
it is much more likely that we will like to know the state (or change the state) of some node/topic while under way,
and the new ROS/topic scheme makes it much harder to poke at anything not explicitly sent via. acomms.

The "Generic Topic Pub/Sub" node is an attempt to regain the acomms-introspection capabilities, at least with data
that can be accessed via topics.  Services are not in scope at the moment.

This should be possible using a *python* node, as python provides much more convenient run-time introspection abilities
for ROS messages.

## Work Scope

### March 23 demo
- Queues
- Queue Manager
- Plain text messages
- Moxa-based Modem (Just sends/recv's data on UDP)

### Engineering Cruise
- AvTrak modem driver node
- uModem node interfacing with legacy uModem driver
- PlainText/Encoded message options
- Generic Pub/Sub
