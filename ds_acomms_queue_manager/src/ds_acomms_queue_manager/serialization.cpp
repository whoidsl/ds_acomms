/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/DataQueueMessage.h"
#include "ds_acomms_queue_manager/LastReceivedMessage.h"

#include <std_msgs/String.h>
#include <boost/date_time.hpp>

namespace ds_acomms_queue_manager
{
template <>
uint8_t queue_message_type<std::string>(const std::string& buf)
{
  // Skip white space
  const auto start = buf.find_first_not_of(' ');

  // SDQ
  if (buf.compare(start, QueueManagerMessageEnum::SEND_DATA_QUEUE_STR.size(),
                  QueueManagerMessageEnum::SEND_DATA_QUEUE_STR) == 0)
  {
    return QueueManagerMessageEnum::SEND_DATA_QUEUE;
  }

  if (buf.compare(start, QueueManagerMessageEnum::SEND_LAST_RECVD_STR.size(),
                  QueueManagerMessageEnum::SEND_LAST_RECVD_STR) == 0)
  {
    return QueueManagerMessageEnum::SEND_LAST_RECVD;
  }

  return QueueManagerMessageEnum::USER_DEFINED;
}
}

namespace ds_acomms_serialization
{
template <>
bool deserialize(const std::string& buf, ds_acomms_queue_manager::DataQueueMessage& msg)
{
  const auto start = buf.find(ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_DATA_QUEUE_STR);
  msg.error = ds_acomms_queue_manager::DataQueueMessage::ERROR_NONE;

  if (start == buf.npos)
  {
    return false;
  }

  auto ss = std::istringstream(
      buf.substr(start + ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_DATA_QUEUE_STR.size()));

  // Need this temporary storage here because uint8 is treated as a char
  auto queue = 0;
  ss >> queue;

  // No number?  Bad string
  if (!ss)
  {
    return false;
  }

  // Negative queue?  Invalid queue number.
  if (queue < 0)
  {
    msg.error = ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE;
    return true;
  }

  // Queue too large?  Invalid queue number.
  if (queue > std::numeric_limits<uint8_t>::max())
  {
    msg.error = ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE;
    return true;
  }

  msg.queue = static_cast<uint8_t>(queue);

  // Find payload marker
  ss.ignore(std::numeric_limits<std::streamsize>::max(), ':');

  // No payload?  Nothing to do
  if (!ss.good())
  {
    return true;
  }

  // Read the rest of the buffer as the payload
  auto payload_str = ss.str().substr(ss.tellg());
  if (payload_str.compare(ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE_STR) == 0)
  {
    msg.error = ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE;
    return true;
  }

  if (payload_str.compare(ds_acomms_queue_manager::DataQueueMessage::ERROR_NO_DATA_STR) == 0)
  {
    msg.error = ds_acomms_queue_manager::DataQueueMessage::ERROR_NO_DATA;
    return true;
  }

  msg.payload.resize(payload_str.size());
  std::copy(std::begin(payload_str), std::end(payload_str), std::begin(msg.payload));
  return true;
}

template <>
void serialize(const ds_acomms_queue_manager::DataQueueMessage& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_DATA_QUEUE_STR << " " << static_cast<int>(msg.queue);
  switch (msg.error)
  {
    case ds_acomms_queue_manager::DataQueueMessage::ERROR_NONE:
      if (!msg.payload.empty())
      {
        os << ":" << std::string(std::begin(msg.payload), std::end(msg.payload));
      }
      break;
    case ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE:
      os << ":" << ds_acomms_queue_manager::DataQueueMessage::ERROR_INVALID_QUEUE_STR;
      break;
    case ds_acomms_queue_manager::DataQueueMessage::ERROR_NO_DATA:
      os << ":" << ds_acomms_queue_manager::DataQueueMessage::ERROR_NO_DATA_STR;
      break;
    default:
      ROS_ERROR_STREAM("BUG!! Unhandled error code: '" << msg.error << "' in string serialization for "
                                                                       "DataQueueMessage");
      break;
  }
  buf = os.str();
}

template <>
void serialize(const ds_acomms_queue_manager::LastReceivedMessage& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_LAST_RECVD_STR << " " << static_cast<int>(msg.local_addr)
     << "," << static_cast<int>(msg.remote_addr);

  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));

  switch (msg.error)
  {
    case ds_acomms_queue_manager::LastReceivedMessage::ERROR_NONE:
      if (!msg.payload.empty())
      {
        os << ":" << msg.stamp.toBoost() << "," << std::string{ std::begin(msg.payload), std::end(msg.payload) };
      }
      break;
    case ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS:
      os << ":" << msg.stamp.toBoost() << ","
         << ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS_STR;
      break;
    case ds_acomms_queue_manager::LastReceivedMessage::ERROR_NO_MESSAGES:
      os << ":" << msg.stamp.toBoost() << "," << ds_acomms_queue_manager::LastReceivedMessage::ERROR_NO_MESSAGES_STR;
      break;
    default:
      ROS_ERROR_STREAM("BUG!! Unhandled error code: '" << msg.error << "' in string serialization for "
                                                                       "LastReceivedMessage");
      break;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_acomms_queue_manager::LastReceivedMessage& msg)
{
  const auto start = buf.find(ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_LAST_RECVD_STR);
  msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_NONE;

  if (start == buf.npos)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }

  auto ss = std::istringstream(
      buf.substr(start + ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_LAST_RECVD_STR.size()));

  int addr;
  ss >> addr;
  if (!ss)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }

  if (addr < 0 || addr > std::numeric_limits<ds_acomms_queue_manager::LastReceivedMessage::_local_addr_type>::max())
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS;
    return false;
  }

  msg.local_addr = static_cast<ds_acomms_queue_manager::LastReceivedMessage::_local_addr_type>(addr);
  ss.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  if (!ss)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }

  ss >> addr;
  if (!ss)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }
  if (addr < 0 || addr > std::numeric_limits<ds_acomms_queue_manager::LastReceivedMessage::_remote_addr_type>::max())
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS;
    return false;
  }

  msg.remote_addr = static_cast<ds_acomms_queue_manager::LastReceivedMessage::_remote_addr_type>(addr);

  ss.ignore(std::numeric_limits<std::streamsize>::max(), ':');

  // No payload?  Nothing left to do
  if (!ss.good())
  {
    return true;
  }

  // Attempt to parse a time -- should be present on all responses.
  auto facet = new boost::posix_time::time_input_facet("%H:%M");
  ss.imbue(std::locale(ss.getloc(), facet));

  auto time = boost::posix_time::ptime{};
  ss >> time;

  if (!ss)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }

  // Date parsing.. is complicated..  We only send HH:MM, so need to be mindful of
  // sending things with timestamps in the previous day.

  // This is the current time according to ROS
  auto boost_stamp = ros::Time::now().toBoost();
  // And the current date
  auto boost_date = boost_stamp.date();

  // If the parsed hours are GREATER than the current hours, then we have a situation where
  // we are sending a timestamp from the previous day.  Subtract one day from the current date
  if (time.time_of_day().hours() > boost_stamp.time_of_day().hours())
  {
    boost_date -= boost::gregorian::days(1);
  }

  // Create our timestamp.
  boost_stamp = boost::posix_time::ptime{ boost_date, time.time_of_day() };
  msg.stamp = msg.stamp.fromBoost(boost_stamp);

  // Ignore the next ','
  ss.ignore(std::numeric_limits<std::streamsize>::max(), ',');
  if (!ss)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_MESSAGE;
    return false;
  }

  // Read the rest of the buffer as the payload
  auto payload_str = ss.str().substr(ss.tellg());

  // Check against error strings
  if (payload_str.compare(ds_acomms_queue_manager::LastReceivedMessage::ERROR_NO_MESSAGES_STR) == 0)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_NO_MESSAGES;
    return true;
  }

  if (payload_str.compare(ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS_STR) == 0)
  {
    msg.error = ds_acomms_queue_manager::LastReceivedMessage::ERROR_INVALID_ADDRESS;
    return true;
  }

  // OK!  valid data, load into payload.
  msg.payload.resize(payload_str.size());
  std::copy(std::begin(payload_str), std::end(payload_str), std::begin(msg.payload));
  return true;
}

/// @brief Decode a DataQueueString message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, std_msgs::String& msg)
{
  msg.data = buf;
  return true;
}

/// @brief Encode a DataQueueString message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const std_msgs::String& msg, std::string& buf)
{
  buf = msg.data;
}
}