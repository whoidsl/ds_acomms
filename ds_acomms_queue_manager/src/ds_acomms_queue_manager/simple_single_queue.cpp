/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms/simple_single_queue.h"

namespace ds_acomms_queue_manager
{
SimpleSingleQueue::SimpleSingleQueue() = default;

SimpleSingleQueue::~SimpleSingleQueue() = default;

boost::any SimpleSingleQueue::next()
{
  return item_;
}

void SimpleSingleQueue::setStringEncoder(StringEncoder func)
{
  string_enc_ = std::move(func);
}

SimpleSingleQueue::StringEncoder& SimpleSingleQueue::stringEncoder()
{
  return string_enc_;
}

void SimpleSingleQueue::setByteEncoder(ByteEncoder func)
{
  byte_enc_ = std::move(func);
}

SimpleSingleQueue::ByteEncoder& SimpleSingleQueue::byteEncoder()
{
  return byte_enc_;
}

bool SimpleSingleQueue::encode(boost::any& msg, std::vector<uint8_t>& buf, bool as_string)
{
  if (as_string)
  {
    if (!string_enc_)
    {
      return false;
    }

    std::string sbuf{};
    string_enc_(msg, sbuf);
    buf = std::vector<uint8_t>{ std::begin(sbuf), std::end(sbuf) };
    return true;
  }

  if (!byte_enc_)
  {
    return false;
  }
  byte_enc_(msg, buf);
  return true;
}
}
