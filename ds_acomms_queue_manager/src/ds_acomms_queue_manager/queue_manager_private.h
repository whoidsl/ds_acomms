/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_PRIVATE_H
#define DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_PRIVATE_H

#include "ds_acomms_queue_manager/queue_manager.h"

namespace ds_acomms_queue_manager
{
struct QueueManagerPrivate
{
  using StampedMessage = std::pair<ros::Time, std::vector<uint8_t>>;
  using ModemHistory = std::unordered_map<QueueManager::ModemAddress, StampedMessage>;

  using QueueHistory = std::unordered_map<QueueManager::ModemAddress, ModemHistory>;

  void setLastReceived(QueueManager::ModemAddress local_addr, QueueManager::ModemAddress remote_addr,
                       std::vector<uint8_t> payload)
  {
    last_recvd_[local_addr][remote_addr] = std::make_pair(ros::Time::now(), std::move(payload));
  }

  void getLastReceived(QueueManager::ModemAddress local_addr, QueueManager::ModemAddress remote_addr, ros::Time& stamp,
                       std::vector<uint8_t>& buf)
  {
    buf.clear();
    const auto local_it = last_recvd_.find(local_addr);
    if (local_it == std::end(last_recvd_))
    {
      return;
    }

    const auto remote_it = local_it->second.find(remote_addr);
    if (remote_it == std::end(local_it->second))
    {
      return;
    }

    stamp = remote_it->second.first;
    buf = remote_it->second.second;
  }

  QueueHistory last_recvd_;
  QueueManager::QueueMap queue_map_;

  ros::Subscriber incoming_sub_;
  ros::Publisher outgoing_pub_;
};
}

#endif  // DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_PRIVATE_H
