/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms/queue.h"

#include <deque>
namespace ds_acomms_queue_manager
{
Queue::Queue(size_t capacity) : capacity_(capacity), retain_last_(false)
{
}

Queue::~Queue() = default;

size_t Queue::capacity() const noexcept
{
  return capacity_;
}

void Queue::setCapacity(size_t capacity)
{
  if (capacity >= queue_.size())
  {
    capacity_ = capacity;
    return;
  }

  const auto start = std::begin(queue_);
  const auto end = std::next(start, queue_.size() - capacity);
  queue_.erase(start, end);
  capacity_ = capacity;
}

size_t Queue::size() const noexcept
{
  return queue_.size();
}

void Queue::insert(boost::any msg, size_t pos)
{
  // Short-circuit if capacity is 1, just replace the head.
  if (capacity_ == 1 && queue_.size() > 0)
  {
    queue_[0] = std::move(msg);
    return;
  }

  // Make space if necessary by removing the first message.
  if (queue_.size() >= capacity_)
  {
    queue_.pop_front();
  }

  if (pos >= queue_.size())
  {
    queue_.push_back(std::move(msg));
    return;
  }

  const auto it = std::next(std::begin(queue_), pos);
  queue_.insert(it, std::move(msg));
}

void Queue::erase(size_t pos)
{
  if (queue_.empty())
  {
    return;
  }

  if (pos >= queue_.size())
  {
    queue_.pop_back();
    return;
  }

  const auto it = std::next(std::begin(queue_), pos);
  queue_.erase(it);
}

boost::any Queue::peek(size_t pos)
{
  if (queue_.empty())
  {
    return {};
  }

  if (pos >= queue_.size())
  {
    return queue_.at(queue_.size() - 1);
  }

  return queue_.at(pos);
}

void Queue::swap(boost::any& rhs, size_t pos)
{
  if (queue_.empty())
  {
    insert(rhs);
    auto empty = boost::any{};
    rhs.swap(empty);
    return;
  }

  pos = std::min(queue_.size() - 1, pos);
  queue_.at(pos).swap(rhs);
}

boost::any Queue::replace(boost::any rhs, size_t pos)
{
  if (queue_.empty())
  {
    insert(rhs);
    auto empty = boost::any{};
    return {};
  }

  pos = std::min(queue_.size() - 1, pos);
  const auto v = queue_.at(pos);
  queue_.at(pos).swap(rhs);
  return v;
}

boost::any Queue::next()
{
  return take(0);
}

boost::any Queue::take(size_t pos)
{
  if (queue_.empty())
  {
    return {};
  }

  auto it = std::begin(queue_);
  if (pos >= queue_.size())
  {
    it = std::prev(std::end(queue_));
  }
  else
  {
    it = std::next(it, pos);
  }

  auto msg = *it;
  if (queue_.size() > 1 || !retain_last_)
  {
    queue_.erase(it);
  }
  return msg;
}

void Queue::setRetainLastEnabled(bool enabled)
{
  retain_last_ = enabled;
}
bool Queue::retainLastEnabled() const noexcept
{
  return retain_last_;
}

void Queue::setStringEncoder(Queue::StringEncoder func)
{
  string_enc_ = std::move(func);
}

Queue::StringEncoder& Queue::stringEncoder()
{
  return string_enc_;
}

void Queue::setByteEncoder(Queue::ByteEncoder func)
{
  byte_enc_ = std::move(func);
}

Queue::ByteEncoder& Queue::byteEncoder()
{
  return byte_enc_;
}

bool Queue::encode(boost::any& msg, std::vector<uint8_t>& buf, bool as_string)
{
  if (as_string)
  {
    if (!string_enc_)
    {
      return false;
    }

    std::string sbuf{};
    string_enc_(msg, sbuf);
    buf = std::vector<uint8_t>{ std::begin(sbuf), std::end(sbuf) };
    return true;
  }

  if (!byte_enc_)
  {
    return false;
  }
  byte_enc_(msg, buf);
  return true;
}
}