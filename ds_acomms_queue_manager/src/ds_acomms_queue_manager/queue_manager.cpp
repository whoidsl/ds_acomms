/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#define BYTE_ENCODING_ENABLED 0

#include "ds_acomms_queue_manager/queue_manager.h"
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/DataQueueMessage.h"
#include "ds_acomms_queue_manager/LastReceivedMessage.h"
#include "ds_acomms_queue_manager/QueueManagerMessageEnum.h"

#include "ds_acomms_msgs/AcousticModemData.h"

#include "queue_manager_private.h"

namespace ds_acomms_queue_manager
{
QueueManager::QueueManager() : DsProcess(), d_ptr_(std::unique_ptr<QueueManagerPrivate>(new QueueManagerPrivate))
{
}
QueueManager::QueueManager(int argc, char** argv, std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<QueueManagerPrivate>(new QueueManagerPrivate))
{
}

QueueManager::~QueueManager() = default;

void QueueManager::setup()
{
  DsProcess::setup();
  setupQueues();
}

bool QueueManager::addQueue(QueueManager::QueueId id, std::unique_ptr<QueueInterface> queue)
{
  DS_D(QueueManager);
  if (d->queue_map_.count(id))
  {
    ROS_WARN_STREAM("Replacing existing queue with id: " << static_cast<int>(id));
  }

  if (!queue)
  {
    ROS_ERROR_STREAM("Could not add queue with id: " << static_cast<int>(id) << ".  Invalid queue pointer.");
    return false;
  }

#if BYTE_ENCODING_ENABLED
  // TODO: Enable byte encoding
  if (!queue->byteEncoder())
  {
    ROS_ERROR_STREAM("Could not add queue with id: " << static_cast<int>(id) << ". No byte encoder provided.");
    return false;
  }

  if (!queue->stringEncoder())
  {
    ROS_ERROR_STREAM("Could not add queue with id: " << static_cast<int>(id) << ". No string encoder provided.");
    return false;
  }
#endif

  d->queue_map_.emplace(id, std::move(queue));
  return true;
}

void QueueManager::removeQueue(QueueManager::QueueId id)
{
  DS_D(QueueManager);
  const auto it = d->queue_map_.find(id);
  if (it == std::end(d->queue_map_))
  {
    ROS_WARN_STREAM("Unable to remove queue with id: " << id << ". No queue by such id exists.");
    return;
  }

  d->queue_map_.erase(it);
}

QueueInterface* QueueManager::queue(QueueManager::QueueId id)
{
  DS_D(QueueManager);
  const auto it = d->queue_map_.find(id);
  if (it == std::end(d->queue_map_))
  {
    return nullptr;
  }

  return it->second.get();
}

void QueueManager::setupQueues()
{
  auto nh = nodeHandle();
  DS_D(QueueManager);
  for (auto& it : d->queue_map_)
  {
    if (!it.second)
    {
      ROS_FATAL_STREAM("Could not setup Queue with id: " << it.first << ", queue is null.");
      ROS_BREAK();
    }

    it.second->setup(nh);
  }
}

void QueueManager::encodeQueueData(QueueManager::QueueId id, std::vector<uint8_t>& buf, bool as_string)
{
  // TODO:  Enable byte encoding
  // For now, force string encoding.
  as_string = true;
  auto msg = DataQueueMessage{};
  msg.queue = id;
  msg.error = DataQueueMessage::ERROR_NONE;

  boost::any value;

  auto q = queue(id);
  if (q)
  {
    if (q->empty())
    {
      msg.error = DataQueueMessage::ERROR_NO_DATA;
    }
    else
    {
      q->encodeNext(msg.payload, as_string);
    }
  }
  else
  {
    msg.error = DataQueueMessage::ERROR_INVALID_QUEUE;
  }

  // Finally, encode the whole message into a flat buffer ready for sending.
  ds_acomms_serialization::serialize(msg, buf, as_string);
}

void QueueManager::handleIncomingModemData(const ds_acomms_msgs::AcousticModemData& msg)
{
  if (msg.payload.empty())
  {
    ROS_WARN_STREAM("Ignoring empty message from remote addr: " << msg.remote_addr
                                                                << " on modem with addr: " << msg.local_addr);
    return;
  }

  const auto as_string = ds_acomms_serialization::is_string_encoded(msg.payload);
  auto type_id = 0u;
  if (as_string)
  {
    const auto str_buf = std::string{ std::begin(msg.payload), std::end(msg.payload) };
    type_id = queue_message_type(str_buf);
    ROS_INFO_STREAM("rx " << static_cast<int>(msg.local_addr) << "<-" << static_cast<int>(msg.remote_addr) << ": '"
                          << str_buf << "'");
  }
  else
  {
#if BYTE_ENCODING_ENABLED
    type_id = message_type(msg.payload)
#endif
        ROS_ERROR_STREAM("Ignoring binary encoded message from remote addr: "
                         << msg.remote_addr << " on modem with addr: " << msg.local_addr);
    return;
  }

  DS_D(QueueManager);

  if (type_id == QueueManagerMessageEnum::SEND_DATA_QUEUE)
  {
    auto queue_msg = DataQueueMessage{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, queue_msg, as_string);

    // Encode and send out the response
    if (ok)
    {
      auto buf = std::vector<uint8_t>{};
      ROS_INFO_STREAM("Responding to SDQ " << static_cast<int>(queue_msg.queue)
                                           << " from remote address: " << static_cast<int>(msg.remote_addr));
      if (queue_msg.error == DataQueueMessage::ERROR_NONE)
      {
        // Data queue, no error condition
        encodeQueueData(queue_msg.queue, buf, as_string);

        // Check if there is extra payload to process in the queue request
        // As a result of ds_acomms_serialization::deserialize, queue_msg.payload
        // is the string after the ":"

        if (!queue_msg.payload.empty())
        {
          auto ok = handleQueueRequestAdditionalPayload(queue_msg.payload, as_string);
        }
      }
      else
      {
        // Data queue, with error condtion (e.g. EMPTY or INVALID)
        ROS_WARN_STREAM("SDQ " << static_cast<int>(queue_msg.queue)
                               << " error code: " << static_cast<int>(queue_msg.error));
        ds_acomms_serialization::serialize(queue_msg, buf, as_string);
      }

      if (as_string)
      {
        ROS_INFO_STREAM("tx " << static_cast<int>(msg.local_addr) << "->" << static_cast<int>(msg.remote_addr) << ": '"
                              << std::string(std::begin(buf), std::end(buf)) << "'");
      }

      sendModemData(msg.local_addr, msg.remote_addr, std::move(buf));
    }
    else
    {
      ROS_ERROR_STREAM("Unable to decode incoming SDQ message");
    }
  }
  else if (type_id == QueueManagerMessageEnum::SEND_LAST_RECVD)
  {
    auto last_msg = LastReceivedMessage{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, last_msg, as_string);
    if (ok)
    {
      ROS_INFO_STREAM("Responding to SLR request from remote address: " << static_cast<int>(msg.remote_addr));
      d->getLastReceived(last_msg.local_addr, last_msg.remote_addr, last_msg.stamp, last_msg.payload);

      // Empty payload?  No history for that pair.
      if (last_msg.payload.empty())
      {
        last_msg.stamp = ros::Time::now();
        last_msg.error = LastReceivedMessage::ERROR_NO_MESSAGES;
      }

      auto buf = std::vector<uint8_t>{};
      ds_acomms_serialization::serialize(last_msg, buf, as_string);
      if (as_string)
      {
        ROS_INFO_STREAM("tx " << static_cast<int>(msg.local_addr) << "->" << static_cast<int>(msg.remote_addr) << ": '"
                              << std::string(std::begin(buf), std::end(buf)) << "'");
      }
      sendModemData(msg.local_addr, msg.remote_addr, std::move(buf));
    }
  }
  else if (type_id >= QueueManagerMessageEnum::USER_DEFINED)
  {
      if (!handleCustomQueueMessage(msg, as_string)) {
          ROS_ERROR_STREAM("Failed to parse user defined message: " << msg << ", " << as_string);
      }
  }
  else
  {
    ROS_ERROR_STREAM("BUG!! message id: '" << type_id << "' should be added to ds_acomms::message_type!!");
  }

  // Save message as the last received.
  d->setLastReceived(msg.local_addr, msg.remote_addr, msg.payload);
}

void QueueManager::sendModemData(uint16_t local_addr, uint16_t remote_addr, std::vector<uint8_t> data)
{
  if (data.empty())
  {
    ROS_ERROR_STREAM("Attempt to send data packet of length 0 from '" << local_addr << "' to '" << remote_addr << "'.");
    return;
  }

  auto msg = ds_acomms_msgs::AcousticModemData{};
  msg.stamp = ros::Time::now();
  msg.local_addr = local_addr;
  msg.remote_addr = remote_addr;
  msg.payload = std::move(data);

  DS_D(QueueManager);
  d->outgoing_pub_.publish(msg);
}

void QueueManager::setupSubscriptions()
{
  DsProcess::setupSubscriptions();

  const auto incoming_topic_name = ros::param::param<std::string>("~incoming_data_topic", "incoming_acomm_data");
  DS_D(QueueManager);
  d->incoming_sub_ = nodeHandle().subscribe(incoming_topic_name, 1, &QueueManager::handleIncomingModemData, this);
}

void QueueManager::setupPublishers()
{
  DsProcess::setupPublishers();
  const auto outgoing_topic_name = ros::param::param<std::string>("~outgoing_data_topic", "outgoing_acomm_data");
  DS_D(QueueManager);
  d->outgoing_pub_ = nodeHandle().advertise<ds_acomms_msgs::AcousticModemData>(outgoing_topic_name, 1, false);
}
}
