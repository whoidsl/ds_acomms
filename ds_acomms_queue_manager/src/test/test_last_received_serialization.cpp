/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/LastReceivedMessage.h"

#include <gtest/gtest.h>
#include <boost/date_time.hpp>

using namespace ds_acomms_queue_manager;

TEST(LastReceivedMessage, message_type)
{
  auto msg = LastReceivedMessage{};
  EXPECT_EQ(QueueManagerMessageEnum::SEND_LAST_RECVD, queue_message_type(msg));
}

TEST(LastReceivedMessage, invalid_message)
{
  auto buf = std::string{ "THIS IS NOT A VALID MESSAGE" };
  auto result = LastReceivedMessage{};
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_MESSAGE, result.error);

  buf = std::string{ "SLR " };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_MESSAGE, result.error);

  buf = std::string{ "SLR 500" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_MESSAGE, result.error);

  buf = std::string{ "SLR 500," };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_MESSAGE, result.error);

  buf = std::string{ "SLR ,500" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_MESSAGE, result.error);
}

TEST(LastReceivedMessage, deserialize_bad_address_numbers)
{
  auto buf = std::string{ "SLR -5,100" };
  auto result = LastReceivedMessage{};
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_ADDRESS, result.error);

  buf = std::string{ "SLR 555555555,50" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_INVALID_ADDRESS, result.error);
}

TEST(LastReceivedMessage, deserialize_string_no_payload)
{
  auto expected = LastReceivedMessage{};
  expected.local_addr = 0;
  expected.remote_addr = 1;

  auto result = LastReceivedMessage{};

  auto buf = std::string{ "SLR 0,1" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_TRUE(result.payload.empty());

  buf = std::string{ "SLR0, 1" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_TRUE(result.payload.empty());

  buf = std::string{ "   SLR    0,    1" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_TRUE(result.payload.empty());
}

TEST(LastReceivedMessage, deserialize_string_with_payload)
{
  auto expected = LastReceivedMessage{};
  expected.local_addr = 5;
  expected.remote_addr = 50;

  expected.stamp = ros::Time::now();
  const auto boost_stamp = expected.stamp.toBoost();

  const auto hours = boost_stamp.time_of_day().hours();
  const auto minutes = boost_stamp.time_of_day().minutes();

  const auto data_str = std::string{ "Some queue data" };
  expected.payload.resize(data_str.size());
  std::copy(std::begin(data_str), std::end(data_str), std::begin(expected.payload));

  auto os = std::ostringstream{};
  os << std::setw(2) << std::setfill('0') << hours << ":" << std::setw(2) << std::setfill('0') << minutes;
  const auto timestamp_str = os.str();
  const auto payload_str = timestamp_str + "," + data_str;

  auto result = LastReceivedMessage{};
  auto buf = std::string{ "SLR 5,50:" } + payload_str;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
  EXPECT_EQ(expected.payload, result.payload);

  buf = std::string{ "SLR 5,50  :" } + payload_str;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
  EXPECT_EQ(expected.payload, result.payload);

  buf = std::string{ "   SLR5,50:" } + payload_str;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
  EXPECT_EQ(expected.payload, result.payload);
}

TEST(LastReceivedMessage, deserialize_string_with_timestamp_wrap)
{
  auto expected = LastReceivedMessage{};
  expected.local_addr = 5;
  expected.remote_addr = 50;

  // rewind the clock 23 hours
  expected.stamp = ros::Time::now() - ros::Duration(23 * 3600);
  const auto boost_stamp = expected.stamp.toBoost();

  const auto hours = boost_stamp.time_of_day().hours();
  const auto minutes = boost_stamp.time_of_day().minutes();

  const auto data_str = std::string{ "Some queue data" };
  expected.payload.resize(data_str.size());
  std::copy(std::begin(data_str), std::end(data_str), std::begin(expected.payload));

  auto os = std::ostringstream{};
  os << std::setw(2) << std::setfill('0') << hours << ":" << std::setw(2) << std::setfill('0') << minutes;
  const auto timestamp_str = os.str();
  const auto payload_str = timestamp_str + "," + data_str;

  auto result = LastReceivedMessage{};
  auto buf = std::string{ "SLR 5,50:" } + payload_str;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(LastReceivedMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.local_addr, result.local_addr);
  EXPECT_EQ(expected.remote_addr, result.remote_addr);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
  EXPECT_EQ(expected.payload, result.payload);
}

TEST(LastReceivedMessage, serialize_string_no_payload)
{
  const auto expected = std::string{ "SLR 5,50" };
  auto msg = LastReceivedMessage{};
  msg.local_addr = 5;
  msg.remote_addr = 50;

  auto result = std::string{};
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

TEST(LastReceivedMessage, serialize_string_with_payload)
{
  auto msg = LastReceivedMessage{};
  msg.local_addr = 5;
  msg.remote_addr = 50;

  msg.stamp = ros::Time::now();
  const auto boost_stamp = msg.stamp.toBoost();

  const auto hours = boost_stamp.time_of_day().hours();
  const auto minutes = boost_stamp.time_of_day().minutes();

  const auto data_str = std::string{ "Some payload data to send" };
  msg.payload.resize(data_str.size());
  std::copy(std::begin(data_str), std::end(data_str), std::begin(msg.payload));

  auto os = std::ostringstream{};
  os << std::setfill('0') << std::setw(2) << hours << ":" << std::setw(2) << minutes;
  const auto time_str = os.str();
  const auto expected = "SLR 5,50:" + time_str + "," + data_str;

  auto result = std::string{};
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

TEST(LastReceivedMessage, serialize_with_errors)
{
  auto msg = LastReceivedMessage{};
  msg.local_addr = 5;
  msg.remote_addr = 50;
  msg.stamp = ros::Time::now();
  const auto boost_stamp = msg.stamp.toBoost();

  const auto hours = boost_stamp.time_of_day().hours();
  const auto minutes = boost_stamp.time_of_day().minutes();

  auto os = std::ostringstream{};
  os << std::setfill('0') << std::setw(2) << hours << ":" << std::setw(2) << minutes;
  const auto time_str = os.str();

  msg.error = LastReceivedMessage::ERROR_NO_MESSAGES;
  auto result = std::string{};

  auto expected = std::string{ "SLR 5,50:" } + time_str + "," + LastReceivedMessage::ERROR_NO_MESSAGES_STR;
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);

  msg.error = LastReceivedMessage::ERROR_INVALID_ADDRESS;
  expected = std::string{ "SLR 5,50:" } + time_str + "," + LastReceivedMessage::ERROR_INVALID_ADDRESS_STR;
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
