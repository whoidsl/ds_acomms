/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#if 1
#include "ds_acomms/rosmsg_serialization.h"
#include "ds_acomms/queue_type_visitor.h"

#include "std_msgs/String.h"
#include "std_msgs/Duration.h"
#include "std_msgs/Empty.h"

#include <gtest/gtest.h>

namespace ds_acomms
{
std::string to_string(std_msgs::String& msg)
{
  return msg.data;
}

std::string to_string(std_msgs::Duration& msg)
{
  return std::to_string(msg.data.toSec()) + " seconds";
}
}

namespace qm = ds_acomms;

class QueueTypeVisitorTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    visitor_.addType<std_msgs::String>();
    visitor_.addType<std_msgs::Duration>();
  }

  ds_acomms::QueueTypeVisitor visitor_;
};

TEST_F(QueueTypeVisitorTest, encode_string)
{
  const auto expected_string_payload = std::string{ "A test string payload" };
  auto string_msg = std_msgs::String{};
  string_msg.data = expected_string_payload;

  auto any = boost::any(string_msg);
  auto encode_buf = std::string{};

  ASSERT_TRUE(visitor_.encode(any, encode_buf));
  EXPECT_STREQ(expected_string_payload.data(), encode_buf.data());

  auto duration_msg = std_msgs::Duration{};
  const auto t = 10.5;
  const auto expected_duration_payload = std::to_string(t) + " seconds";
  duration_msg.data.fromSec(t);
  any = boost::any(duration_msg);

  ASSERT_TRUE(visitor_.encode(any, encode_buf));
  EXPECT_STREQ(expected_duration_payload.data(), encode_buf.data());
}

TEST_F(QueueTypeVisitorTest, encode_string_failure)
{
  const auto msg = std_msgs::Empty{};
  auto any = boost::any(msg);

  auto encode_buf = std::string{};
  ASSERT_FALSE(visitor_.encode(any, encode_buf));
}

TEST_F(QueueTypeVisitorTest, encode_bytes)
{
  const auto expected_string_payload = std::string{ "A test string payload" };
  auto string_msg = std_msgs::String{};
  string_msg.data = expected_string_payload;

  auto any = boost::any(string_msg);
  auto encode_buf = std::vector<uint8_t>{};

  ASSERT_TRUE(visitor_.encode(any, encode_buf));
  ASSERT_GT(encode_buf.size(), 0);

#if 0
  std::cerr << encode_buf.size() << std::endl;
  for (const auto&it: encode_buf)
  {
    std::cerr << it << ", ";
  }
  std::cerr << std::endl;
#endif
  auto string_msg_deserialized = std_msgs::String{};
  qm::from_bytes(encode_buf, string_msg_deserialized);

  ASSERT_FALSE(string_msg_deserialized.data.empty());
  ASSERT_EQ(string_msg.data, string_msg_deserialized.data);
#if 0
  EXPECT_STREQ(expected_string_payload.data(), encode_buf.data());

  auto duration_msg = std_msgs::Duration{};
  const auto t = 10.5;
  const auto expected_duration_payload = std::to_string(t) + " seconds";
  duration_msg.data.fromSec(t);
  any = boost::any(duration_msg);

  ASSERT_TRUE(visitor_.encode(any, encode_buf));
  EXPECT_STREQ(expected_duration_payload.data(), encode_buf.data());
#endif
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

#else
#include <boost/any.hpp>
#include <unordered_map>
#include <functional>
#include <iostream>
#include <vector>

struct type_info_hash
{
  std::size_t operator()(std::type_info const& t) const
  {
    return t.hash_code();
  }
};

struct equal_ref
{
  template <typename T>
  bool operator()(std::reference_wrapper<T> a, std::reference_wrapper<T> b) const
  {
    return a.get() == b.get();
  }
};

struct any_visitor
{
  using type_info_ref = std::reference_wrapper<std::type_info const>;
  using function = std::function<void(boost::any&)>;
  std::unordered_map<type_info_ref, function, type_info_hash, equal_ref> fs;

  template <typename T>
  void insert_visitor(std::function<void(T&)> f)
  {
    fs.insert(std::make_pair(std::ref(typeid(T)), function([&f](boost::any& x) { f(boost::any_cast<T&>(x)); })));
  }

  bool operator()(boost::any& x)
  {
    auto it = fs.find(x.type());
    if (it != fs.end())
    {
      it->second(x);
      return true;
    }
    else
    {
      return false;
    }
  }
};

struct abc
{
};

void fa(int i)
{
  std::cout << "fa(" << i << ")" << std::endl;
}
void fb(abc)
{
  std::cout << "fb(abc())" << std::endl;
}

int main()
{
  any_visitor f;
  f.insert_visitor<int>(fa);
  f.insert_visitor<abc>(fb);

  std::vector<boost::any> xs;
  xs.push_back(1);
  xs.push_back(abc());
  xs.push_back(1.5);

  for (auto& x : xs)
  {
    if (!f(x))
      std::cout << "no visitor registered" << std::endl;
  }
}
#endif
