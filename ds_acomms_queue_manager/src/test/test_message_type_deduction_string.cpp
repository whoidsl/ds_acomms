/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/QueueManagerMessageEnum.h"

#include <gtest/gtest.h>

using namespace ds_acomms_queue_manager;

TEST(MessageTypeDeductionString, unknown_type)
{
  auto buf = std::string{ "MCA SHFTABS 43 -23" };
  const auto result = queue_message_type(buf);
  EXPECT_EQ(QueueManagerMessageEnum::USER_DEFINED, result);
}

TEST(MessageTypeDeductionString, leading_spaces)
{
  auto buf = std::string{ "    SDQ 0" };
  const auto result = queue_message_type(buf);
  EXPECT_EQ(QueueManagerMessageEnum::SEND_DATA_QUEUE, result);
}

TEST(MessageTypeDeductionString, standard_types)
{
  EXPECT_EQ(QueueManagerMessageEnum::SEND_DATA_QUEUE, queue_message_type(QueueManagerMessageEnum::SEND_DATA_QUEUE_STR));
  EXPECT_EQ(QueueManagerMessageEnum::SEND_LAST_RECVD, queue_message_type(QueueManagerMessageEnum::SEND_LAST_RECVD_STR));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
