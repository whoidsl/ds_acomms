/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/queue.h"
#include "std_msgs/String.h"

#include <gtest/gtest.h>

using TestMsg = std_msgs::String;

using namespace ds_acomms_queue_manager;

TEST(QueueAccess, take_next_empty)
{
  Queue<TestMsg> queue(1);
  auto msg = TestMsg{};
  ASSERT_FALSE(queue.take(msg));
}

TEST(QueueAccess, remove_empty)
{
  Queue<TestMsg> queue(1);
  ASSERT_NO_THROW(queue.erase(0));
}

TEST(QueueAccess, take)
{
  Queue<TestMsg> queue(1);
  queue.setCapacity(1);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;

  queue.insert(msg);
  ASSERT_EQ(1, queue.size());

  auto next = TestMsg{};
  ASSERT_TRUE(queue.take(next));
  ASSERT_EQ(0, queue.size());

  ASSERT_STREQ(expected_data.data(), next.data.data());
}

TEST(QueueAccess, peek_empty)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  auto msg = TestMsg{};
  ASSERT_FALSE(queue.peek(msg));
}

TEST(QueueAccess, peek)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;

  queue.insert(msg);
  ASSERT_EQ(1, queue.size());

  auto value = TestMsg{};
  ASSERT_TRUE(queue.peek(value));
  ASSERT_EQ(1, queue.size());

  ASSERT_STREQ(expected_data.data(), value.data.data());
}

TEST(QueueAccess, swap_empty)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;
  queue.swap(msg);

  ASSERT_EQ(1, queue.size());
  ASSERT_TRUE(msg.data.empty());
}

TEST(QueueAccess, swap)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;

  queue.insert(msg);
  ASSERT_EQ(1, queue.size());

  const auto changed_data = std::string{ "Some new data" };
  msg.data = changed_data;
  queue.swap(msg);

  ASSERT_EQ(1, queue.size());
  ASSERT_FALSE(msg.data.empty());

  EXPECT_STREQ(expected_data.data(), msg.data.data());

  ASSERT_TRUE(queue.take(msg));
  ASSERT_EQ(0, queue.size());

  EXPECT_STREQ(changed_data.data(), msg.data.data());
}

TEST(QueueAccess, replace_empty)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;

  ASSERT_FALSE(msg.data.empty());

  msg = queue.replace(msg);

  ASSERT_TRUE(msg.data.empty());
  ASSERT_EQ(1, queue.size());
}

TEST(QueueAccess, replace)
{
  Queue<TestMsg> queue(1);
  const auto expected_capacity = 1;
  queue.setCapacity(expected_capacity);
  ASSERT_EQ(0, queue.size());

  const auto expected_data = std::string{ "Expected Data" };
  auto msg = TestMsg{};
  msg.data = expected_data;

  queue.insert(msg);
  ASSERT_EQ(1, queue.size());

  const auto changed_data = std::string{ "Some new data" };
  msg.data = changed_data;

  msg = queue.replace(msg);

  ASSERT_EQ(1, queue.size());
  ASSERT_FALSE(msg.data.empty());

  EXPECT_STREQ(expected_data.data(), msg.data.data());

  ASSERT_TRUE(queue.take(msg));
  ASSERT_EQ(0, queue.size());

  EXPECT_STREQ(changed_data.data(), msg.data.data());
}
TEST(QueueCapacity, default_capacity)
{
  Queue<TestMsg> queue(1);
  const auto default_capacity = 1;
  ASSERT_EQ(default_capacity, queue.capacity());
}

TEST(QueueCapacity, constructor_set_capacity)
{
  const auto capacity = 5;
  Queue<TestMsg> queue(capacity);
  ASSERT_EQ(capacity, queue.capacity());
}

TEST(QueueCapacity, grow_capacity)
{
  Queue<TestMsg> queue;
  auto capacity = size_t{ 5 };
  queue.setCapacity(capacity);
  EXPECT_EQ(0, queue.size());
  ASSERT_EQ(capacity, queue.capacity());

  // Fill queue
  auto original_entries = std::vector<TestMsg>{};
  for (auto i = 0; i < capacity; ++i)
  {
    auto msg = TestMsg{};
    msg.data = std::to_string(i);
    original_entries.push_back(msg);
    queue.insert(std::move(msg));
  }

  EXPECT_EQ(capacity, queue.size());
}

TEST(QueueCapacity, shrink_capacity)
{
  Queue<TestMsg> queue;
  auto capacity = size_t{ 5 };
  queue.setCapacity(capacity);
  EXPECT_EQ(0, queue.size());
  ASSERT_EQ(capacity, queue.capacity());

  // Fill queue
  auto original_entries = std::vector<TestMsg>{};
  for (auto i = 0; i < capacity; ++i)
  {
    auto msg = TestMsg{};
    msg.data = std::to_string(i);
    original_entries.push_back(msg);
    queue.insert(std::move(msg));
  }

  EXPECT_EQ(capacity, queue.size());

  // Grow capacity, size should not change.
  auto expected_size = queue.size();
  capacity += 5;
  queue.setCapacity(capacity);
  ASSERT_EQ(capacity, queue.capacity());
  ASSERT_EQ(expected_size, queue.size());

  // Shrink capacity to size of queue, no data should be lost.
  capacity = queue.size();
  queue.setCapacity(capacity);
  ASSERT_EQ(capacity, queue.size());

  auto it = std::begin(original_entries);
  for (auto i = 0; i < capacity; ++i)
  {
    auto peek_ = TestMsg{};
    ASSERT_TRUE(queue.peek(peek_, i));
    ASSERT_FALSE(peek_.data.empty());
    const auto msg = boost::any_cast<TestMsg>(peek_);
    EXPECT_STREQ(it->data.c_str(), msg.data.c_str());
    ++it;
  }

  // Shrink capacity to less than size of queue, should remove entries from FRONT.
  capacity = 2;
  expected_size = capacity;
  queue.setCapacity(capacity);
  ASSERT_EQ(capacity, queue.capacity());
  ASSERT_EQ(expected_size, queue.size());

  it = std::prev(std::end(original_entries), capacity);
  for (auto i = 0; i < capacity; ++i)
  {
    auto peek_ = TestMsg{};
    ASSERT_TRUE(queue.peek(peek_, i));
    ASSERT_FALSE(peek_.data.empty());
    const auto msg = boost::any_cast<TestMsg>(peek_);
    EXPECT_STREQ(it->data.c_str(), msg.data.c_str());
    ++it;
  }
}

class QueueModificationTest : public ::testing::Test
{
  void SetUp()
  {
    const auto num_messages = 5;
    queue_.setCapacity(num_messages);

    for (auto i = 0; i < queue_.capacity(); ++i)
    {
      auto msg = TestMsg{};
      msg.data = std::string{ "Example message number " } + std::to_string(i);
      queue_.insert(msg);
      messages_.push_back(msg);
    }
  }

protected:
  Queue<TestMsg> queue_;
  std::vector<TestMsg> messages_;
};

TEST_F(QueueModificationTest, append_to_tail_at_capacity)
{
  auto msg = TestMsg{};
  msg.data = std::string{ "New message!" };
  queue_.insert(msg);

  messages_.erase(std::begin(messages_));
  messages_.push_back(msg);

  ASSERT_EQ(queue_.capacity(), queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, insert_at_head_at_capacity)
{
  auto msg = TestMsg{};
  msg.data = std::string{ "New message!" };
  queue_.insert(msg, 0);

  messages_.erase(std::begin(messages_));
  messages_.insert(std::begin(messages_), msg);

  ASSERT_EQ(queue_.capacity(), queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, insert_in_middle_at_capacity)
{
  const auto index = 2;
  auto msg = TestMsg{};
  msg.data = std::string{ "New message!" };
  queue_.insert(msg, index);

  auto it = std::begin(messages_);
  messages_.erase(it);
  it = std::next(std::begin(messages_), index);
  messages_.insert(it, msg);

  ASSERT_EQ(queue_.capacity(), queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, remove_head)
{
  auto it = std::begin(messages_);
  messages_.erase(it);
  queue_.erase(0);

  ASSERT_EQ(queue_.capacity() - 1, queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    auto msg = TestMsg{};
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, remove_tail)
{
  auto it = std::prev(std::end(messages_));
  messages_.erase(it);
  queue_.erase();

  ASSERT_EQ(queue_.capacity() - 1, queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    auto msg = TestMsg{};
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, remove_middle)
{
  const auto index = 2;
  auto it = std::next(std::begin(messages_), index);
  messages_.erase(it);
  queue_.erase(index);

  ASSERT_EQ(queue_.capacity() - 1, queue_.size());
  for (auto i = 0; i < queue_.size(); ++i)
  {
    auto msg = TestMsg{};
    ASSERT_TRUE(queue_.peek(msg, i));
    EXPECT_STREQ(messages_.at(i).data.data(), msg.data.data());
  }
}

TEST_F(QueueModificationTest, retain_last)
{
  queue_.setRetainLastEnabled(true);

  // Take all the messages
  const auto num_messages = queue_.size();
  for (auto i = 0; i < num_messages; i++)
  {
    auto msg = TestMsg{};
    ASSERT_TRUE(queue_.take(msg));
  }

  //  Should still have one left
  ASSERT_EQ(1, queue_.size());

  auto last_msg = TestMsg{};
  ASSERT_TRUE(queue_.take(last_msg));

  // Size remains 1... again
  ASSERT_EQ(1, queue_.size());

  auto remaining_msg = TestMsg{};
  ASSERT_TRUE(queue_.peek(remaining_msg));

  EXPECT_STREQ(last_msg.data.data(), remaining_msg.data.data());
  EXPECT_STREQ(messages_.back().data.data(), last_msg.data.data());
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
