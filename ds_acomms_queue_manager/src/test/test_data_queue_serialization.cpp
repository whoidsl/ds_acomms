/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/DataQueueMessage.h"

#include <gtest/gtest.h>

using namespace ds_acomms_queue_manager;

TEST(DataQueueMessage, message_type)
{
  auto msg = DataQueueMessage{};
  EXPECT_EQ(QueueManagerMessageEnum::SEND_DATA_QUEUE, queue_message_type(msg));
}

TEST(DataQueueMessage, deserialize_bad_queue_numbers)
{
  auto buf = std::string{ "SDQ -5" };
  auto result = DataQueueMessage{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_INVALID_QUEUE, result.error);

  buf = std::string{ "SDQ 55555" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_INVALID_QUEUE, result.error);
}

TEST(DataQueueMessage, deserialize_string_no_payload)
{
  auto expected = DataQueueMessage{};
  expected.queue = 5;

  auto result = DataQueueMessage{};

  auto buf = std::string{ "SDQ 5" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_TRUE(result.payload.empty());

  buf = std::string{ "SDQ5" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_TRUE(result.payload.empty());

  buf = std::string{ "   SDQ5" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_TRUE(result.payload.empty());
}

TEST(DataQueueMessage, deserialize_string_with_payload)
{
  auto expected = DataQueueMessage{};
  expected.queue = 5;
  const auto payload_str = std::string{ "Some queue data" };
  expected.payload.resize(payload_str.size());
  std::copy(std::begin(payload_str), std::end(payload_str), std::begin(expected.payload));

  auto result = DataQueueMessage{};
  auto buf = std::string{ "SDQ 5:Some queue data" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_EQ(expected.payload, result.payload);

  buf = std::string{ "SDQ 5  :Some queue data" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_EQ(expected.payload, result.payload);

  buf = std::string{ "   SDQ5:Some queue data" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(DataQueueMessage::ERROR_NONE, result.error);
  EXPECT_EQ(expected.queue, result.queue);
  EXPECT_EQ(expected.payload, result.payload);
}

TEST(DataQueueMessage, serialize_string_no_payload)
{
  const auto expected = std::string{ "SDQ 5" };
  auto msg = DataQueueMessage{};
  msg.queue = 5;

  auto result = std::string{};
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

TEST(DataQueueMessage, serialize_string_with_payload)
{
  const auto expected = std::string{ "SDQ 5:Some payload data to send" };
  auto msg = DataQueueMessage{};
  msg.queue = 5;
  const auto payload_str = std::string{ "Some payload data to send" };
  msg.payload.resize(payload_str.size());
  std::copy(std::begin(payload_str), std::end(payload_str), std::begin(msg.payload));

  auto result = std::string{};
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

TEST(DataQueueMessage, serialize_with_errors)
{
  auto msg = DataQueueMessage{};
  msg.queue = 5;
  msg.error = DataQueueMessage::ERROR_INVALID_QUEUE;
  auto result = std::string{};

  auto expected = std::string{ "SDQ 5:" } + DataQueueMessage::ERROR_INVALID_QUEUE_STR;
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);

  msg.error = DataQueueMessage::ERROR_NO_DATA;
  expected = std::string{ "SDQ 5:" } + DataQueueMessage::ERROR_NO_DATA_STR;
  ds_acomms_serialization::serialize(msg, result);
  EXPECT_EQ(expected, result);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
