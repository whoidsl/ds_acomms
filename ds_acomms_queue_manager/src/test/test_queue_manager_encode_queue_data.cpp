/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_queue_manager/serialization.h"
#include "ds_acomms_queue_manager/queue_manager.h"
#include "ds_acomms_queue_manager/simple_single_queue.h"

#include "std_msgs/String.h"

#include <gtest/gtest.h>

using namespace ds_acomms_queue_manager;

using TestMessage = std_msgs::String;

class TestQueue : public SimpleSingleQueue<TestMessage>
{
public:
  void setEmpty(bool empty)
  {
    empty_ = empty;
  }
  bool empty() override
  {
    return empty_;
  }
  void setup(ros::NodeHandle& node) override
  {
  }

protected:
  bool empty_ = false;
};

namespace ds_acomms_serialization
{
template <>
void serialize(const TestMessage& value, std::string& buf)
{
  buf = value.data;
}
}

class EncodeDataTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    manager_.reset(new QueueManager);
    auto queue = std::unique_ptr<TestQueue>(new TestQueue);

    ASSERT_TRUE(manager_->addQueue(id_, std::move(queue)));
    queue_ptr_ = static_cast<TestQueue*>(manager_->queue(id_));
    manager_->setup();
  }

  std::unique_ptr<QueueManager> manager_;
  QueueManager::QueueId id_ = 0;
  TestQueue* queue_ptr_;
};

TEST_F(EncodeDataTest, invalid_queue_as_string)
{
  std::vector<uint8_t> buf{};
  auto msg = DataQueueMessage{};
  msg.queue = id_ + 1;
  msg.error = DataQueueMessage::ERROR_INVALID_QUEUE;

  auto expected = std::string{};
  ds_acomms_serialization::serialize(msg, expected);

  manager_->encodeQueueData(msg.queue, buf, true);

  const auto result = std::string{ std::begin(buf), std::end(buf) };
  EXPECT_EQ(expected, result);
}

TEST_F(EncodeDataTest, no_data_as_string)
{
  std::vector<uint8_t> buf{};
  auto msg = DataQueueMessage{};
  msg.queue = id_;
  msg.error = DataQueueMessage::ERROR_NO_DATA;
  queue_ptr_->setEmpty(true);

  auto expected = std::string{};
  ds_acomms_serialization::serialize(msg, expected);

  manager_->encodeQueueData(msg.queue, buf, true);

  const auto result = std::string{ std::begin(buf), std::end(buf) };
  EXPECT_EQ(expected, result);
}

TEST_F(EncodeDataTest, encode_as_string)
{
  // Setup our test queue data
  auto msg = TestMessage{};
  const auto payload = std::string{ "Example payload" };
  msg.data = payload;

  // This is our expected result.
  auto queue_msg = DataQueueMessage{};
  queue_msg.queue = id_;
  queue_msg.error = 0;
  queue_msg.payload.resize(payload.size());
  std::copy(std::begin(payload), std::end(payload), std::begin(queue_msg.payload));

  // And encoded as a string.
  auto expected = std::string{};
  ds_acomms_serialization::serialize(queue_msg, expected);

  // Insert the message into the queue
  auto q = static_cast<TestQueue*>(manager_->queue(id_));
  ASSERT_TRUE(q);
  q->set(msg);

  // And now attempt to retrieve it.
  auto buf = std::vector<uint8_t>{};
  manager_->encodeQueueData(id_, buf, true);

  // Compare against expected string
  auto result = std::string{ std::begin(buf), std::end(buf) };
  EXPECT_EQ(expected, result);

  // Now try again, should be empty
  queue_ptr_->setEmpty(true);
  queue_msg.queue = id_;
  queue_msg.error = DataQueueMessage::ERROR_NO_DATA;
  queue_msg.payload.clear();

  expected = std::string{};
  ds_acomms_serialization::serialize(queue_msg, expected);

  manager_->encodeQueueData(id_, buf, true);
  result = std::string{ std::begin(buf), std::end(buf) };
  EXPECT_EQ(expected, result);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "queue_manager");
  testing::InitGoogleTest(&argc, argv);
  auto result = RUN_ALL_TESTS();
  ros::shutdown();
  ros::waitForShutdown();
  return result;
}
