/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_SIMPLE_SINGLE_QUEUE_H
#define DS_ACOMMS_SIMPLE_SINGLE_QUEUE_H

#include "ds_base/ds_global.h"
#include "ds_acomms_queue_manager/queue_interface.h"
#include "ds_acomms_serialization/serialization.h"

#include <functional>

namespace ds_acomms_queue_manager
{
template <typename T>
class SimpleSingleQueue : public QueueInterface
{
public:
  // In order to be able to access the type for creating a publisher in
  // my new sentry_vehicle_queue_publisher
  typedef T msgT;

  using StringEncoder = std::function<void(const T& msg, std::string& buf)>;
  using ByteEncoder = std::function<void(const T& msg, std::vector<uint8_t>& buf)>;

  explicit SimpleSingleQueue() = default;
  virtual ~SimpleSingleQueue() = default;

  DS_DISABLE_COPY(SimpleSingleQueue);

  bool empty() override
  {
    return false;
  }

  void set(const T& msg)
  {
    item_ = msg;
  }

  T& ref()
  {
    return item_;
  }

  bool encodeNext(std::vector<uint8_t>& buf, bool as_string) override
  {
    if (empty())
    {
      return false;
    }

    ds_acomms_serialization::serialize(item_, buf, as_string);
    return true;
  }

protected:
  T item_;
};
}
#endif  // DS_ACOMMS_SIMPLE_SINGLE_QUEUE_H
