/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_QUEUE_MANAGER_SERIALIZATION_H
#define DS_ACOMMS_QUEUE_MANAGER_SERIALIZATION_H

#include "ds_acomms_serialization/serialization.h"
#include "ds_acomms_queue_manager/QueueManagerMessageEnum.h"

namespace std_msgs
{
ROS_DECLARE_MESSAGE(String)
}

namespace ds_acomms_queue_manager
{
ROS_DECLARE_MESSAGE(DataQueueMessage)
ROS_DECLARE_MESSAGE(LastReceivedMessage)

template <typename T>
uint8_t queue_message_type(const T&)
{
  return QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
}

template <>
constexpr uint8_t queue_message_type(const DataQueueMessage&)
{
  return QueueManagerMessageEnum::SEND_DATA_QUEUE;
}

template <>
constexpr uint8_t queue_message_type(const LastReceivedMessage&)
{
  return QueueManagerMessageEnum::SEND_LAST_RECVD;
}

/// @brief Determine the message type of the provided string.
///
/// Returns AcousticMessageTypeEnum::USER_DEFINED if unable to match a message
/// type to the string.
///
/// \param buf
/// \return
template <>
uint8_t queue_message_type(const std::string& buf);
}

namespace ds_acomms_serialization
{
/// @brief Decode a DataQueueString message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_queue_manager::DataQueueMessage& msg);

/// @brief Encode a DataQueueString message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_queue_manager::DataQueueMessage& msg, std::string& buf);

/// @brief Decode a LastReceivedMessage message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, ds_acomms_queue_manager::LastReceivedMessage& msg);

/// @brief Encode a LastReceivedMessage message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const ds_acomms_queue_manager::LastReceivedMessage& msg, std::string& buf);

/// @brief Decode a DataQueueString message serialized as plain-text
///
/// \param buf
/// \param msg
/// \return  False if parsing failed.
template <>
bool deserialize(const std::string& buf, std_msgs::String& msg);

/// @brief Encode a DataQueueString message into a plain-text string
///
/// \param msg
/// \param buf
template <>
void serialize(const std_msgs::String& msg, std::string& buf);
}
#endif  // DS_ACOMMS_QUEUE_MANAGER_SERIALIZATION_H
