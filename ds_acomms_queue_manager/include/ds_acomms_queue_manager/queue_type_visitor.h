/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMM_QUEUE_MANAGER_QUEUE_TYPE_VISISTOR_H
#define DS_ACOMM_QUEUE_MANAGER_QUEUE_TYPE_VISISTOR_H

#include "ds_acomms_queue_manager/queue.h"
#include "ds_acomms_serialization/rosmsg_serialization.h"

#include <unordered_map>
#include <typeinfo>

namespace ds_acomms
{
//
// Adapted from https://gist.github.com/pyrtsa/2945472
//
struct QueueTypeVisitor
{
  using TypeInfoHash = struct
  {
    std::size_t operator()(std::type_info const& t) const
    {
      return t.hash_code();
    }
  };

  using EqualRef = struct
  {
    template <typename T>
    bool operator()(std::reference_wrapper<T> a, std::reference_wrapper<T> b) const
    {
      return a.get() == b.get();
    }
  };

  using TypeInfoRef = std::reference_wrapper<std::type_info const>;
  using StringEncodingFunction = std::function<StringEncodedData(boost::any&)>;
  using ByteEncodingFunction = std::function<ByteEncodedData(boost::any&)>;

  std::unordered_map<TypeInfoRef, StringEncodingFunction, TypeInfoHash, EqualRef> string_enc_map_;
  std::unordered_map<TypeInfoRef, ByteEncodingFunction, TypeInfoHash, EqualRef> byte_enc_map_;

  template <typename T>
  void addType()
  {
    byte_enc_map_.insert(std::make_pair(
        std::ref(typeid(T)), ByteEncodingFunction([](boost::any& x) { return to_bytes(boost::any_cast<T&>(x)); })));

    string_enc_map_.insert(std::make_pair(
        std::ref(typeid(T)), StringEncodingFunction([](boost::any& x) { return to_string(boost::any_cast<T&>(x)); })));
  }

  bool encode(boost::any& x, ByteEncodedData& buf)
  {
    auto it = byte_enc_map_.find(x.type());
    if (it != byte_enc_map_.end())
    {
      buf = it->second(x);
      return true;
    }
    else
    {
      return false;
    }
  }

  bool encode(boost::any& x, StringEncodedData& buf)
  {
    auto it = string_enc_map_.find(x.type());
    if (it != string_enc_map_.end())
    {
      buf = it->second(x);
      return true;
    }
    else
    {
      return false;
    }
  }
};
}
#endif  // DS_ACOMM_QUEUE_MANAGER_QUEUE_TYPE_VISISTOR_H
