/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMMS_SIMPLE_DS_CONNECTION_QUEUE_H
#define DS_ACOMMS_SIMPLE_DS_CONNECTION_QUEUE_H

#include <ds_base/ds_global.h>
#include <ds_base/ds_process.h>
#include "ds_acomms_queue_manager/simple_single_queue.h"

#include <functional>

namespace ds_asio
{
class DsConnection;
}

namespace ds_core_msgs
{
ROS_DECLARE_MESSAGE(RawData);
}

namespace ds_acomms_queue_manager
{
/// @brief A SingleSimpleQueue fed from data received on a DsConnection
///
/// \tparam T Type of data store in queue.  Must have ds_acomms_serialization::serialize and
/// ds_acomms_serialization::deserialize defined.
template <typename T>
class SimpleDsConnectionQueue : public SimpleSingleQueue<T>
{
public:
  explicit SimpleDsConnectionQueue(ds_base::DsProcess& dsprocess, const std::string& name) : SimpleSingleQueue<T>()
  {
    udp_ = dsprocess.addConnection(name, boost::bind(&SimpleDsConnectionQueue::rawCallback, this, _1));
  }

  ~SimpleDsConnectionQueue() override = default;

  DS_DISABLE_COPY(SimpleDsConnectionQueue)

  void setup(ros::NodeHandle& node) override
  {
  }

  const boost::shared_ptr<ds_asio::DsConnection>& conn() const
  {
    return udp_;
  }

private:
  void rawCallback(const ds_core_msgs::RawData& msg)
  {
    ROS_WARN_STREAM("Inside callback.");
    T item;
    if (!ds_acomms_serialization::deserialize(msg.data, item, true))
    {
      ROS_WARN_STREAM("Failed to parse connection data as string: '" << std::string(msg.data.begin(), msg.data.end())
                                                                     << "'");
      return;
    }
    this->set(item);
  }

  boost::shared_ptr<ds_asio::DsConnection> udp_;
};
}
#endif  // DS_ACOMMS_SIMPLE_DS_CONNECTION_QUEUE_H
