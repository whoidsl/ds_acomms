/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMM_QUEUE_MANAGER_QUEUE_H
#define DS_ACOMM_QUEUE_MANAGER_QUEUE_H

#include "ds_base/ds_global.h"
#include "ds_acomms_queue_manager/queue_interface.h"

#include <ros/node_handle.h>

#include <boost/any.hpp>

#include <deque>
namespace ds_acomms_queue_manager
{
/// @brief Generic Acoustic Message Queue
///
/// A generic message queue for acoustic messaging use with ROS.
///
template <typename T>
class Queue : public QueueInterface
{
public:
  using StringEncoder = std::function<void(const T& msg, std::string& buf)>;
  using ByteEncoder = std::function<void(const T& msg, std::vector<uint8_t>& buf)>;

  explicit Queue(size_t capacity = 1) : retain_last_(false), capacity_(capacity)
  {
  }

  Queue(StringEncoder string_enc, ByteEncoder byte_enc, size_t capacity = 1)
    : string_enc_(std::move(string_enc)), byte_enc_(std::move(byte_enc)), retain_last_(false), capacity_(capacity)
  {
  }

  virtual ~Queue() = default;
  DS_DISABLE_COPY(Queue);

  /// @brief Get the capacity for the queue
  /// The capacity of a queue is the maximum number of messages that can be
  /// queued pending for transmission.  Once the queue reaches capacity, new
  /// messages inserted into the queue will remove messages from the front to
  /// make room.  Under normal operation, removing messages from the front is
  /// equivalent to removing the oldest message in the queue.
  ///
  /// \return
  size_t capacity() const noexcept
  {
    return capacity_;
  }

  /// @brief Set the capacity for the queue
  ///
  /// The capacity of a queue is the maximum number of messages that can be
  /// queued pending for transmission.  Once the queue reaches capacity, new
  /// messages inserted into the queue will remove messages from the front to
  /// make room.  Under normal operation, removing messages from the front is
  /// equivalent to removing the oldest message in the queue.
  ///
  /// \param capacity
  void setCapacity(size_t capacity)
  {
    if (capacity >= queue_.size())
    {
      capacity_ = capacity;
      return;
    }

    const auto start = std::begin(queue_);
    const auto end = std::next(start, queue_.size() - capacity);
    queue_.erase(start, end);
    capacity_ = capacity;
  }

  bool empty() override
  {
    return queue_.empty();
  }
  /// @brief Get the current size of the queue
  ///
  /// Returns the number of messages pending in the queue.
  ///
  /// \return
  size_t size() const noexcept
  {
    return queue_.size();
  }

  /// @brief Insert a new message into the queue
  ///
  /// Inserts a new message into the queue.  The message is appended
  /// to the end of the queue by default.
  ///
  /// \param msg Message to insert
  /// \param pos Position to insert.
  void insert(T msg, size_t pos = SIZE_MAX)
  {
    // Short-circuit if capacity is 1, just replace the head.
    if (capacity_ == 1 && queue_.size() > 0)
    {
      queue_[0] = std::move(msg);
      return;
    }

    // Make space if necessary by removing the first message.
    if (queue_.size() >= capacity_)
    {
      queue_.pop_front();
    }

    if (pos >= queue_.size())
    {
      queue_.push_back(std::move(msg));
      return;
    }

    const auto it = std::next(std::begin(queue_), pos);
    queue_.insert(it, std::move(msg));
  }

  /// @brief Remove a message from the queue by position.
  ///
  /// The default behavior removes the message at the tail of the queue.
  /// Typically, this will be the last message that was inserted into the
  /// queue.
  ///
  /// \param pos
  void erase(size_t pos = SIZE_MAX)
  {
    if (empty())
    {
      return;
    }

    if (pos >= queue_.size())
    {
      queue_.pop_back();
      return;
    }

    const auto it = std::next(std::begin(queue_), pos);
    queue_.erase(it);
  }

  /// @brief Get a message from the queue without removing it.
  ///
  /// The default behavior is to return the message at the head of
  /// the queue.  The message at the tail of the queue is returned if
  /// the provided position is greater than the size of the queue.
  ///
  /// An empty shared pointer is returned if the queue is empty.
  ///
  /// \param pos
  /// \return
  bool peek(T& msg, size_t pos = 0)
  {
    if (empty())
    {
      return false;
    }

    msg = pos >= queue_.size() ? queue_.back() : queue_.at(pos);
    return true;

#if 0
    if (pos >= queue_.size())
    {
      msg = queue_.at(queue_.size() - 1);
      return true;
    }

    queue_.at(pos);
#endif
  }

  /// @brief Swap the value at the desired index.
  ///
  /// The default behavior is to swap the message at the tail of
  /// the queue.
  ///
  /// If the queue is empty, the provided message will be inserted into
  /// the queue and the provided reference will be set empty.
  ///
  /// \param rhs
  /// \param pos
  /// \return
  void swap(T& rhs, size_t pos = SIZE_MAX)
  {
    if (empty())
    {
      insert(rhs);
      rhs = T{};
      return;
    }

    pos = std::min(queue_.size() - 1, pos);
    auto tmp = queue_.at(pos);
    queue_[pos] = rhs;
    rhs = tmp;
  }

  /// @brief Replace the value at the desired index
  ///
  /// The default behavior is to replace the message at the tail of
  /// the queue.
  ///
  /// If the queue is empty, the provided message will be inserted into
  /// the queue and the provided reference will be set empty.
  ///
  /// \param rhs
  /// \param pos
  /// \return
  T replace(T rhs, size_t pos = SIZE_MAX)
  {
    if (empty())
    {
      insert(rhs);
      return {};
    }

    pos = std::min(queue_.size() - 1, pos);
    const auto v = queue_.at(pos);
    queue_[pos] = rhs;
    return v;
  }

  /// @brief Take a message from the queue
  ///
  /// This method removes the message at the desired position and returns
  /// it to the user.
  ///
  /// By default, the message at the head of the queue is returned.  If the
  /// provided position is greater than the size of the queue, the message at
  /// the tail of the queue is returned.
  ///
  /// An empty shared pointer is returned if the queue is empty
  ///
  /// \param pos
  /// \return
  bool take(T& msg, size_t pos = 0)
  {
    if (empty())
    {
      return false;
    }

    auto it = std::begin(queue_);
    if (pos >= queue_.size())
    {
      it = std::prev(std::end(queue_));
    }
    else
    {
      it = std::next(it, pos);
    }

    msg = *it;
    if (queue_.size() > 1 || !retain_last_)
    {
      queue_.erase(it);
    }
    return true;
  }

  /// @brief Retain the last value of a queue
  ///
  /// Setting this true changes the behavior of the queue when take() would leave
  /// the queue empty.  When enabled, the last value remains in the queue, preventing
  /// the queue from ever being empty.  This may be handy for status messages that are
  /// produced less frequently than they are queried.
  ///
  /// \param enabled
  void setRetainLastEnabled(bool enabled)
  {
    retain_last_ = enabled;
  }

  /// @brief Retain the last value of a queue
  ///
  /// Setting this true changes the behavior of the queue when take() would leave
  /// the queue empty.  When enabled, the last value remains in the queue, preventing
  /// the queue from ever being empty.  This may be handy for status messages that are
  /// produced less frequently than they are queried.
  ///
  /// \return
  bool retainLastEnabled() const noexcept
  {
    return retain_last_;
  }

  /// @brief Set the string encoder for messages in this queue.
  ///
  /// The string encoder is used when encoding messages in plain-text.
  ///
  /// \param func
  void setStringEncoder(StringEncoder func)
  {
    string_enc_ = func;
  }

  /// @brief Get the string encoder for messages in this queue.
  ///
  /// \return
  StringEncoder& stringEncoder()
  {
    return string_enc_;
  }

  /// @brief Set the byte encoder for messages in this queue.
  ///
  /// The byte encoder is used when encoding messages as raw bytes.
  ///
  /// \param func
  void setByteEncoder(ByteEncoder func)
  {
    byte_enc_ = func;
  }

  /// @brief Get the byte encoder for messages in this queue.
  ///
  /// \return
  ByteEncoder& byteEncoder()
  {
    return byte_enc_;
  }

  /// @brief Encode a queue message into the provided byte buffer.
  ///
  /// Returns false if no byte encoding function has been provided
  ///
  /// \param msg
  /// \param buf
  /// \param as_string
  /// \return
  bool encodeNext(std::vector<uint8_t>& buf, bool as_string = false) override
  {
    if (empty())
    {
      return false;
    }

    auto next = T{};
    if (!take(next))
    {
      return false;
    }

    if (as_string)
    {
      if (!string_enc_)
      {
        return false;
      }

      std::string sbuf{};
      string_enc_(next, sbuf);
      buf = std::vector<uint8_t>{ std::begin(sbuf), std::end(sbuf) };
      return true;
    }

    if (!byte_enc_)
    {
      return false;
    }
    byte_enc_(next, buf);
    return true;
  }

  void setup(ros::NodeHandle& node) override
  {
  }

private:
  size_t capacity_;
  std::deque<T> queue_;
  Queue::StringEncoder string_enc_;
  Queue::ByteEncoder byte_enc_;
  bool retain_last_;
};
}
#endif  // DS_ACOMM_QUEUE_MANAGER_QUEUE_H
