/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_H
#define DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_H

#include "ds_base/ds_process.h"
#include "ds_acomms_queue_manager/queue_interface.h"
#include "ds_acomms_queue_manager/DataQueueMessage.h"

#include <stdint.h>

namespace ds_acomms_msgs
{
ROS_DECLARE_MESSAGE(AcousticModemData)
}

namespace ds_acomms_queue_manager
{
struct QueueManagerPrivate;

/// @brief  A message queue manager for acoustic communication.
///
/// ## Usage Requirements
///
/// Using the QueueManager class with a message types requires that those types have a few
/// serialization/deserialization methods defined for them:
///
///   // Convert to a string
///   std::string ds_acomm_queue_manager::to_string(const T& msg)
///
///   // Convert from a string
///   ds_acomm_queue_manager::from_string(std::string& buf, T& msg)
///
///   // To bytes
///   void ds_acomm_queue_manager::to_bytes(const T& msg, std::vector<uint8_t>& buf))
///
///   // From bytes
///   void ds_acomm_queue_manager::from_bytes(std::vector<uint8_t>& buf, T& msg)
///
/// where 'T' in this case is the message type.
///
/// For ROS Message types, the to_bytes() and from_bytes() methods are satisifed by templated functions
/// found in `ds_acomm_queue_manager/serialize.h`
///
class QueueManager : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(QueueManager)

public:
  using QueueId = uint8_t;
  using QueueMap = std::unordered_map<QueueId, std::shared_ptr<QueueInterface>>;

  using ModemAddress = uint16_t;

  QueueManager();
  QueueManager(int argc, char* argv[], std::string& name);
  ~QueueManager() override;
  DS_DISABLE_COPY(QueueManager)

  /// @brief Add a queue to the queue manager
  ///
  /// The queue must have both string and byte encoders defined to
  /// be added.
  ///
  /// \param id
  /// \param queue
  /// \return
  bool addQueue(QueueId id, std::unique_ptr<QueueInterface> queue);

  /// @brief Remove a queue from the queue manager
  ///
  /// \param id
  void removeQueue(QueueId id);

  /// @brief Access a queue
  ///
  /// Returns a nullptr if the provided queue id does not exist.
  ///
  /// \param id
  /// \return
  QueueInterface* queue(QueueId id);

  /// @brief Setup the queue manager
  ///
  /// The QueueManager setup override adds a setupQueues() step after
  /// the initial DsProcess::setup() call.
  void setup() override;

  void sendModemData(ModemAddress local_addr, ModemAddress remote_addr, std::vector<uint8_t> data);

  void encodeQueueData(QueueId id, std::vector<uint8_t>& buf, bool as_string = true);

protected:
  /// @brief Setup queues
  ///
  /// This hook is called by QueueManager::setup() to populate
  /// the queues.  It calls the 'setup()' method on all queues
  /// added to the manager.
  virtual void setupQueues();
  void setupSubscriptions() override;
  void setupPublishers() override;
  virtual void handleIncomingModemData(const ds_acomms_msgs::AcousticModemData& msg);

  virtual bool handleCustomQueueMessage(const ds_acomms_msgs::AcousticModemData& msg, bool as_string = true)
  {
    return false;
  }

  // @brief Added to handle additional payload after : in SDQ queue requests
  //
  // This hook's typical use case is for additional nav data intended for the vehicle appended after
  // a queue request data sent down with SDQ requests
  virtual bool handleQueueRequestAdditionalPayload(std::vector<uint8_t>& buf, bool as_string = true)
  {
    return false;
  }

private:
  std::unique_ptr<QueueManagerPrivate> d_ptr_;
};
}
#endif  // DS_ACOMM_QUEUE_MANAGER_QUEUE_MANAGER_H
