/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ACOMM_QUEUE_MANAGER_SERIALIZATION_H
#define DS_ACOMM_QUEUE_MANAGER_SERIALIZATION_H

#include "ros/serialization.h"
#include "ros/ros.h"
#include <boost/any.hpp>
#include <type_traits>

namespace ds_acomms_serialization
{
ROS_DECLARE_MESSAGE(DataQueueMessage)
ROS_DECLARE_MESSAGE(LastReceivedMessage)

#if 0
/// @brief Prototype message type string-ifier
///
/// \tparam T
/// \param msg
/// \return
template <typename T>
std::string to_string(T& msg);
#endif

/// @brief Serialization for ROS messages
///
/// A templated function for serializing ros messages
/// \tparam T
/// \param msg
/// \param buf
template <typename T, typename = std::enable_if<ros::message_traits::IsMessage<T>::value> >
void rosmsg_to_bytes(const T& msg, std::vector<uint8_t>& buf)
{
  const auto size = ros::serialization::serializationLength(msg);
  buf.resize(size);
  auto ostream = ros::serialization::OStream(buf.data(), size);
  ros::serialization::serialize(ostream, msg);
}

template <typename T, typename = std::enable_if<ros::message_traits::IsMessage<T>::value> >
void to_bytes(const boost::any& msg, std::vector<uint8_t>& buf)
{
  try
  {
    auto data = boost::any_cast<T>(msg);
    rosmsg_to_bytes(data, buf);
  }
  catch (boost::bad_any_cast&)
  {
    return;
  }
}

/// @brief Deserialize ROS message from bytes
///
/// A templated function for deserializing ros messages
///
/// NOTE:  It does not appear that ros informs users when deserialization fails!
/// \tparam T
/// \param buf
/// \param msg
template <typename T, typename = std::enable_if<ros::message_traits::IsMessage<T>::value> >
void rosmsg_from_bytes(const std::vector<uint8_t>& buf, T& msg)
{
  // NOTE:  Instructions from :
  //      http://wiki.ros.org/roscpp/Overview/MessagesSerializationAndAdaptingTypes
  //
  // appear to be wrong, at least for messages with Strings in them (and probably other
  // variable-length members).  The ::serializationLength(T) method returns how large
  // the buffer needs to be to *serialize*, it has NOTHING to do do with the deserialization
  // requirements.  Doing the following DOES NOT WORK:
  //
  //   const auto size = ros::serialization::serializationLength(msg);
  //   ros::serialization::IStream istream(buf.data(), size);
  //
  // Instead, we'll assume that the buffer contains at least as much data as needed.  But
  // we'll wrap it for now in a try/catch block.

  // Make a copy... since rosmsg deserilization REQUIRES a non-const buffer to read from.
  // We make a copy here so that subscription handlers can use a constant reference and the
  // performance hit is ONLY for ros messages (and not for ALL messages)
  auto buf_copy = buf;
  ros::serialization::IStream istream(buf_copy.data(), buf_copy.size());
  try
  {
    ros::serialization::deserialize(istream, msg);
  }
  catch (ros::Exception& e)
  {
    ROS_ERROR("Caught ros exception deserializing ros type %s from buffer of length %lu", typeid(T).name(),
              buf_copy.size());
  }
}

/// @brief Determine whether the buffer is a simple string or encoded bytes
///
/// \param buf
/// \return
// TODO: Update when we are actually encoding data, for now it's all strings.
inline bool is_string_encoded(const std::vector<uint8_t>& buf)
{
  return true;
}

// template <typename T>
// bool deserialize(const std::vector<uint8_t>& buf, T& msg);

template <typename T>
bool deserialize(const std::string& buf, T& msg);

/// @brief Deserialization from bytes for ROS messages
///
/// Always returns true -- there is no way to know if deserializing ROS messages fails
/// due to incorrect type.
///
/// \tparam T  ROS Message class
/// \param buf buffer with serialized data
/// \param msg Resulting message
/// \return  True if successful
template <typename T, typename = std::enable_if<ros::message_traits::IsMessage<T>::value> >
bool deserialize(const std::vector<uint8_t>& buf, T& msg, bool as_string = false)
{
  if (as_string)
  {
    const auto sbuf = std::string{ std::begin(buf), std::end(buf) };
    return deserialize(sbuf, msg);
  }
  else
  {
    rosmsg_from_bytes(buf, msg);
    return true;
  }
};

/// @brief Serialization of data into a plain-text string
///
/// \tparam T
/// \param msg
/// \param buf
template <typename T>
void serialize(const T& msg, std::string& buf);

/// @brief Serialization of a ROS message into a sequence of raw bytes
///
/// \tparam T
/// \param msg
/// \param buf
template <typename T, typename = std::enable_if<ros::message_traits::IsMessage<T>::value> >
void serialize(const T& msg, std::vector<uint8_t>& buf, bool as_string = false)
{
  if (as_string)
  {
    auto sbuf = std::string{};
    serialize(msg, sbuf);
    buf.resize(sbuf.size());
    std::copy(std::begin(sbuf), std::end(sbuf), std::begin(buf));
  }
  else
  {
    rosmsg_to_bytes(msg, buf);
  }
};
}
#endif  // DS_ACOMM_QUEUE_MANAGER_SERIALIZATION_H
