/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_acomms_serialization/serialization.h"
#include "std_msgs/UInt32.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/String.h"
#include <gtest/gtest.h>

using namespace ds_acomms_serialization;

// DS_DEFINE_ROSMSG_TO_BYTES(std_msgs::UInt32)

TEST(RosSerialization, serialize)
{
  std_msgs::UInt32 my_value;
  my_value.data = 5;

  uint32_t serial_size = ros::serialization::serializationLength(my_value);
  std::vector<uint8_t> buffer(serial_size);

  ros::serialization::OStream stream(buffer.data(), serial_size);
  ros::serialization::serialize(stream, my_value);
  ASSERT_GT(buffer.size(), 0);

  auto buffer_2 = std::vector<uint8_t>{};
  rosmsg_to_bytes(my_value, buffer_2);
  ASSERT_GT(buffer_2.size(), 0);

  EXPECT_EQ(buffer.size(), buffer_2.size());

  EXPECT_TRUE(std::equal(std::begin(buffer), std::end(buffer), std::begin(buffer_2)));
}

TEST(RosSerialization, deserialize)
{
  std_msgs::UInt32 my_value;
  my_value.data = 5;

  uint32_t serial_size = ros::serialization::serializationLength(my_value);
  std::vector<uint8_t> buffer(serial_size);

  ros::serialization::OStream stream(buffer.data(), serial_size);
  ros::serialization::serialize(stream, my_value);
  ASSERT_GT(buffer.size(), 0);

  auto msg_2 = std_msgs::UInt32{};

  rosmsg_from_bytes(buffer, msg_2);

  ASSERT_EQ(my_value.data, msg_2.data);
}

TEST(RosSerialization, serialize_deserialize)
{
  std_msgs::UInt32 my_value;
  my_value.data = 5;

  auto buffer = std::vector<uint8_t>{};
  rosmsg_to_bytes(my_value, buffer);
  ASSERT_GT(buffer.size(), 0);

  auto msg_2 = std_msgs::UInt32{};

  rosmsg_from_bytes(buffer, msg_2);

  ASSERT_EQ(my_value.data, msg_2.data);
}

TEST(RosSerialization, serialize_deserialize_string)
{
  std_msgs::String my_value;
  my_value.data = std::string{ "Hello!" };

  auto buffer = std::vector<uint8_t>{};
  rosmsg_to_bytes(my_value, buffer);
  ASSERT_GT(buffer.size(), 0);

  auto msg_2 = std_msgs::String{};

  rosmsg_from_bytes(buffer, msg_2);

  ASSERT_EQ(my_value.data, msg_2.data);
}

TEST(RosSerialization, deserialize_short_buf)
{
  std_msgs::String my_value;
  my_value.data = std::string{ "Hello!" };

  auto buffer = std::vector<uint8_t>{};
  rosmsg_to_bytes(my_value, buffer);
  ASSERT_GT(buffer.size(), 2);

  // Truncate buffer.  This will cause the deserialization to fail.
  buffer.resize(buffer.size() - 2);

  auto msg_2 = std_msgs::String{};
  ASSERT_NO_THROW(rosmsg_from_bytes(buffer, msg_2));
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
